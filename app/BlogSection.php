<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogSection extends Model
{
    protected $fillable = [
        'blog_id',
        'arrangement',
        'div_styling',
        'div_class',
        'div_id',
        'div_image_background',
        'section_style'
	];

    /**
     * Get parent blog
     */
    public function blog() {
        return $this->belongsTo('App\Blog');
    }

    /**
     * Get content children
     */
    public function contents() {
        return $this->hasMany('App\BlogContent', 'blog_section_id', 'id')->orderBy('arrangement');
    }

    /**
     * Get feature block children
     */
    public function feature_blocks() {
        return $this->hasMany('App\BlogFeatureBlock', 'blog_section_id', 'id')->orderBy('arrangement');
    }

    /**
     * Get feature section children
     */
    public function feature_sections() {
        return $this->hasMany('App\BlogFeatureSection', 'blog_section_id', 'id')->orderBy('arrangement');
    }
}
