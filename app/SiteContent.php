<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteContent extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
    	'partners_heading',
        'show_about_us_section',
        'show_testimonials_section',
        'show_partners_section',
        'show_content_1_section',
        'show_content_2_section',
        'show_pricing_section',
        'show_team_section',
        'show_qotd_section',
        'show_recent_blogs_section',
        'show_faq_section',
        'content_1_heading',
        'content_1_details',
        'content_1_cta_name',
        'content_1_cta_link',
        'content_2_heading',
        'content_2_details',
        'content_2_cta_name',
        'content_2_cta_link',
        'newsletter_heading',
        'newsletter_details',
        'feature_1_heading',
        'feature_1_details',
        'feature_1_cta_name',
        'feature_1_cta_link',
        'feature_1_image',
        'feature_2_heading',
        'feature_2_details',
        'feature_2_cta_name',
        'feature_2_cta_link',
        'feature_2_image',
        'feature_3_heading',
        'feature_3_details',
        'feature_3_cta_name',
        'feature_3_cta_link',
        'feature_3_image',

        'content_1_cta_type',
        'content_1_cta_name_2',
        'content_1_cta_link_2',
        'content_1_cta_type_2',
        'content_2_cta_type',
        'content_2_cta_name_2',
        'content_2_cta_link_2',
        'content_2_cta_type_2',

        'feature_1_cta_type',
        'feature_1_cta_name_2',
        'feature_1_cta_link_2',
        'feature_1_cta_type_2',

        'feature_2_cta_type',
        'feature_2_cta_name_2',
        'feature_2_cta_link_2',
        'feature_2_cta_type_2',

        'feature_3_cta_type',
        'feature_3_cta_name_2',
        'feature_3_cta_link_2',
        'feature_3_cta_type_2',
    ];
}
