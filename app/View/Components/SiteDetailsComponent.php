<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\SiteDetails;

class SiteDetailsComponent extends Component
{
    public $title;
    public $tab;
    public $site_details;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $tab)
    {
        $this->title = $title;
        $this->tab = $tab;
        $this->site_details = SiteDetails::first();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.site-details');
    }
}
