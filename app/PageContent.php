<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
    protected $fillable = [
        'page_section_id',
        'arrangement',
        'div_styling',
        'div_class',
        'div_id',
        'value'
	];
}
