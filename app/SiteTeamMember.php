<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteTeamMember extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'name',
        'position',
        'image',
        'show'
	];
}
