<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteDetails extends Model
{
    public $timestamps = false;

    protected $fillable = [
    	'name',
    	'description',
        'logo',
        'logo_2',
        'logo_3',
        'email',
        'phone',
        'open_on',
        'address',
        'site_under_maintenance'
	];
}
