<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCategory extends Model
{
    protected $fillable = [
        'name',
        'description',
        'slug',
        'image',
        'show',
	];

    /**
     * Related to many videos
     */
    public function videos() {
        return $this->hasMany('App\Video');
    }
}
