<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SitePricing extends Model
{
    protected $fillable = [
        'package_name',
        'price_one_time',
        'price_per_month',
        'price_per_year',
        'inclusions',
        'is_best_value',
        'pricing_type',
        'show'
	];
}
