<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteBanner extends Model
{
    protected $fillable = [
    	'image',
        'image_mobile',
        'title',
        'sub_title',
        'cta_name',
        'cta_link',
        'heading_position',
        'bg_color',
        'show',
        'cta_type',
        'cta_name_2',
        'cta_link_2',
        'cta_type_2'
    ];
}
