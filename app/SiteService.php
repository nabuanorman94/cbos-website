<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteService extends Model
{
    protected $fillable = [
    	'icon',
        'name',
        'details',
        'service_type',
        'content_1',
        'content_2',
        'image_1',
        'image_mobile',
        'image_2',
        'seo_serp',
        'seo_keywords',
        'seo_image',
        'slug',
        'bg_color',
        'show'
	];
}
