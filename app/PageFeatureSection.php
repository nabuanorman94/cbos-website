<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageFeatureSection extends Model
{
    protected $fillable = [
        'page_section_id',
        'arrangement',
        'div_styling',
        'div_class',
        'div_id',
        'feat_image',
        'feat_title',
        'feat_content',
        'image_placement'
	];
}
