<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteTestimonial extends Model
{
    protected $fillable = [
    	'name',
        'position',
        'content',
        'image',
        'show'
	];
}
