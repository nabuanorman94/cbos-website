<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'video_category_id',
        'title',
        'sub_title',
        'slug',
        'banner_image',
        'banner_image_mobile',
        'heading_position',
        'bg_color',
        'seo_serp',
        'seo_keywords',
        'seo_image',
        'show',
        'count_view',
        'count_likes'
	];

    /**
     * Get the video sections
     */
    public function videoSection() {
        return $this->hasMany('App\VideoSection', 'video_id', 'id')->orderBy('arrangement');
    }
}
