<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $fillable = [
        'name',
        'description',
        'slug',
        'image',
        'show',
	];

    /**
     * Related to many blogs
     */
    public function blogs() {
        return $this->hasMany('App\Blog');
    }
}
