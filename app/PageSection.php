<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageSection extends Model
{
    protected $fillable = [
        'page_id',
        'arrangement',
        'div_styling',
        'div_class',
        'div_id',
        'div_image_background',
        'section_style'
	];

    /**
     * Get parent page
     */
    public function page() {
        return $this->belongsTo('App\SitePage', 'page_id');
    }

    /**
     * Get content children
     */
    public function contents() {
        return $this->hasMany('App\PageContent', 'page_section_id')->orderBy('arrangement');
    }

    /**
     * Get feature block children
     */
    public function feature_blocks() {
        return $this->hasMany('App\PageFeatureBlock', 'page_section_id')->orderBy('arrangement');
    }

    /**
     * Get feature section children
     */
    public function feature_sections() {
        return $this->hasMany('App\PageFeatureSection', 'page_section_id')->orderBy('arrangement');
    }
}
