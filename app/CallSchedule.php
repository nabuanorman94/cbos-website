<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallSchedule extends Model
{
    protected $fillable = [
        'date',
        'time_start',
        'time_end',
        'status',
        'content'
    ];
}
