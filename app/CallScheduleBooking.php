<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallScheduleBooking extends Model
{
    protected $fillable = [
        'call_schedule_id',
        'name',
        'email',
        'phone',
        'details',
        'token'
    ];
}
