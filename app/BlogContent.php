<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogContent extends Model
{
    protected $fillable = [
        'blog_section_id',
        'arrangement',
        'div_styling',
        'div_class',
        'div_id',
        'value'
	];
}
