<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'contact_no',
        'email',
        'company_name',
        'replied'
    ];
}
