<?php 

namespace App\Traits;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\User;
use App\UserRole;

trait UserTrait
{
    public function getUser() {
        return User::where('id', auth()->user()->id)->with(['role'])->first();
    }

    public function removeRefreshToken($token_id) {
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $token_id)
            ->delete();
    }
}