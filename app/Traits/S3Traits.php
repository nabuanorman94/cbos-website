<?php 

namespace App\Traits;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\SiteFaq;

trait S3Traits
{   
    public function uploadImageToS3($image) {
        $path = Storage::disk('s3')->put($image->folder, $image->file, 'public');
        
        return Storage::disk('s3')->url($path);
    }

    public function removeImageFromS3($path) {
        $my_path = str_replace('https://cbos-site-bucket.s3.ap-northeast-1.amazonaws.com/', '', $path);
        return Storage::disk('s3')->delete($my_path);
    }

    public function duplicateImage($path) {
        $my_path = str_replace('https://cbos-site-bucket.s3.ap-northeast-1.amazonaws.com/', '', $path);
        $new_folder = config('app.aws_bucket_folder').'/dupplicates/'.Carbon::now()->timestamp.'/'.$my_path;

        Storage::disk('s3')->copy($my_path, $new_folder);
        return 'https://cbos-site-bucket.s3.ap-northeast-1.amazonaws.com/'.$new_folder;
    }

    // Used only when updating page/blog slug, also updates faq_type
    public function updateFaqType($old, $new) {
        return SiteFaq::where('faq_type', $old)->update(['faq_type' => $new]);
    }

    public function removeFaqs($slug) {
        return SiteFaq::where('faq_type', $slug)->delete();
    }
}