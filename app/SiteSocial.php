<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteSocial extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'icon',
        'link',
	];
}
