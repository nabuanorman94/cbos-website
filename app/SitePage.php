<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SitePage extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'seo_serp',
        'seo_keywords',
        'seo_image',
        'page_type',
        'show',
        'sub_title',
        'banner_image',
        'banner_image_mobile',
        'heading_position',
        'bg_color'
	];

    /**
     * Get the page sections
     */
    public function pageSection() {
        return $this->hasMany('App\PageSection', 'page_id', 'id')->orderBy('arrangement');
    }
}
