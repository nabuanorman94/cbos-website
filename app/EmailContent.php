<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailContent extends Model
{
    protected $fillable = [
        'content',
        'email_type',
        'subject'
    ];
}
