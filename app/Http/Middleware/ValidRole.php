<?php

namespace App\Http\Middleware;

use Closure;

class ValidRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        dd($roles);
        if(in_array(auth()->user()->role_name, $roles)) {
            return $next($request);
        } else {
            return abort(404);
        }
    }
}
