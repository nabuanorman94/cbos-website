<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\UserTrait;

class AuthController extends Controller
{
    use UserTrait;
    
    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $http = new \GuzzleHttp\Client;

        try {
            $credentials = [
                'username' => $request->username,
                'password' => $request->password
            ];

            // dd(auth()->attempt(['username' => $request->username, 'password' => $request->password]));

            if (auth()->attempt($credentials)) {
                // Authorize only with users who are verified (Email or Phone)
                
                // Login and grant access token to user
                $response = $http->post($request->client_login_endpoint, [
                    'form_params' => [
                        'grant_type' => 'password',
                        'client_id' => $request->client_id,
                        'client_secret' => $request->client_secret,
                        'username' => $request->username,
                        'password' => $request->password,
                    ]
                ]);
                $token = json_decode($response->getBody(), true);
                // return $response->getBody();
                // dd($response->getBody());
                return response()->json([
                    'user' => $this->getUser(),
                    'auth' => $token
                ], 200);
            } else {
                return response()->json(['error' => 'These credentials do not match our records.'], 200);
            }
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return response()->json($e, $e->getCode());
        }
    }

    /**
     * Logouts User details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout (Request $request) {
        $client_id = (isset($request->client_id))? $request->client_id : config('services.passport.client_id') ;

        auth()->user()->tokens->each(function ($token, $key) use ($client_id) {
            if($token->client_id == $client_id) {
                $token->delete();
                // Delete refresh token
                $this->removeRefreshToken($token->id);
            }
        });

        return response()->json('Logged out successfully', 200);
    }
}
