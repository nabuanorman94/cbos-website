<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home')->with([
            'breadcrumbs_title' => 'Admin Dashboard',
            'breadcrumbs_links' => [
                [ 'name' => 'Home', 'route' => 'home' ]
            ],
            'nav_links' => [
                [ 'name' => 'Site Details', 'tab' => 'site-details' ],
                [ 'name' => 'About Us', 'tab' => 'about-us' ],
                [ 'name' => 'Partners', 'tab' => 'partners' ],
                [ 'name' => 'Services', 'tab' => 'services' ],
                [ 'name' => 'Testimonials', 'tab' => 'testimonials' ],
                [ 'name' => 'Team', 'tab' => 'team' ],
                [ 'name' => 'Pricing', 'tab' => 'pricing' ],
                [ 'name' => 'FAQs', 'tab' => 'faq' ],
                [ 'name' => 'Social Links', 'tab' => 'socials' ],
            ]
        ]);
    }
}
