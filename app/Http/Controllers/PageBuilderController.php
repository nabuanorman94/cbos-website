<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\SitePage;
use App\PageContent;
use App\PageFeatureBlock;
use App\PageFeatureSection;
use App\PageReviewSection;
use App\PageSection;
use App\PageType;
use App\PageBanner;
use App\SiteFaq;
use App\SiteContent;
use App\SiteDetails;
use App\SiteSocial;
use App\Traits\S3Traits;

class PageBuilderController extends Controller
{
    use S3Traits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // ******************************* All Pages ******************************* //
    public function allPages() {
        return view('dashboard.admin.manage-site.pages.all')->with([
            // where('page_type', 'stand-alone')->
            'items' => SitePage::orderBy('updated_at', 'desc')->get()
        ]);
    }

    public function addPage() {
        $object = array(
            'model' => 'SitePage',
            'type' => 'page'
        );

        return view('dashboard.admin.manage-site.pages.add_page')->with([
            'item' => [],
            'data_array' => (object) $object
        ]);
    }

    public function editPage($slug) {
        $page = SitePage::where('slug', $slug)->first();
        $object = array(
            'model' => 'SitePage',
            'type' => 'page'
        );

        return view('dashboard.admin.manage-site.pages.add_page')->with([
            'item' => $page,
            'data_array' => (object) $object
        ]);
    }

    public function addOrEditPage(Request $request) {
        // dd($request->all());
        $exempted = ['model', 'type', '_token', 'banner_image_file', 'banner_image_mobile_file', 'seo_image_file'];
        $request['show'] = (isset($request->show) && $request->show == 'on')? 1 : 0;

        $page = SitePage::where('id', $request->id)->first();

        // Check if has image for SEO
        if($request->hasFile('seo_image_file')) {
            if($page && $page->seo_image) {
                $this->removeImageFromS3($page->seo_image);
            }
            $image = new Collection();
            $image->folder = config('app.aws_bucket_folder').'/pages';
            $image->file = $request->seo_image_file;
            $request['seo_image'] = $this->uploadImageToS3($image);
        }

        // Check if has image for banner
        if($request->hasFile('banner_image_file')) {
            if($page && $page->banner_image) {
                $this->removeImageFromS3($page->banner_image);
            }
            $image = new Collection();
            $image->folder = config('app.aws_bucket_folder').'/pages';
            $image->file = $request->banner_image_file;
            $request['banner_image'] = $this->uploadImageToS3($image);
        }

        // Check if has image for banner mobike
        if($request->hasFile('banner_image_mobile_file')) {
            if($page && $page->banner_image_mobile) {
                $this->removeImageFromS3($page->banner_image_mobile);
            }
            $image = new Collection();
            $image->folder = config('app.aws_bucket_folder').'/pages';
            $image->file = $request->banner_image_mobile_file;
            $request['banner_image_mobile'] = $this->uploadImageToS3($image);
        }
        
        if(isset($request->id)) {
            // Check if slug alrealy used
            if(!SitePage::where([['slug', '=', $request->slug], ['id', '!=', $request->id]])->first()) {
                // Update faq_type to new blog slug (function placed on S3Traits)
                $this->updateFaqType($page->slug, $request->slug);

                // remove seo_image
                $page->update($request->except($exempted));

                // return redirect(route('admin-manage-site-page-builder', [
                //     'type' => $request->page_type, 
                //     'slug' => ($request->slug)? $request->slug : null
                // ]));
                return redirect(route('admin-manage-site-pages-all'))->with([
                    'success' => 'Updated successfully!'
                ]);
            } else {
                return back()->with([
                    'error' => $request->slug . ' slug already used. Please use another slug for url.'
                ])->withInput();
            }
        } else {
            // Check if slug alrealy used
            if(!SitePage::where('slug', $request->slug)->first()) {
                $page = SitePage::create($request->except($exempted));

                return redirect(route('admin-manage-site-page-builder', [
                    'type' => $request->page_type, 
                    'slug' => ($page->slug)? $page->slug : null
                ]))->with([
                    'success' => 'Page added successfully. You can now add contents to this page.'
                ]);
            } else {
                return back()->with([
                    'error' => $request->slug . ' slug already used. Please use another slug for url.'
                ])->withInput();
            }
        }

        // return view('dashboard.admin.manage-site.pages.add_page')->with([
        //     'item' => $page,
        //     'data_array' => (object) $object
        // ]);
    }

    // ******************************* Page Builder (SitePage or SiteServices) ******************************* //
    public function pageBuilder($type, $slug) {
        $page = SitePage::where('slug', $slug)->first();
        $object = array(
            'model' => 'SitePage',
            'type' => 'page'
        );

        // $components = [];

        $page_sections = PageSection::where('page_id', $page->id)
            ->orderBy('arrangement', 'asc')
            ->get();

        // foreach ($page_sections as $page_section) {
        //     // Get contents
        //     if($page_section->contents) {
        //         foreach ($page_section->contents as $ps_content) {
        //             array_push($components, ['model' => 'PageComponent', 'arrangement' => $ps_content->arrangement, 'page_section_id' => $page_section->id, 'id' => $ps_content->id]);
        //         }
        //     }
        // }
        // // $count = count($page_section[0]->contents);
        // dd($components);

        return view('dashboard.admin.manage-site.pages.page_builder')->with([
            'page' => $page,
            'page_types' => PageType::all(),
            'page_sections' => $page_sections,
            'page_section_types' => (object) [
                [ 'name' => 'Content', 'model' => 'PageContent', 'description' => 'All text and image content within section.' ],
                [ 'name' => 'Feature Block', 'model' => 'PageFeatureBlock', 'description' => 'Featured image with heading and content.' ],
                [ 'name' => 'Feature Section', 'model' => 'PageFeatureSection', 'description' => 'Feature with image and content within the whole section.' ],
                // [ 'name' => 'Review Section', 'model' => 'PageReviewSection', 'description' => 'Review section with image, name, position, and review content.' ],
            ],
            // Count number of components the section has
            
        ]);
    }

    public function sectionAddContent($section_id, $content_id) {
        $section = PageSection::where('id', $section_id)->with('page')->first();
        $object = array(
            'model' => 'PageContent',
            'page_section_id' => $section_id,
            'arrangement' => $section->arrangement,
            'page_id' => $section->page->id,
            'page_type' => $section->page->page_type
        );

        if($section) {
            return view('dashboard.admin.manage-site.page-components.content')->with([
                'section' => $section,
                'page' => $section->page,
                'content' => PageContent::where('id', $content_id)->first(),
                'data_array' => (object) $object,
                'page_types' => PageType::all()
            ]);
        } else {
            return abort(404);
        }
    }

    public function sectionAddFeatureBlock($section_id, $feature_block_id) {
        $section = PageSection::where('id', $section_id)->with('page')->first();
        $object = array(
            'model' => 'PageFeatureBlock',
            'page_section_id' => $section_id,
            'arrangement' => $section->arrangement,
            'page_id' => $section->page->id,
            'page_slug' => $section->page->slug,
            'page_type' => $section->page->page_type,
        );

        if($section) {
            return view('dashboard.admin.manage-site.page-components.feature_image')->with([
                'section' => $section,
                'page' => $section->page,
                'feature_block' => PageFeatureBlock::where('id', $feature_block_id)->first(),
                'data_array' => (object) $object,
                'page_types' => PageType::all()
            ]);
        } else {
            return abort(404);
        }
    }

    public function sectionAddFeatureSection($section_id, $feature_section_id) {
        $section = PageSection::where('id', $section_id)->with('page')->first();
        $object = array(
            'model' => 'PageFeatureSection',
            'page_section_id' => $section_id,
            'arrangement' => $section->arrangement,
            'page_id' => $section->page->id,
            'page_slug' => $section->page->slug,
            'page_type' => $section->page->page_type,
        );

        if($section) {
            return view('dashboard.admin.manage-site.page-components.feature_section')->with([
                'section' => $section,
                'page' => $section->page,
                'feature_section' => PageFeatureSection::where('id', $feature_section_id)->first(),
                'data_array' => (object) $object,
                'page_types' => PageType::all()
            ]);
        } else {
            return abort(404);
        }
    }

    public function previewPage($slug) {
        $page = SitePage::where('slug', $slug)
            ->with('pageSection')
            ->first();

        if($page) {
            return view('view_page')->with([
                'page' => $page,
                'page_sections' => PageSection::where('page_id', $page->id)->orderBy('arrangement', 'asc')->get(),
                'faqs' => SiteFaq::where('faq_type', $page->slug)->get(),
                'details' => SiteDetails::first(),
                'site_content' => SiteContent::first(),
                'socials' => SiteSocial::orderBy('id', 'asc')->get(),
                'page_type' => ($page->page_type == 'stand-alone')? 'page' : 'article'
            ]);
        } else {
            return abort(404);
        }
    }

    // Make generic CRUD for all models
    // Create
    public function createData(Request $request) {
        // dd($request->all());
        $exempted = ['model', 'type', '_token', 'section_type'];
        // Dynamic Validation enable if $request->type is present
        if(isset($request->model) && $request->model != null) {
            $this->validate($request, $this->getvalidation($request->model)->rules, $this->getvalidation($request->model)->messages);
        }

        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $create = $model->create($request->except($exempted));

        if($create) {
            switch($request->model) {
                case 'PageSection':
                    // Check what component is selected
                    switch($request->section_type) {
                        case 'PageContent':
                            $content = PageContent::create([
                                'page_section_id' => $create->id,
                                'arrangement' => (PageContent::where('page_section_id', $create->id)->count() + 1),
                                'value' => 'Your content here.'
                            ]);
                            return redirect(route('pb-content', [
                                'section_id' => $create->id, 
                                'content_id' => $content->id
                            ]));
                            break;
                        case 'PageFeatureBlock':
                            $feature_block = PageFeatureBlock::create([
                                'page_section_id' => $create->id,
                                'arrangement' => (PageFeatureBlock::where('page_section_id', $create->id)->count() + 1),
                                'feat_content' => 'Feature content here.',
                            ]);
                            return redirect(route('pb-feature-block', [
                                'section_id' => $create->id, 
                                'feature_block_id' => $feature_block->id
                            ]));
                            break;
                        case 'PageFeatureSection':
                            $feature_section = PageFeatureSection::create([
                                'page_section_id' => $create->id,
                                'arrangement' => (PageFeatureSection::where('page_section_id', $create->id)->count() + 1),
                                'feat_content' => 'Feature content here.',
                                'image_placement' => 'left'
                            ]);
                            return redirect(route('pb-feature-section', [
                                'section_id' => $create->id, 
                                'feature_section_id' => $feature_section->id
                            ]));
                            break;
                    }
                    break;
                case 'PageContent':
                    return redirect(route('admin-manage-site-page-builder', [
                        'type' => $request->page_type, 
                        'slug' => ($create->slug)? $create->slug : null
                    ]));
                    break;
                case 'PageFeatureBlock':
                    return redirect(route('admin-manage-site-page-builder', [
                        'type' => $request->page_type, 
                        'slug' => ($create->slug)? $create->slug : null
                    ]));
                    break;
                case 'PageFeatureSection':
                    return redirect(route('admin-manage-site-page-builder', [
                        'type' => $request->page_type, 
                        'slug' => ($create->slug)? $create->slug : null
                    ]));
                    break;
                default:
                    return back()->with([
                        'success' => 'Added successfully!'
                    ]);
                    break;
            }
            
        } else {
            return back()->with([
                'error' => 'Error while creating. Please try again.'
            ]);
        }
    }

    // Update
    public function updateData(Request $request) {
        // dd($request->all());
        $exempted = ['model', 'type', '_token', 'section_type'];
        // Dynamic Validation enable if $request->type is present
        if(isset($request->model) && $request->model != null) {
            $this->validate($request, $this->getvalidation($request->model)->rules, $this->getvalidation($request->model)->messages);

            // Check if form has image/file | Check via $request->type
            switch($request->model) {
                case 'PageSection':
                    // array_push($exempted, 'logo_file', 'logo_2_file', 'logo_3_file', 'old_logo', 'old_logo_2', 'old_logo_3');
                    // // Main Logo
                    // if($request->hasFile('logo_file')) {
                    //     $image = new Collection();
                    //     $image->folder = config('app.aws_bucket_folder').'/site-details/logo';
                    //     $image->file = $request->logo_file;
                    //     $request['logo'] = $this->uploadImageToS3($image);
                    // }
                    break;
                case 'PageContent':
                    array_push($exempted, 'page_id', 'section_style', 'page_type');
                    break;
                case 'PageFeatureBlock':
                    array_push($exempted, 'page_id', 'section_style', 'feat_image_file', 'page_type', 'page_slug');
                    if($request->hasFile('feat_image_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/pages';
                        $image->file = $request->feat_image_file;
                        $request['feat_image'] = $this->uploadImageToS3($image);
                    }
                    break;
                case 'PageFeatureSection':
                    array_push($exempted, 'page_id', 'section_style', 'feat_image_file', 'page_type', 'page_slug');
                    if($request->hasFile('feat_image_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/pages';
                        $image->file = $request->feat_image_file;
                        $request['feat_image'] = $this->uploadImageToS3($image);
                    }
                    break;
            }
        }

        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $update = $model->where('id', $request->id)->update($request->except($exempted));

        if($update) {
            switch($request->model) {
                case 'PageSection':
                    // Check what component is selected
                    if(isset($request->section_type)) {
                        switch($request->section_type) {
                            case 'PageContent':
                                $content = PageContent::create([
                                    'page_section_id' => $request->id,
                                    'arrangement' => (PageContent::where('page_section_id', $request->id)->count() + 1),
                                    'value' => 'Your content here.'
                                ]);
                                return redirect(route('pb-content', [
                                    'section_id' => $request->id, 
                                    'content_id' => $content->id
                                ]));
                                break;
                            case 'PageFeatureBlock':
                                $feature_block = PageFeatureBlock::create([
                                    'page_section_id' => $request->id,
                                    'arrangement' => (PageFeatureBlock::where('page_section_id', $request->id)->count() + 1),
                                    'feat_content' => 'Feature content here.',
                                ]);
                                return redirect(route('pb-feature-block', [
                                    'section_id' => $request->id, 
                                    'feature_block_id' => $feature_block->id
                                ]));
                                break;
                            case 'PageFeatureSection':
                                $feature_section = PageFeatureSection::create([
                                    'page_section_id' => $request->id,
                                    'arrangement' => (PageFeatureSection::where('page_section_id', $request->id)->count() + 1),
                                    'feat_content' => 'Feature content here.',
                                    'image_placement' => 'left'
                                ]);
                                return redirect(route('pb-feature-section', [
                                    'section_id' => $request->id, 
                                    'feature_section_id' => $feature_section->id
                                ]));
                                break;
                        }
                    } else {
                        return back()->with([
                            'success' => 'Updated successfully!'
                        ]);
                    }
                    break;
                case 'PageContent':
                    $page = SitePage::where('id', $request->page_id)->first();
                    return redirect(route('admin-manage-site-page-builder', [
                        'type' => $request->page_type, 
                        'slug' => ($page)? $page->slug : null
                    ]));
                    break;
                case 'PageFeatureBlock':
                    $page = SitePage::where('id', $request->page_id)->first();
                    return redirect(route('admin-manage-site-page-builder', [
                        'type' => $request->page_type, 
                        'slug' => ($page)? $page->slug : null
                    ]));
                    break;
                case 'PageFeatureSection':
                    $page = SitePage::where('id', $request->page_id)->first();
                    return redirect(route('admin-manage-site-page-builder', [
                        'type' => $request->page_type, 
                        'slug' => ($page)? $page->slug : null
                    ]));
                    break;
                default:
                    return back()->with([
                        'success' => 'Updated successfully!'
                    ]);
                    break;
            }
            
        } else {
            return back()->with([
                'error' => 'Error while updating. Please try again.'
            ]);
        }
    }

    public function removeData(Request $request) {
        // dd($request->all());
        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $data = $model->where('id', $request->id)->first();

        if($data) {
            switch($request->model) {
                case 'SitePage':
                    $sections = PageSection::where('page_id', $data->id)->get();
                    if($sections) {
                        foreach ($sections as $section) {
                            PageContent::where('page_section_id', $section->id)->delete();
                            PageFeatureBlock::where('page_section_id', $section->id)->delete();
                            PageFeatureSection::where('page_section_id', $section->id)->delete();
                            PageReviewSection::where('page_section_id', $section->id)->delete();
                            $section->delete();
                        }
                    }
                    break;
                case 'PageSection':
                    PageContent::where('page_section_id', $data->id)->delete();
                    PageFeatureBlock::where('page_section_id', $data->id)->delete();
                    PageFeatureSection::where('page_section_id', $data->id)->delete();
                    PageReviewSection::where('page_section_id', $data->id)->delete();
                    break;
            }

        }

        $remove = $data->delete();

        if($remove) {
            return back()->with([
                'success' => 'Removed Successfully!'
            ]);
        } else {
            return back()->with([
                'error' => 'Error while removing data. Please try again.'
            ]);
        }
    }

    // Dynamic Validation Function accepts $request->type
    public function getvalidation($model, $update = false) {
        $rules = [];
        $messages = [];

        switch($model) {
            // Site Details
            case 'SitePage':
                $rules = [
                    'title' => 'required',
                    'slug' => 'required',
                ];
        
                $messages = [
                    'title.required' => 'Page title is required.',
                    'slug.required' => 'Page slug is required.',
                ];
                break;
            
            // case 'service':
            //     $rules = [
            //         'name' => 'required',
            //         // 'icon' => 'required',
            //         // 'details' => 'required',
            //         // 'content_1' => 'required',
            //         // 'seo_serp' => 'required',
            //         // 'seo_keywords' => 'required',
            //         'service_type' => 'required',
            //     ];
        
            //     $messages = [
            //         'name.required' => 'Service name is required.',
            //         'icon.required' => 'Header color is required.',
            //         'details.required' => 'Details is required.',
            //         'content_1.required' => 'Content is required.',
            //         'seo_serp.required' => 'SEO serp is required.',
            //         'seo_keywords.required' => 'SEO keywords is required.',
            //         'service_type.required' => 'Service type is required.',
            //     ];
            //     break;
            // case 'faq':
            //     $rules = [
            //         'question' => 'required',
            //         'answer' => 'required',
            //         'faq_type' => 'required',
            //     ];
        
            //     $messages = [
            //         'question.required' => 'Question is required.',
            //         'answer.required' => 'Answer is required.',
            //         'faq_type.required' => 'FAQ type is required.',
            //     ];
            //     break;
            // case 'pricing':
            //     $rules = [
            //         'package_name' => 'required',
            //         // 'icon' => 'required',
            //         // 'details' => 'required',
            //         // 'content_1' => 'required',
            //         // 'seo_serp' => 'required',
            //         // 'seo_keywords' => 'required',
            //         'pricing_type' => 'required',
            //     ];
        
            //     $messages = [
            //         'package_name.required' => 'Package name is required.',
            //         // 'icon.required' => 'Header color is required.',
            //         // 'details.required' => 'Details is required.',
            //         // 'content_1.required' => 'Content is required.',
            //         // 'seo_serp.required' => 'SEO serp is required.',
            //         // 'seo_keywords.required' => 'SEO keywords is required.',
            //         'pricing_type.required' => 'Type is required.',
            //     ];
            //     break;
        }

        return (object) [ 'rules' => $rules, 'messages' => $messages ];
    }

    // public function updateSwitch(Request $request) {
    //     $exempted = ['model', 'type', '_token'];

    //     $model_name = '\\App\\'.$request->model;
    //     $model = new $model_name;

    //     $update = $model->where('id', $request->id)->update($request->except($exempted));

    //     dd($update);

    //     return response()->json([
    //         'success' => 'Updated successfully.'
    //     ]);
    // }

    public function duplicatePage(Request $request) {
        $old_page = SitePage::find($request->old_page_id);

        if($old_page) {
            // Create New Page with new Page Title
            $new_page = $old_page->replicate();
            $new_page->title = $request->new_page_title;
            $new_page->slug = $request->new_page_slug;
            $new_page->banner_image = ($old_page->banner_image)? $this->duplicateImage($old_page->banner_image) : null;
            $new_page->banner_image_mobile = ($old_page->banner_image_mobile)? $this->duplicateImage($old_page->banner_image_mobile) : null;
            $new_page->seo_image = ($old_page->seo_image)? $this->duplicateImage($old_page->seo_image) : null;
            $new_page->show = 0;
            $new_page->save();

            // Get all page sections and duplicate
            $old_page_sections = PageSection::where('page_id', $old_page->id)->get();

            if($old_page_sections) {
                foreach ($old_page_sections as $ps_item) {
                    if($ps_item) {
                        $new_page_section = $ps_item->replicate();
                        $new_page_section->div_image_background = ($ps_item->div_image_background)? $this->duplicateImage($ps_item->div_image_background, $request->new_page_slug) : null;
                        $new_page_section->page_id = $new_page->id;
                        $new_page_section->save();

                        // Check and duplicate all page section components
                        // PageContent
                        $old_ps_page_contents = PageContent::where('page_section_id', $ps_item->id)->get();
                        if($old_ps_page_contents) {
                            foreach ($old_ps_page_contents as $old_ps_page_content) {
                                $new_ps_page_content = $old_ps_page_content->replicate();
                                $new_ps_page_content->page_section_id = $new_page_section->id;
                                $new_ps_page_content->save();
                            }
                        }
                        // PageFeatureBlock
                        $old_ps_page_feature_blocks = PageFeatureBlock::where('page_section_id', $ps_item->id)->get();
                        if($old_ps_page_feature_blocks) {
                            foreach ($old_ps_page_feature_blocks as $old_ps_page_feature_block) {
                                $new_ps_page_feature_block = $old_ps_page_feature_block->replicate();
                                $new_ps_page_feature_block->page_section_id = $new_page_section->id;
                                $new_ps_page_feature_block->feat_image = ($old_ps_page_feature_block->feat_image)? $this->duplicateImage($old_ps_page_feature_block->feat_image) : null;
                                $new_ps_page_feature_block->save();
                            }
                        }
                        // PageFeatureSection
                        $old_ps_page_feature_sections = PageFeatureSection::where('page_section_id', $ps_item->id)->get();
                        if($old_ps_page_feature_sections) {
                            foreach ($old_ps_page_feature_sections as $old_ps_page_feature_section) {
                                $new_ps_page_feature_section = $old_ps_page_feature_section->replicate();
                                $new_ps_page_feature_section->page_section_id = $new_page_section->id;
                                $new_ps_page_feature_section->feat_image = ($old_ps_page_feature_section->feat_image)? $this->duplicateImage($old_ps_page_feature_section->feat_image) : null;
                                $new_ps_page_feature_section->save();
                            }
                        }
                    }
                }
            }

            return back()->with([
                'success' => 'Page Duplicated Successfully!'
            ]);
        } else {
            return back()->with([
                'error' => 'Error while duplicating page. Please try again.'
            ]);
        }
    }

    public function duplicateSection(Request $request) {
        // Get all page sections and duplicate
        $old_page_section = PageSection::find($request->page_section_id);

        if($old_page_section) {
            $new_page_section = $old_page_section->replicate();
            $new_page_section->div_image_background = ($old_page_section->div_image_background)? $this->duplicateImage($old_page_section->div_image_background) : null;
            $new_page_section->page_id = $old_page_section->page_id;
            $new_page_section->arrangement = PageSection::where('page_id', $old_page_section->page_id)->count() + 1;
            $new_page_section->save();

            // Check and duplicate all page section components
            // PageContent
            $old_ps_page_contents = PageContent::where('page_section_id', $old_page_section->id)->get();
            if($old_ps_page_contents) {
                foreach ($old_ps_page_contents as $old_ps_page_content) {
                    $new_ps_page_content = $old_ps_page_content->replicate();
                    $new_ps_page_content->page_section_id = $new_page_section->id;
                    $new_ps_page_content->save();
                }
            }
            // PageFeatureBlock
            $old_ps_page_feature_blocks = PageFeatureBlock::where('page_section_id', $old_page_section->id)->get();
            if($old_ps_page_feature_blocks) {
                foreach ($old_ps_page_feature_blocks as $old_ps_page_feature_block) {
                    $new_ps_page_feature_block = $old_ps_page_feature_block->replicate();
                    $new_ps_page_feature_block->page_section_id = $new_page_section->id;
                    $new_ps_page_feature_block->feat_image = ($old_ps_page_feature_block->feat_image)? $this->duplicateImage($old_ps_page_feature_block->feat_image) : null;
                    $new_ps_page_feature_block->save();
                }
            }
            // PageFeatureSection
            $old_ps_page_feature_sections = PageFeatureSection::where('page_section_id', $old_page_section->id)->get();
            if($old_ps_page_feature_sections) {
                foreach ($old_ps_page_feature_sections as $old_ps_page_feature_section) {
                    $new_ps_page_feature_section = $old_ps_page_feature_section->replicate();
                    $new_ps_page_feature_section->page_section_id = $new_page_section->id;
                    $new_ps_page_feature_section->feat_image = ($old_ps_page_feature_section->feat_image)? $this->duplicateImage($old_ps_page_feature_section->feat_image) : null;
                    $new_ps_page_feature_section->save();
                }
            }
            return back()->with([
                'success' => 'Section Duplicated Successfully!'
            ]);
        } else {
            return back()->with([
                'error' => 'Error while duplicating section. Please try again.'
            ]);
        }
    }

    public function duplicateComponent(Request $request) {
        // Check model 
        switch($request->model) {
            case 'PageContent':
                $old_page_content = PageContent::find($request->component_id);
                if($old_page_content) {
                    $new_page_content = $old_page_content->replicate();
                    $new_page_content->arrangement = PageContent::where('page_section_id', $old_page_content->page_section_id)->count() + 1;
                    $new_page_content->save();

                    return back()->with([
                        'success' => 'Content Duplicated Successfully!'
                    ]);
                } else {
                    return back()->with([
                        'error' => 'Error while duplicating content. Please try again.'
                    ]);
                }
                break;
            case 'PageFeatureBlock':
                $old_page_feature_block = PageFeatureBlock::find($request->component_id);
                if($old_page_feature_block) {
                    $new_page_feature_block = $old_page_feature_block->replicate();
                    $new_page_feature_block->arrangement = PageFeatureBlock::where('page_section_id', $old_page_feature_block->page_section_id)->count() + 1;
                    $new_page_feature_block->feat_image = ($old_page_feature_block->feat_image)? $this->duplicateImage($old_page_feature_block->feat_image) : null;
                    $new_page_feature_block->save();

                    return back()->with([
                        'success' => 'Feature Block Duplicated Successfully!'
                    ]);
                } else {
                    return back()->with([
                        'error' => 'Error while duplicating feature block. Please try again.'
                    ]);
                }
                break;
            case 'PageFeatureSection':
                $old_page_feature_section = PageFeatureSection::find($request->component_id);
                if($old_page_feature_section) {
                    $new_page_feature_section = $old_page_feature_section->replicate();
                    $new_page_feature_section->arrangement = PageFeatureSection::where('page_section_id', $old_page_feature_section->page_section_id)->count() + 1;
                    $new_page_feature_section->feat_image = ($old_page_feature_section->feat_image)? $this->duplicateImage($old_page_feature_section->feat_image) : null;
                    $new_page_feature_section->save();

                    return back()->with([
                        'success' => 'Feature Section Duplicated Successfully!'
                    ]);
                } else {
                    return back()->with([
                        'error' => 'Error while duplicating feature section. Please try again.'
                    ]);
                }
                break;
            default:
                return back()->with([
                    'error' => 'Error while duplicating component. Please try again.'
                ]);
                break;
        }
    }

    public function uploadImage(Request $request) {
        if($request->hasFile('image')) {
            $image = new Collection();
            $image->folder = config('app.aws_bucket_folder').$request->folder;
            $image->file = $request->image;
            $url = $this->uploadImageToS3($image);
            return $url;
        }
    }

    public function uploadVideo(Request $request) {
        if($request->hasFile('video')) {
            $video = new Collection();
            $video->folder = config('app.aws_bucket_folder').$request->folder;
            $video->file = $request->video;
            $url = $this->uploadImageToS3($video);
            return $url;
        }
    }
}
