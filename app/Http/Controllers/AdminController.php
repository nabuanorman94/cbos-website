<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\SiteAboutUs;
use App\SiteBanner;
use App\SiteContent;
use App\SiteDetails;
use App\SiteFaq;
use App\SitePartner;
use App\SitePricing;
// use App\SiteService;
use App\SiteSocial;
use App\SiteTeamMember;
use App\SiteTestimonial;
use App\ContactUs;
use App\Inquiry;
use App\SitePage;
use App\Qotd;
use App\Newsletter;
use App\EmailContent;
use App\CallSchedule;
use App\CallScheduleBooking;

use App\Mail\ContactUsToUser;
use App\Mail\ContactUsToAdmin;
use App\Mail\GetStarted;
use App\Mail\GetStartedToAdmin;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    // Admin Dashboard Home
    public function index() {
        return view('dashboard.admin.home');
    }

    public function showNotificationsPage() {
        return view('dashboard.notifications');
    }

    public function showEmailContentsPage() {
        $object = array(
            'model' => 'EmailContent',
            'type' => 'email_content'
        );

        return view('dashboard.admin.manage-site.email_contents')
            ->with([
                'items' => EmailContent::orderBy('created_at', 'desc')->paginate(5),
                'details' => SiteDetails::first(),
                'data_array' => (object) $object
            ]);
    }

    public function showQuestionsDailyPage() {
        return view('dashboard.admin.articles-videos.question_for_the_day')
            ->with([
                'qotds' => Qotd::all()
            ]);
    }

    public function showQuestionsDailyPageCreateUpdate(Request $request) {
        if(isset($request->id)) {
            $update = Qotd::find($request->id)->update($request->except('_token'));
            if($update) {
                return back()->with([
                    'success' => 'Updated successfully!'
                ]);
            } else {
                return back()->with([
                    'error' => 'Error while updating data. Please try again.'
                ]);
            }
        } else {
            $create = Qotd::create($request->except('_token'));

            if($create) {
                return back()->with([
                    'success' => 'Added successfully!'
                ]);
            } else {
                return back()->with([
                    'error' => 'Error while creating data. Please try again.'
                ]);
            }
        }
    }

    public function showVideosPage() {
        return view('dashboard.admin.articles-videos.videos');
    }

    public function showManageClientsPage() {
        return view('dashboard.admin.manage_clients');
    }

    public function showManageUsersPage() {
        return view('dashboard.admin.manage_users');
    }

    public function showMyTeamPage() {
        return view('dashboard.admin.my_team');
    }

    public function showNewslettersPage() {
        $items = Newsletter::where('status', 1)->orderBy('created_at', 'desc')->get();
        $object = array(
            'model' => 'Newsletter',
            'type' => 'newsletter'
        );

        return view('dashboard.admin.newsletters')->with([
            'items' => $items,
            'data_array' => (object) $object
        ]);
    }

    public function showWebminarsPage() {
        return view('dashboard.admin.webminars');
    }

    public function showInvoicesPage() {
        return view('dashboard.admin.invoices');
    }

    public function showCallScheduleAllPage() {
        return view('dashboard.admin.call_schedules')
        ->with([
            'schedules' => []
        ]);
    }

    public function showCallScheduleManagePage($date) {
        $active_dates = CallSchedule::groupBy('date')->select('date')->whereRaw('MONTH(date) = MONTH(NOW())')->get()->pluck('date');

        return view('dashboard.admin.call_schedule_manage')
        ->with([
            'schedules' => CallSchedule::where('date', $date)->get(),
            'date' => $date,
            'active_dates' => $active_dates
        ]);
    }

    public function showReportsPage() {
        return view('dashboard.admin.reports');
    }

    // Contact Us
    public function showContactUsPage() {
        $items = ContactUs::orderBy('created_at', 'desc')->get();
        $object = array(
            'model' => 'ContactUs',
            'type' => 'contact-us'
        );

        return view('dashboard.admin.contact_us')->with([
            'items' => $items,
            'data_array' => (object) $object
        ]);
    }

    // Inquiries
    public function showInquiriesPage() {
        $items = Inquiry::orderBy('created_at', 'desc')->get();
        $object = array(
            'model' => 'Inquiry',
            'type' => 'inquiry'
        );

        return view('dashboard.admin.inquiries')->with([
            'items' => $items,
            'data_array' => (object) $object
        ]);
    }

    // Dashboard views for Manage Site Details -- Admin
    // ******************************* Site Details ******************************* //
    public function showSiteDetailsPage() {
        $details = SiteDetails::first();
        $object = array(
            'model' => 'SiteDetails',
            'type' => 'details',
            'old_logo' => ($details)? $details->logo : null,
            'old_logo_2' => ($details)? $details->logo_2 : null,
            'old_logo_3' => ($details)? $details->logo_3 : null,
        );

        return view('dashboard.admin.manage-site.details')->with([
            'item' => $details,
            'data_array' => (object) $object
        ]);
    }
    
    // ******************************* Site Content ******************************* //
    public function showSiteContentPage() {
        $content = SiteContent::first();
        $object = array(
            'model' => 'SiteContent',
            'type' => 'content',
            'old_feature_1_image' => ($content)? $content->feature_1_image : null,
            'old_feature_2_image' => ($content)? $content->feature_2_image : null,
            'old_feature_3_image' => ($content)? $content->feature_3_image : null,
        );

        return view('dashboard.admin.manage-site.content')->with([
            'item' => $content,
            'data_array' => (object) $object
        ]);
    }

    // ******************************* Site Banners ******************************* //
    public function showSiteBannersPage() {
        return view('dashboard.admin.manage-site.banners')->with([
            'items' => SiteBanner::orderBy('updated_at', 'desc')->get()
        ]);
    }

    public function addSiteBannersPage() {
        $object = array(
            'model' => 'SiteBanner',
            'type' => 'banner'
        );

        return view('dashboard.admin.manage-site.banners_add')->with([
            'item' => [],
            'data_array' => (object) $object
        ]);
    }

    public function editSiteBannersPage($id) {
        $banner = SiteBanner::where('id', $id)->first();
        $object = array(
            'model' => 'SiteBanner',
            'type' => 'banner',
            'old_image' => ($banner)? $banner->image : null,
            'old_image_mobile' => ($banner)? $banner->image_mobile : null,
        );

        return view('dashboard.admin.manage-site.banners_add')->with([
            'item' => $banner,
            'data_array' => (object) $object
        ]);
    }

    // ******************************* Site About Us ******************************* //
    public function showSiteAboutUsPage() {
        $about_us = SiteAboutUs::first();
        $object = array(
            'model' => 'SiteAboutUs',
            'type' => 'about_us',
            'old_image' => ($about_us)? $about_us->image : null
        );

        return view('dashboard.admin.manage-site.about_us')->with([
            'item' => $about_us,
            'data_array' => (object) $object
        ]);
    }
    
    // ******************************* Site Partners ******************************* //
    public function showSitePartnersPage() {
        // $partner = ($id)? SitePartner::where('id', $id)->first() : null;

        $object = array(
            'model' => 'SitePartner',
            'type' => 'partner'
        );

        return view('dashboard.admin.manage-site.partners')->with([
            'items' => SitePartner::orderBy('id', 'desc')->get(),
            'data_array' => (object) $object
        ]);
    }

    // ******************************* Site Services ******************************* //
    // public function showSiteServicesPage() {
    //     return view('dashboard.admin.manage-site.services')->with([
    //         'items' => SitePage::where([['page_type', '!=', 'stand-alone']])->orderBy('updated_at', 'desc')->get()
    //     ]);
    // }

    // public function addSiteServicesPage() {
    //     $object = array(
    //         'model' => 'SiteService',
    //         'type' => 'service'
    //     );

    //     return view('dashboard.admin.manage-site.services_add')->with([
    //         'item' => [],
    //         'data_array' => (object) $object
    //     ]);
    // }

    // public function editSiteServicesPage($id) {
    //     $service = SiteService::where('id', $id)->first();
    //     $object = array(
    //         'model' => 'SiteService',
    //         'type' => 'service',
    //         'old_image_1' => ($service)? $service->image_1 : null,
    //         'old_image_2' => ($service)? $service->image_2 : null,
    //         'old_seo_image' => ($service)? $service->seo_image : null,
    //         'old_image_mobile' => ($service)? $service->image_mobile : null,
    //     );

    //     return view('dashboard.admin.manage-site.services_add')->with([
    //         'item' => $service,
    //         'data_array' => (object) $object
    //     ]);
    // }

    // public function previewService($slug) {
    //     $service = SiteService::where('slug', $slug)->first();
    //     $faqs = SiteFaq::where('faq_type', $slug)->get();
    //     $pricings = SitePricing::where('pricing_type', $service->service_type)->get();

    //     if($service) {
    //         return view('view_service_page')->with([
    //             'breadcrumbs_title' => $service->name,
    //             'breadcrumbs_links' => [
    //                 [
    //                     'name' => 'Home',
    //                     'route' => 'homepage'
    //                 ]
    //             ],
    //             'service' => $service,
    //             'faqs' => $faqs,
    //             'pricings' => $pricings
    //         ]);
    //     } else {
    //         return abort(404);
    //     }
    // }

    // ******************************* Site Testimonials ******************************* //
    public function showSiteTestimonialsPage() {
        return view('dashboard.admin.manage-site.testimonials')->with([
            'items' => SiteTestimonial::orderBy('id', 'desc')->get()
        ]);
    }

    public function addSiteTestimonialsPage() {
        $object = array(
            'model' => 'SiteTestimonial',
            'type' => 'testimonial'
        );

        return view('dashboard.admin.manage-site.testimonial_add')->with([
            'item' => [],
            'data_array' => (object) $object
        ]);
    }

    public function editSiteTestimonialsPage($id) {
        $testimonial = SiteTestimonial::where('id', $id)->first();
        $object = array(
            'model' => 'SiteTestimonial',
            'type' => 'testimonial',
            'old_image' => ($testimonial)? $testimonial->image : null
        );

        return view('dashboard.admin.manage-site.testimonial_add')->with([
            'item' => $testimonial,
            'data_array' => (object) $object
        ]);
    }

    // ******************************* Site Team ******************************* //
    public function showSiteTeamPage() {
        return view('dashboard.admin.manage-site.team')->with([
            'items' => SiteTeamMember::orderBy('id', 'asc')->get()
        ]);
    }

    public function addSiteTeamPage() {
        $object = array(
            'model' => 'SiteTeamMember',
            'type' => 'team'
        );

        return view('dashboard.admin.manage-site.team_add')->with([
            'item' => [],
            'data_array' => (object) $object
        ]);
    }

    public function editSiteTeamPage($id) {
        $team = SiteTeamMember::where('id', $id)->first();
        $object = array(
            'model' => 'SiteTeamMember',
            'type' => 'team',
            'old_image' => $team->image
        );

        return view('dashboard.admin.manage-site.team_add')->with([
            'item' => $team,
            'data_array' => (object) $object
        ]);
    }

    // ******************************* Site Pricing ******************************* //
    public function showSitePricingPage() {
        return view('dashboard.admin.manage-site.pricing')->with([
            'items' => SitePricing::orderBy('id', 'desc')->get()
        ]);
    }

    public function addSitePricingPage() {
        $object = array(
            'model' => 'SitePricing',
            'type' => 'pricing',
            'is_best_value' => 'false'
        );

        return view('dashboard.admin.manage-site.pricing_add')->with([
            'item' => [],
            'data_array' => (object) $object
        ]);
    }

    public function editSitePricingPage($id) {
        $pricing = SitePricing::where('id', $id)->first();
        $object = array(
            'model' => 'SitePricing',
            'type' => 'pricing'
        );

        return view('dashboard.admin.manage-site.pricing_add')->with([
            'item' => $pricing,
            'data_array' => (object) $object
        ]);
    }

    // ******************************* Site FAQs ******************************* //
    public function showSiteFAQsPage() {
        return view('dashboard.admin.manage-site.faqs')->with([
            'items' => SiteFaq::orderBy('id', 'asc')->get()
        ]);
    }

    public function addSiteFAQsPage() {
        $object = array(
            'model' => 'SiteFaq',
            'type' => 'faq'
        );

        return view('dashboard.admin.manage-site.faq_add')->with([
            'item' => [],
            'data_array' => (object) $object,
            'pages' => SitePage::where([['show', '=', 1], ['page_type', '!=', 'stand-alone']])->orderBy('id', 'asc')->get()
        ]);
    }

    public function editSiteFAQsPage($id) {
        $faq = SiteFaq::where('id', $id)->first();
        $object = array(
            'model' => 'SiteFaq',
            'type' => 'faq'
        );

        return view('dashboard.admin.manage-site.faq_add')->with([
            'item' => $faq,
            'data_array' => (object) $object,
            'pages' => SitePage::where([['show', '=', 1], ['page_type', '!=', 'stand-alone']])->orderBy('id', 'asc')->get()
        ]);
    }

    // ******************************* Site Socials ******************************* //
    public function showSiteSocialLinksPage() {
        return view('dashboard.admin.manage-site.socials')->with([
            'items' => SiteSocial::orderBy('id', 'asc')->get()
        ]);
    }

    public function addSiteSocialPage() {
        $object = array(
            'model' => 'SiteSocial',
            'type' => 'social'
        );

        return view('dashboard.admin.manage-site.socials_add')->with([
            'item' => [],
            'data_array' => (object) $object
        ]);
    }

    public function editSiteSocialPage($id) {
        $social = SiteSocial::where('id', $id)->first();
        $object = array(
            'model' => 'SiteSocial',
            'type' => 'social'
        );

        return view('dashboard.admin.manage-site.socials_add')->with([
            'item' => $social,
            'data_array' => (object) $object
        ]);
    }

    // Manage Other Data CRUD
    public function createOtherData(Request $request) {
        $exempted = ['model', 'type', '_token'];

        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $request['show'] = (isset($request->show) && $request->show == 'on')? 1 : 0;
        
        $create = $model->create($request->except($exempted));

        if($create) {
            return back()->with([
                'success' => 'Added successfully!'
            ]);
        } else {
            return back()->with([
                'error' => 'Error while creating data. Please try again.'
            ]);
        }
    }

    public function updateOtherData(Request $request) {
        $exempted = ['model', 'type', '_token'];

        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $update = $model->find($request->id)->update($request->except($exempted));

        if($update) {
            return back()->with([
                'success' => 'Update successfully!'
            ]);
        } else {
            return back()->with([
                'error' => 'Error while updating data. Please try again.'
            ]);
        }
    }

    public function removeOtherData(Request $request) {
        // dd($request->all());
        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $data = $model->where('id', $request->id)->first();

        $remove = $data->delete();

        if($remove) {
            return back()->with([
                'success' => 'Removed Successfully!'
            ]);
        } else {
            return back()->with([
                'error' => 'Error while removing data. Please try again.'
            ]);
        }
    }

    public function updateSwitch(Request $request) {
        $exempted = ['model', 'type', '_token'];

        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $update = $model->where('id', $request->id)->update($request->except($exempted));

        return response()->json([
            'success' => 'Updated successfully.'
        ]);
    }

    public function sendTestEmail(Request $request) {
        $email_content = EmailContent::find($request->id);

        if($email_content) {
            switch ($email_content->email_type) {
                case 'Contact Us':
                    Mail::to($request->to_email)->send(new ContactUsToUser($request->name, $email_content, SiteDetails::first()));
                    break;
                case 'Get Started':
                    Mail::to($request->to_email)->send(new GetStarted($request->name, $email_content, SiteDetails::first()));
                    break;
            }

            return back()->with([
                'success' => 'Test email sent successfully.'
            ]);
        } else {
            return back()->with([
                'error' => 'Error while sending test email. Please try again.'
            ]);
        }
    }

    public function createCallSchedulesForTheDay(Request $request) {
        // dd($request->all());
        // '2010-01-09T12:30:00'
        $time = [
            // ['8:00:00', '8:30:00'],
            // ['8:30:00', '9:00:00'],
            // ['9:00:00', '9:30:00'],
            // ['9:30:00', '10:00:00'],
            // ['10:00:00', '10:30:00'],
            // ['10:30:00', '11:00:00'],
            // ['11:00:00', '11:30:00'],
            // ['11:30:00', '12:00:00'],
            // ['1:00:00', '1:30:00'],
            // ['1:30:00', '2:00:00'],
            // ['2:30:00', '3:00:00'],
            // ['3:00:00', '3:30:00'],
            // ['3:30:00', '4:00:00'],
            // ['4:00:00', '4:30:00'],
            // ['4:30:00', '5:00:00']
            ['8:00am', '8:30am'],
            ['8:30am', '9:00am'],
            ['9:00am', '9:30am'],
            ['9:30am', '10:00am'],
            ['10:00am', '10:30am'],
            ['10:30am', '11:00am'],
            ['11:00am', '11:30am'],
            ['11:30am', '12:00pm'],
            ['1:00pm', '1:30pm'],
            ['1:30pm', '2:00pm'],
            ['2:30pm', '3:00pm'],
            ['3:00pm', '3:30pm'],
            ['3:30pm', '4:00pm'],
            ['4:00pm', '4:30pm'],
            ['4:30pm', '5:00pm']
        ];

        foreach ($time as $item) {
            CallSchedule::create([
                'date' => $request->schedule_date,
                // 'time_start' => $request->schedule_date . 'T' . $item[0],
                // 'time_end' => $request->schedule_date . 'T' . $item[1],
                'time_start' => $item[0],
                'time_end' => $item[1],
                'status' => 1
            ]);
        }

        return back()->with([
            'success' => 'Schedules created successfully.'
        ]);
    }
}
