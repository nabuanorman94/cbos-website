<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\SiteAboutUs;
use App\SiteBanner;
use App\SiteContent;
use App\SiteDetails;
use App\SiteFaq;
use App\SitePage;
use App\SitePartner;
use App\SitePricing;
// use App\SiteService;
use App\SiteSocial;
use App\SiteTeamMember;
use App\SiteTestimonial;
use App\Traits\S3Traits;

class SiteController extends Controller
{
    use S3Traits;

    // For admin controllers managing site details || No need for Traits

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Make generic CRUD for all models

    // Create
    public function createData(Request $request) {
        $exempted = ['model', 'type', '_token', 'to_upload_video', 'to_upload_video_2'];
        // Dynamic Validation enable if $request->type is present
        if(isset($request->type) && $request->type != null) {
            $this->validate($request, $this->getvalidation($request->type)->rules, $this->getvalidation($request->type)->messages);

            // Check if form has image/file | Check via $request->type
            switch($request->type) {
                case 'details':
                    array_push($exempted, 'logo_file', 'logo_2_file', 'logo_3_file', 'old_logo', 'old_logo_2', 'old_logo_3');
                    // Main Logo
                    if($request->hasFile('logo_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/site-details/logo';
                        $image->file = $request->logo_file;
                        $request['logo'] = $this->uploadImageToS3($image);
                    }
                    // Dashboard Logo
                    if($request->hasFile('logo_2_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/site-details/logo';
                        $image->file = $request->logo_2_file;
                        $request['logo_2'] = $this->uploadImageToS3($image);
                    }
                    // Footer Logo
                    if($request->hasFile('logo_3_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/site-details/logo';
                        $image->file = $request->logo_3_file;
                        $request['logo_3'] = $this->uploadImageToS3($image);
                    }
                    break;
                case 'about_us':
                    array_push($exempted, 'image_file', 'old_image');
                    if($request->hasFile('image_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/about-us/image';
                        $image->file = $request->image_file;
                        $request['image'] = $this->uploadImageToS3($image);
                    }
                    $request['section_1_list'] = 'Default';
                    break;
                case 'partner':
                    array_push($exempted, 'image_file');
                    if($request->hasFile('image_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/partners/image';
                        $image->file = $request->image_file;
                        $request['logo'] = $this->uploadImageToS3($image);
                    }
                    $request['section_1_list'] = 'Default';
                    break;
                case 'service':
                    array_push($exempted, 'image_1_file', 'image_2_file', 'seo_image_file', 'image_mobile_file');
                    // Header Image
                    if($request->hasFile('image_1_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/services';
                        $image->file = $request->image_1_file;
                        $request['image_1'] = $this->uploadImageToS3($image);
                    }
                    // Mobile Header Image
                    if($request->hasFile('image_mobile_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/services';
                        $image->file = $request->image_mobile_file;
                        $request['image_mobile'] = $this->uploadImageToS3($image);
                    }
                    // Dashboard Logo
                    if($request->hasFile('image_2_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/services';
                        $image->file = $request->image_2_file;
                        $request['image_2'] = $this->uploadImageToS3($image);
                    }
                    // Footer Logo
                    if($request->hasFile('seo_image_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/services';
                        $image->file = $request->seo_image_file;
                        $request['seo_image'] = $this->uploadImageToS3($image);
                    }

                    // Make Service name a slug
                    $request['slug'] = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $request->name), '-'));
                    $request['show'] = (isset($request->show) && $request->show == 'on')? 1 : 0;
                    break;
                case 'team':
                    array_push($exempted, 'image_file');
                    if($request->hasFile('image_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/team';
                        $image->file = $request->image_file;
                        $request['image'] = $this->uploadImageToS3($image);
                    }
                    $request['show'] = (isset($request->show) && $request->show == 'on')? 1 : 0;
                    break;
                case 'testimonial':
                    array_push($exempted, 'image_file');
                    if($request->hasFile('image_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/testimonials';
                        $image->file = $request->image_file;
                        $request['image'] = $this->uploadImageToS3($image);
                    }
                    $request['show'] = (isset($request->show) && $request->show == 'on')? 1 : 0;
                    break;
                case 'pricing':
                    if($request->inclusions == null) {
                        $request['inclusions'] = 'Basic';
                    }
                    $request['show'] = (isset($request->show) && $request->show == 'on')? 1 : 0;
                    break;
                case 'banner':
                    array_push($exempted, 'image_file', 'image_mobile_file');
                    // Header
                    if($request->hasFile('image_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/banners';
                        $image->file = $request->image_file;
                        $request['image'] = $this->uploadImageToS3($image);
                    }
                    // Mobile Header Image
                    if($request->hasFile('image_mobile_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/banners';
                        $image->file = $request->image_mobile_file;
                        $request['image_mobile'] = $this->uploadImageToS3($image);
                    }
                    $request['show'] = (isset($request->show) && $request->show == 'on')? 1 : 0;
                    break;
                case 'content':
                    array_push($exempted, 'feature_1_image_file', 'feature_2_image_file', 'feature_3_image_file');
                    // Feature Image 1
                    if($request->hasFile('feature_1_image_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/site-content/features';
                        $image->file = $request->feature_1_image_file;
                        $request['feature_1_image'] = $this->uploadImageToS3($image);
                    }
                    // Feature Image 2
                    if($request->hasFile('feature_2_image_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/site-content/features';
                        $image->file = $request->feature_2_image_file;
                        $request['feature_2_image'] = $this->uploadImageToS3($image);
                    }
                    // Feature Image 3
                    if($request->hasFile('feature_3_image_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/site-content/features/';
                        $image->file = $request->feature_3_image_file;
                        $request['feature_3_image'] = $this->uploadImageToS3($image);
                    }
                    $request['show_about_us_section'] = (isset($request->show_about_us_section) && $request->show_about_us_section == 'on')? 1 : 0;
                    $request['show_testimonials_section'] = (isset($request->show_testimonials_section) && $request->show_testimonials_section == 'on')? 1 : 0;
                    $request['show_partners_section'] = (isset($request->show_partners_section) && $request->show_partners_section == 'on')? 1 : 0;
                    $request['show_content_1_section'] = (isset($request->show_content_1_section) && $request->show_content_1_section == 'on')? 1 : 0;
                    $request['show_content_2_section'] = (isset($request->show_content_2_section) && $request->show_content_2_section == 'on')? 1 : 0;
                    $request['show_faq_section'] = (isset($request->show_faq_section) && $request->show_faq_section == 'on')? 1 : 0;
                    $request['show_pricing_section'] = (isset($request->show_pricing_section) && $request->show_pricing_section == 'on')? 1 : 0;
                    $request['show_team_section'] = (isset($request->show_team_section) && $request->show_team_section == 'on')? 1 : 0;
                    break;
                case 'email_content':
                    array_push($exempted, 'id');
                    break;
            }
        }

        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $create = $model->create($request->except($exempted));

        if($create) {
            switch($request->type) {
                // case 'service':
                //     return redirect()->route('admin-manage-site-services')->with([
                //         'services' => SiteService::orderBy('id', 'desc')->get(),
                //         'success' => $request->name . ' service added successfully!'
                //     ]);
                //     break;
                case 'testimonial':
                    return redirect()->route('admin-manage-site-testimonials')->with([
                        'testimonials' => SiteTestimonial::orderBy('id', 'desc')->get(),
                        'success' => $request->name . "'s". ' testimonial added successfully!'
                    ]);
                    break;
                case 'team':
                    return redirect()->route('admin-manage-site-team')->with([
                        'teams' => SiteTeamMember::orderBy('id', 'desc')->get(),
                        'success' => $request->name . ' added successfully!'
                    ]);
                    break;
                case 'faq':
                    return redirect()->route('admin-manage-site-faqs')->with([
                        'teams' => SiteFaq::orderBy('id', 'desc')->get(),
                        'success' => 'FAQ added successfully!'
                    ]);
                    break;
                case 'pricing':
                    return redirect()->route('admin-manage-site-pricing')->with([
                        'pricings' => SitePricing::orderBy('id', 'desc')->get(),
                        'success' => 'Pricing added successfully!'
                    ]);
                case 'social':
                    return redirect()->route('admin-manage-site-socials')->with([
                        'socials' => SiteSocial::orderBy('id', 'desc')->get(),
                        'success' => 'Social link added successfully!'
                    ]);
                    break;
                case 'banner':
                    return redirect()->route('admin-manage-site-banners')->with([
                        'banners' => SiteBanner::orderBy('id', 'desc')->get(),
                        'success' => 'Banner added successfully!'
                    ]);
                    break;
                default:
                    return back()->with([
                        'success' => 'Added successfully!'
                    ]);
                    break;
            }
        } else {
            return back()->with([
                'error' => 'Error while creating. Please try again.'
            ]);
        }
    }

    // Update
    public function updateData(Request $request) {
        $exempted = ['model', 'type', '_token', 'to_upload_video', 'to_upload_video_2'];
        // dd($request->all());
        // Dynamic Validation enable if $request->type is present
        if(isset($request->type) && $request->type != null) {
            $this->validate($request, $this->getvalidation($request->type, true)->rules, $this->getvalidation($request->type, true)->messages);

            // Check if form has image/file | Check via $request->type
            switch($request->type) {
                case 'details':
                    array_push($exempted, 'logo_file', 'logo_2_file', 'logo_3_file', 'old_logo', 'old_logo_2', 'old_logo_3');
                    // Main Logo
                    if($request->hasFile('logo_file')) {
                        // remove old logo
                        if($request->old_logo) {
                            $this->removeImageFromS3($request->old_logo);
                        }

                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/site-details/logo';
                        $image->file = $request->logo_file;
                        $request['logo'] = $this->uploadImageToS3($image);
                    }
                    // Dashboard Logo
                    if($request->hasFile('logo_2_file')) {
                        // remove old logo
                        if($request->old_logo_2) {
                            $this->removeImageFromS3($request->old_logo_2);
                        }

                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/site-details/logo';
                        $image->file = $request->logo_2_file;
                        $request['logo_2'] = $this->uploadImageToS3($image);
                    }
                    // Footer Logo
                    if($request->hasFile('logo_3_file')) {
                        // remove old logo
                        if($request->old_logo_3) {
                            $this->removeImageFromS3($request->old_logo_3);
                        }

                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/site-details/logo';
                        $image->file = $request->logo_3_file;
                        $request['logo_3'] = $this->uploadImageToS3($image);
                    }
                    break;
                case 'about_us':
                    array_push($exempted, 'image_file', 'old_image');
                    if($request->hasFile('image_file')) {
                        // remove old logo
                        if($request->old_image) {
                            $this->removeImageFromS3($request->old_image);
                        }

                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/about-us/image';
                        $image->file = $request->image_file;
                        $request['image'] = $this->uploadImageToS3($image);
                    }
                    $request['section_1_list'] = 'Default';
                    break;
                case 'service':
                    array_push($exempted, 'image_1_file', 'image_2_file', 'seo_image_file', 'image_mobile_file', 'old_image_1', 'old_image_2', 'old_seo_image', 'old_image_mobile');
                    // Main Logo
                    if($request->hasFile('image_1_file')) {
                        if($request->old_image_1) {
                            $this->removeImageFromS3($request->old_image_1);
                        }
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/services';
                        $image->file = $request->image_1_file;
                        $request['image_1'] = $this->uploadImageToS3($image);
                    }
                    // Mobile Header Image
                    if($request->hasFile('image_mobile_file')) {
                        if($request->old_image_mobile) {
                            $this->removeImageFromS3($request->old_image_mobile);
                        }
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/services';
                        $image->file = $request->image_mobile_file;
                        $request['image_mobile'] = $this->uploadImageToS3($image);
                    }
                    // Dashboard Logo
                    if($request->hasFile('image_2_file')) {
                        if($request->old_image_2) {
                            $this->removeImageFromS3($request->old_image_2);
                        }
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/services';
                        $image->file = $request->image_2_file;
                        $request['image_2'] = $this->uploadImageToS3($image);
                    }
                    // Footer Logo
                    if($request->hasFile('seo_image_file')) {
                        if($request->old_seo_image) {
                            $this->removeImageFromS3($request->old_seo_image);
                        }
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/services';
                        $image->file = $request->seo_image_file;
                        $request['seo_image'] = $this->uploadImageToS3($image);
                    }

                    $request['slug'] = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $request->name), '-'));
                    $request['show'] = (isset($request->show) && $request->show == 'on')? 1 : 0;
                    break;
                case 'team':
                    array_push($exempted, 'image_file', 'old_image');
                    if($request->hasFile('image_file')) {
                        if($request->old_image) {
                            $this->removeImageFromS3($request->old_image);
                        }
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/team';
                        $image->file = $request->image_file;
                        $request['image'] = $this->uploadImageToS3($image);
                    }
                    $request['show'] = (isset($request->show) && $request->show == 'on')? 1 : 0;
                    break;
                case 'testimonial':
                    array_push($exempted, 'image_file', 'old_image');
                    if($request->hasFile('image_file')) {
                        if($request->old_image) {
                            $this->removeImageFromS3($request->old_image);
                        }

                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/testimonials';
                        $image->file = $request->image_file;
                        $request['image'] = $this->uploadImageToS3($image);
                    }
                    $request['show'] = (isset($request->show) && $request->show == 'on')? 1 : 0;
                    break;
                case 'pricing':
                    if($request->inclusions == null) {
                        $request['inclusions'] = 'Basic';
                    }
                    $request['show'] = (isset($request->show) && $request->show == 'on')? 1 : 0;
                    break;
                case 'banner':
                    array_push($exempted, 'image_file', 'old_image', 'image_mobile_file', 'old_image_mobile');
                    // Header
                    if($request->hasFile('image_file')) {
                        if($request->old_image) {
                            $this->removeImageFromS3($request->old_image);
                        }

                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/banners';
                        $image->file = $request->image_file;
                        $request['image'] = $this->uploadImageToS3($image);
                    }
                    // Mobile Header Image
                    if($request->hasFile('image_mobile_file')) {
                        if($request->old_image_mobile) {
                            $this->removeImageFromS3($request->old_image_mobile);
                        }
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/banners';
                        $image->file = $request->image_mobile_file;
                        $request['image_mobile'] = $this->uploadImageToS3($image);
                    }
                    $request['show'] = (isset($request->show) && $request->show == 'on')? 1 : 0;
                    break;
                case 'content':
                    array_push($exempted, 'feature_1_image_file', 'feature_2_image_file', 'feature_3_image_file','old_feature_1_image', 'old_feature_2_image', 'old_feature_3_image');
                    // Feature Image 1
                    if($request->hasFile('feature_1_image_file')) {
                        if($request->old_feature_1_image) {
                            $this->removeImageFromS3($request->old_feature_1_image);
                        }

                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/site-content/features';
                        $image->file = $request->feature_1_image_file;
                        $request['feature_1_image'] = $this->uploadImageToS3($image);
                    }
                    // Feature Image 2
                    if($request->hasFile('feature_2_image_file')) {
                        if($request->old_feature_2_image) {
                            $this->removeImageFromS3($request->old_feature_2_image);
                        }

                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/site-content/features';
                        $image->file = $request->feature_2_image_file;
                        $request['feature_2_image'] = $this->uploadImageToS3($image);
                    }
                    // Feature Image 3
                    if($request->hasFile('feature_3_image_file')) {
                        if($request->old_feature_3_image) {
                            $this->removeImageFromS3($request->old_feature_3_image);
                        }

                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/site-content/features';
                        $image->file = $request->feature_3_image_file;
                        $request['feature_3_image'] = $this->uploadImageToS3($image);
                    }

                    $request['show_about_us_section'] = (isset($request->show_about_us_section) && $request->show_about_us_section == 'on')? 1 : 0;
                    $request['show_testimonials_section'] = (isset($request->show_testimonials_section) && $request->show_testimonials_section == 'on')? 1 : 0;
                    $request['show_partners_section'] = (isset($request->show_partners_section) && $request->show_partners_section == 'on')? 1 : 0;
                    $request['show_content_1_section'] = (isset($request->show_content_1_section) && $request->show_content_1_section == 'on')? 1 : 0;
                    $request['show_content_2_section'] = (isset($request->show_content_2_section) && $request->show_content_2_section == 'on')? 1 : 0;
                    $request['show_faq_section'] = (isset($request->show_faq_section) && $request->show_faq_section == 'on')? 1 : 0;
                    $request['show_pricing_section'] = (isset($request->show_pricing_section) && $request->show_pricing_section == 'on')? 1 : 0;
                    $request['show_team_section'] = (isset($request->show_team_section) && $request->show_team_section == 'on')? 1 : 0;
                    $request['show_qotd_section'] = (isset($request->show_qotd_section) && $request->show_qotd_section == 'on')? 1 : 0;
                    $request['show_recent_blogs_section'] = (isset($request->show_recent_blogs_section) && $request->show_recent_blogs_section == 'on')? 1 : 0;
                    break;
            }
        }

        // dd($exempted);
        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $update = $model->where('id', $request->id)->update($request->except($exempted));

        if($update) {
            switch($request->type) {
                // case 'service':
                //     return redirect()->route('admin-manage-site-services')->with([
                //         'services' => SiteService::orderBy('id', 'desc')->get(),
                //         'info' => $request->name . ' service updated successfully!'
                //     ]);
                //     break;
                case 'testimonial':
                    return redirect()->route('admin-manage-site-testimonials')->with([
                        'testimonials' => SiteTestimonial::orderBy('id', 'desc')->get(),
                        'info' => $request->name . "'s " . 'testimonial updated successfully!'
                    ]);
                    break;
                case 'team':
                    return redirect()->route('admin-manage-site-team')->with([
                        'teams' => SiteTeamMember::orderBy('id', 'desc')->get(),
                        'info' => $request->name . ' updated successfully!'
                    ]);
                    break;
                case 'faq':
                    return redirect()->route('admin-manage-site-faqs')->with([
                        'faqs' => SiteFaq::orderBy('id', 'desc')->get(),
                        'info' => 'FAQ updated successfully!'
                    ]);
                    break;
                case 'pricing':
                    return redirect()->route('admin-manage-site-pricing')->with([
                        'pricings' => SitePricing::orderBy('id', 'desc')->get(),
                        'info' => 'Pricing updated successfully!'
                    ]);
                    break;
                case 'social':
                    return redirect()->route('admin-manage-site-socials')->with([
                        'socials' => SiteSocial::orderBy('id', 'desc')->get(),
                        'info' => 'Social link updated successfully!'
                    ]);
                case 'banner':
                    return redirect()->route('admin-manage-site-banners')->with([
                        'banners' => SiteBanner::orderBy('id', 'desc')->get(),
                        'info' => 'Banner updated successfully!'
                    ]);
                default:
                    return back()->with([
                        'info' => 'Updated successfully!'
                    ]);
                    break;
            }
        } else {
            return back()->with([
                'error' => 'Error while updating. Please try again.'
            ]);
        }
    }

    public function removeData(Request $request) {
        // dd($request->all());
        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $data = $model->where('id', $request->id)->first();

        if($data) {
            switch($request->type) {
                case 'partner':
                    $this->removeImageFromS3($data->logo);
                    break;
                case 'service':
                    $this->removeImageFromS3($data->image_1);
                    $this->removeImageFromS3($data->image_2);
                    $this->removeImageFromS3($data->seo_image);
                    break;
                // To Add Other Model with images ////////////////////// ******************************** ????????????????????????????????????????????????????????????????????????? ++++++++++++++++++++++++++++++++++++++++
            }

        }

        $remove = $data->delete();

        if($remove) {
            return back()->with([
                'success' => 'Removed Successfully!'
            ]);
        } else {
            return back()->with([
                'error' => 'Error while removing data. Please try again.'
            ]);
        }
    }

    // Dynamic Validation Function accepts $request->type
    public function getvalidation($type, $update = false) {
        $rules = [];
        $messages = [];

        switch($type) {
            // Site Details
            case 'details':
                $rules = [
                    // 'name' => ($update)? 'required' : 'required|unique:pages',
                    'name' => 'required',
                    'description' => 'required',
                    // 'logo_file' => 'required',
                    'email' => 'required',
                    'phone' => 'required',
                    'open_on' => 'required',
                    'address' => 'required'
                ];
        
                $messages = [
                    'name.required' => 'Company name is required.',
                    'description.required' => 'Description is required.',
                    // 'logo_file.required' => 'Logo is required.',
                    'email.required' => 'Email is required.',
                    'phone.required' => 'Phone is required.',
                    'open_on.required' => 'Office hours is required.',
                    'address.required' => 'Address is required.',
                ];
                break;
            
            case 'service':
                $rules = [
                    'name' => 'required',
                    // 'icon' => 'required',
                    // 'details' => 'required',
                    // 'content_1' => 'required',
                    // 'seo_serp' => 'required',
                    // 'seo_keywords' => 'required',
                    'service_type' => 'required',
                ];
        
                $messages = [
                    'name.required' => 'Service name is required.',
                    'icon.required' => 'Header color is required.',
                    'details.required' => 'Details is required.',
                    'content_1.required' => 'Content is required.',
                    'seo_serp.required' => 'SEO serp is required.',
                    'seo_keywords.required' => 'SEO keywords is required.',
                    'service_type.required' => 'Service type is required.',
                ];
                break;
            case 'faq':
                $rules = [
                    'question' => 'required',
                    'answer' => 'required',
                    'faq_type' => 'required',
                ];
        
                $messages = [
                    'question.required' => 'Question is required.',
                    'answer.required' => 'Answer is required.',
                    'faq_type.required' => 'FAQ type is required.',
                ];
                break;
            case 'pricing':
                $rules = [
                    'package_name' => 'required',
                    // 'icon' => 'required',
                    // 'details' => 'required',
                    // 'content_1' => 'required',
                    // 'seo_serp' => 'required',
                    // 'seo_keywords' => 'required',
                    'pricing_type' => 'required',
                ];
        
                $messages = [
                    'package_name.required' => 'Package name is required.',
                    // 'icon.required' => 'Header color is required.',
                    // 'details.required' => 'Details is required.',
                    // 'content_1.required' => 'Content is required.',
                    // 'seo_serp.required' => 'SEO serp is required.',
                    // 'seo_keywords.required' => 'SEO keywords is required.',
                    'pricing_type.required' => 'Type is required.',
                ];
                break;
        }

        return (object) [ 'rules' => $rules, 'messages' => $messages ];
    }
}
