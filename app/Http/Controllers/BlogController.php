<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Blog;
use App\BlogCategory;
use App\BlogSection;
use App\BlogContent;
use App\BlogFeatureBlock;
use App\BlogFeatureSection;
use App\PageType;
use App\SiteFaq;
use App\SiteContent;
use App\SiteDetails;
use App\SiteSocial;
use App\Traits\S3Traits;

class BlogController extends Controller
{
    use S3Traits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Show Blog Page
    public function showBlogsPage($category) {
        switch($category) {
            case 'all':
                $blogs = Blog::with('blogCategory')->orderBy('created_at', 'desc')->paginate(12);
                break;
            default:
                $blog_category = BlogCategory::where('slug', $category)->first();
                if($blog_category) {
                    $blogs = Blog::where('blog_category_id', $blog_category->id)->with('blogCategory')->orderBy('created_at', 'desc')->paginate(12);
                } else {
                    $blogs = [];
                }
                break;
        }
        return view('dashboard.admin.articles-videos.blogs')->with([
            'categories' => BlogCategory::where('show', 1)->orderBy('name', 'asc')->get(),
            'slug' => $category,
            'blogs' => $blogs
        ]);
    }

    public function showBlogsCategoriesPage() {
        return view('dashboard.admin.articles-videos.manage_blog_categories')->with([
            'items' => BlogCategory::orderBy('name', 'asc')->get()
        ]);
    }

    public function addBlogPage() {
        $object = array(
            'model' => 'Blog'
        );

        return view('dashboard.admin.articles-videos.blog_add')->with([
            'item' => [],
            'categories' => BlogCategory::where('show', 1)->orderBy('name', 'asc')->get(),
            'data_array' => (object) $object
        ]);
    }

    public function editBlogPage($slug) {
        $object = array(
            'model' => 'Blog'
        );

        return view('dashboard.admin.articles-videos.blog_add')->with([
            'item' => Blog::where('slug', $slug)->first(),
            'categories' => BlogCategory::where('show', 1)->orderBy('name', 'asc')->get(),
            'data_array' => (object) $object
        ]);
    }

    public function addOrEditBlog(Request $request) {
        // dd($request->all());
        $exempted = ['model', 'type', '_token', 'banner_image_file', 'banner_image_mobile_file', 'seo_image_file'];
        $request['show'] = (isset($request->show) && $request->show == 'on')? 1 : 0;

        $blog = Blog::where('id', $request->id)->first();

        // Check if has image for SEO
        if($request->hasFile('seo_image_file')) {
            if($blog && $blog->seo_image) {
                $this->removeImageFromS3($blog->seo_image);
            }
            $image = new Collection();
            $image->folder = config('app.aws_bucket_folder').'/blogs';
            $image->file = $request->seo_image_file;
            $request['seo_image'] = $this->uploadImageToS3($image);
        }

        // Check if has image for banner
        if($request->hasFile('banner_image_file')) {
            if($blog && $blog->banner_image) {
                $this->removeImageFromS3($blog->banner_image);
            }
            $image = new Collection();
            $image->folder = config('app.aws_bucket_folder').'/blogs';
            $image->file = $request->banner_image_file;
            $request['banner_image'] = $this->uploadImageToS3($image);
        }

        // Check if has image for banner mobike
        if($request->hasFile('banner_image_mobile_file')) {
            if($blog && $blog->banner_image_mobile) {
                $this->removeImageFromS3($blog->banner_image_mobile);
            }
            $image = new Collection();
            $image->folder = config('app.aws_bucket_folder').'/blogs';
            $image->file = $request->banner_image_mobile_file;
            $request['banner_image_mobile'] = $this->uploadImageToS3($image);
        }
        
        if($blog && isset($request->slug)) {
            // Check if slug alrealy used
            if(!Blog::where([['slug', '=', $request->slug], ['id', '!=', $request->id]])->first()) {
                // Update faq_type to new blog slug (function placed on S3Traits)
                $this->updateFaqType($blog->slug, $request->slug);

                // remove seo_image
                $blog->update($request->except($exempted));

                // return redirect(route('admin-resources-blog-builder', [
                //     'slug' => ($request->slug)? $request->slug : null
                // ]));
                return redirect(route('admin-resources-blogs', [
                    'category' => 'all'
                ]))->with([
                    'success' => 'Updated successfully!'
                ]);
            } else {
                return back()->with([
                    'error' => $request->slug . ' slug already used. Please use another slug for url.'
                ])->withInput();
            }
        } else {
            $request['count_view'] = 0;
            $request['count_likes'] = 0;
            // Check if slug alrealy used
            if(!Blog::where('slug', $request->slug)->first()) {
                $blog = Blog::create($request->except($exempted));

                return redirect(route('admin-resources-blog-builder', [
                    'slug' => ($blog->slug)? $blog->slug : null
                ]))->with([
                    'success' => 'Blog added successfully. You can now add contents to this blog.'
                ]);;
            } else {
                return back()->with([
                    'error' => $request->slug . ' slug already used. Please use another slug for url.'
                ])->withInput();
            }
        }
    }

    public function blogBuilder($slug) {
        $blog = Blog::where('slug', $slug)->first();
        $object = array(
            'model' => 'Blog'
        );
        $blog_sections = BlogSection::where('blog_id', $blog->id)
            ->orderBy('arrangement', 'asc')
            ->get();

        return view('dashboard.admin.articles-videos.blog_builder')->with([
            'blog' => $blog,
            'page_types' => PageType::all(),
            'categories' => BlogCategory::where('show', 1)->orderBy('name', 'asc')->get(),
            'blog_sections' => $blog_sections,
            'blog_section_types' => (object) [
                [ 'name' => 'Content', 'model' => 'BlogContent', 'description' => 'All text and image content within section.' ],
                [ 'name' => 'Feature Block', 'model' => 'BlogFeatureBlock', 'description' => 'Featured image with heading and content.' ],
                [ 'name' => 'Feature Section', 'model' => 'BlogFeatureSection', 'description' => 'Feature with image and content within the whole section.' ],
            ]
        ]);
    }

    public function sectionAddContent($section_id, $content_id) {
        $section = BlogSection::where('id', $section_id)->with('blog')->first();
        $object = array(
            'model' => 'BlogContent',
            'blog_section_id' => $section_id,
            'arrangement' => $section->arrangement,
            'blog_id' => $section->blog->id
        );

        if($section) {
            return view('dashboard.admin.articles-videos.blog-components.content')->with([
                'section' => $section,
                'blog' => $section->blog,
                'content' => BlogContent::where('id', $content_id)->first(),
                'data_array' => (object) $object,
                'page_types' => PageType::all()
            ]);
        } else {
            return abort(404);
        }
    }

    public function sectionAddFeatureBlock($section_id, $feature_block_id) {
        $section = BlogSection::where('id', $section_id)->with('blog')->first();
        $object = array(
            'model' => 'BlogFeatureBlock',
            'blog_section_id' => $section_id,
            'arrangement' => $section->arrangement,
            'blog_id' => $section->blog->id,
        );

        if($section) {
            return view('dashboard.admin.articles-videos.blog-components.feature_image')->with([
                'section' => $section,
                'blog' => $section->blog,
                'feature_block' => BlogFeatureBlock::where('id', $feature_block_id)->first(),
                'data_array' => (object) $object,
                'page_types' => PageType::all()
            ]);
        } else {
            return abort(404);
        }
    }

    public function sectionAddFeatureSection($section_id, $feature_section_id) {
        $section = BlogSection::where('id', $section_id)->with('blog')->first();
        $object = array(
            'model' => 'BlogFeatureSection',
            'blog_section_id' => $section_id,
            'arrangement' => $section->arrangement,
            'blog_id' => $section->blog->id,
        );

        if($section) {
            return view('dashboard.admin.articles-videos.blog-components.feature_section')->with([
                'section' => $section,
                'blog' => $section->blog,
                'feature_section' => BlogFeatureSection::where('id', $feature_section_id)->first(),
                'data_array' => (object) $object,
                'page_types' => PageType::all()
            ]);
        } else {
            return abort(404);
        }
    }

    public function previewBlog($slug) {
        $blog = Blog::where('slug', $slug)
            ->with('blogSection')
            ->first();

        if($blog) {
            return view('view_blog')->with([
                'blog' => $blog,
                'blog_sections' => BlogSection::where('blog_id', $blog->id)->orderBy('arrangement', 'asc')->get(),
                'faqs' => SiteFaq::where('faq_type', $blog->slug)->get(),
                'details' => SiteDetails::first(),
                'site_content' => SiteContent::first(),
                'socials' => SiteSocial::orderBy('id', 'asc')->get(),
                'page_type' => 'article',
                'breadcrumbs_title' => $blog->title,
                'breadcrumbs_links' => [
                    [
                        'name' => 'Home',
                        'route' => 'homepage'
                    ],
                    [
                        'name' => 'Home',
                        'route' => 'homepage'
                    ]
                ],
            ]);
        } else {
            return abort(404);
        }
    }

    // Make generic CRUD for all models
    // Create
    public function createData(Request $request) {
        // dd($request->all());
        $exempted = ['model', 'type', '_token', 'section_type'];
        // Dynamic Validation enable if $request->type is present
        if(isset($request->model) && $request->model != null) {
            $this->validate($request, $this->getvalidation($request->model)->rules, $this->getvalidation($request->model)->messages);
        }

        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $create = $model->create($request->except($exempted));

        if($create) {
            switch($request->model) {
                case 'BlogSection':
                    // Check what component is selected
                    switch($request->section_type) {
                        case 'BlogContent':
                            $content = BlogContent::create([
                                'blog_section_id' => $create->id,
                                'arrangement' => (BlogContent::where('blog_section_id', $create->id)->count() + 1),
                                'value' => 'Your content here.'
                            ]);
                            return redirect(route('bb-content', [
                                'section_id' => $create->id, 
                                'content_id' => $content->id
                            ]));
                            break;
                        case 'BlogFeatureBlock':
                            $feature_block = BlogFeatureBlock::create([
                                'blog_section_id' => $create->id,
                                'arrangement' => (BlogFeatureBlock::where('blog_section_id', $create->id)->count() + 1),
                                'feat_content' => 'Feature content here.',
                            ]);
                            return redirect(route('bb-feature-block', [
                                'section_id' => $create->id, 
                                'feature_block_id' => $feature_block->id
                            ]));
                            break;
                        case 'BlogFeatureSection':
                            $feature_section = BlogFeatureSection::create([
                                'blog_section_id' => $create->id,
                                'arrangement' => (BlogFeatureSection::where('blog_section_id', $create->id)->count() + 1),
                                'feat_content' => 'Feature content here.',
                                'image_placement' => 'left'
                            ]);
                            return redirect(route('bb-feature-section', [
                                'section_id' => $create->id, 
                                'feature_section_id' => $feature_section->id
                            ]));
                            break;
                    }
                    break;
                case 'BlogContent':
                    return redirect(route('admin-resources-blog-builder', [
                        'slug' => ($create)? $create->slug : null
                    ]));
                    break;
                case 'BlogFeatureBlock':
                    return redirect(route('admin-resources-blog-builder', [
                        'slug' => ($create)? $create->slug : null
                    ]));
                    break;
                case 'BlogFeatureSection':
                    return redirect(route('admin-resources-blog-builder', [
                        'slug' => ($create)? $create->slug : null
                    ]));
                    break;
                default:
                    return back()->with([
                        'success' => 'Added successfully!'
                    ]);
                    break;
            }
            
        } else {
            return back()->with([
                'error' => 'Error while creating. Please try again.'
            ]);
        }
    }

    // Update
    public function updateData(Request $request) {
        // dd($request->all());
        $exempted = ['model', 'type', '_token', 'section_type'];
        // Dynamic Validation enable if $request->type is present
        if(isset($request->model) && $request->model != null) {
            $this->validate($request, $this->getvalidation($request->model)->rules, $this->getvalidation($request->model)->messages);

            // Check if form has image/file | Check via $request->type
            switch($request->model) {
                case 'BlogContent':
                    array_push($exempted, 'blog_id', 'section_style');
                    break;
                case 'BlogFeatureBlock':
                    array_push($exempted, 'blog_id', 'section_style', 'feat_image_file', 'page_slug');
                    if($request->hasFile('feat_image_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/blogs';
                        $image->file = $request->feat_image_file;
                        $request['feat_image'] = $this->uploadImageToS3($image);
                    }
                    break;
                case 'BlogFeatureSection':
                    array_push($exempted, 'blog_id', 'section_style', 'feat_image_file', 'page_slug');
                    if($request->hasFile('feat_image_file')) {
                        $image = new Collection();
                        $image->folder = config('app.aws_bucket_folder').'/blogs';
                        $image->file = $request->feat_image_file;
                        $request['feat_image'] = $this->uploadImageToS3($image);
                    }
                    break;
            }
        }

        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $update = $model->where('id', $request->id)->update($request->except($exempted));

        if($update) {
            switch($request->model) {
                case 'BlogSection':
                    // Check what component is selected
                    if(isset($request->section_type)) {
                        switch($request->section_type) {
                            case 'BlogContent':
                                $content = BlogContent::create([
                                    'blog_section_id' => $request->id,
                                    'arrangement' => (BlogContent::where('blog_section_id', $request->id)->count() + 1),
                                    'value' => 'Your content here.'
                                ]);
                                return redirect(route('bb-content', [
                                    'section_id' => $request->id, 
                                    'content_id' => $content->id
                                ]));
                                break;
                            case 'BlogFeatureBlock':
                                $feature_block = BlogFeatureBlock::create([
                                    'blog_section_id' => $request->id,
                                    'arrangement' => (BlogFeatureBlock::where('blog_section_id', $request->id)->count() + 1),
                                    'feat_content' => 'Feature content here.',
                                ]);
                                return redirect(route('bb-feature-block', [
                                    'section_id' => $request->id, 
                                    'feature_block_id' => $feature_block->id
                                ]));
                                break;
                            case 'BlogFeatureSection':
                                $feature_section = BlogFeatureSection::create([
                                    'blog_section_id' => $request->id,
                                    'arrangement' => (BlogFeatureSection::where('blog_section_id', $request->id)->count() + 1),
                                    'feat_content' => 'Feature content here.',
                                    'image_placement' => 'left'
                                ]);
                                return redirect(route('bb-feature-section', [
                                    'section_id' => $request->id, 
                                    'feature_section_id' => $feature_section->id
                                ]));
                                break;
                        }
                    } else {
                        return back()->with([
                            'success' => 'Updated successfully!'
                        ]);
                    }
                    break;
                case 'BlogContent':
                    $blog = Blog::where('id', $request->blog_id)->first();
                    return redirect(route('admin-resources-blog-builder', [
                        'slug' => ($blog)? $blog->slug : null
                    ]));
                    break;
                case 'BlogFeatureBlock':
                    $blog = Blog::where('id', $request->blog_id)->first();
                    return redirect(route('admin-resources-blog-builder', [
                        'slug' => ($blog)? $blog->slug : null
                    ]));
                    break;
                case 'BlogFeatureSection':
                    $blog = Blog::where('id', $request->blog_id)->first();
                    return redirect(route('admin-resources-blog-builder', [
                        'slug' => ($blog)? $blog->slug : null
                    ]));
                    break;
                default:
                    return back()->with([
                        'success' => 'Updated successfully!'
                    ]);
                    break;
            }
            
        } else {
            return back()->with([
                'error' => 'Error while updating. Please try again.'
            ]);
        }
    }

    public function removeData(Request $request) {
        // dd($request->all());
        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $data = $model->where('id', $request->id)->first();

        if($data) {
            switch($request->model) {
                case 'Blog':
                    $sections = BlogSection::where('blog_id', $data->id)->get();
                    if($sections) {
                        foreach ($sections as $section) {
                            BlogContent::where('blog_section_id', $section->id)->delete();
                            BlogFeatureBlock::where('blog_section_id', $section->id)->delete();
                            BlogFeatureSection::where('blog_section_id', $section->id)->delete();
                            $section->delete();
                        }
                    }
                    break;
                case 'BlogSection':
                    BlogContent::where('blog_section_id', $data->id)->delete();
                    BlogFeatureBlock::where('blog_section_id', $data->id)->delete();
                    BlogFeatureSection::where('blog_section_id', $data->id)->delete();
                    break;
            }

        }

        $remove = $data->delete();

        if($remove) {
            return back()->with([
                'success' => 'Removed Successfully!'
            ]);
        } else {
            return back()->with([
                'error' => 'Error while removing data. Please try again.'
            ]);
        }
    }

    public function duplicatePage(Request $request) {
        $old_page = Blog::find($request->old_blog_id);

        if($old_page) {
            // Create New Page with new Page Title
            $new_page = $old_page->replicate();
            $new_page->title = $request->new_page_title;
            $new_page->slug = $request->new_page_slug;
            $new_page->banner_image = ($old_page->banner_image)? $this->duplicateImage($old_page->banner_image) : null;
            $new_page->banner_image_mobile = ($old_page->banner_image_mobile)? $this->duplicateImage($old_page->banner_image_mobile) : null;
            $new_page->seo_image = ($old_page->seo_image)? $this->duplicateImage($old_page->seo_image) : null;
            $new_page->show = 0;
            $new_page->save();

            // Get all page sections and duplicate
            $old_page_sections = BlogSection::where('blog_id', $old_page->id)->get();

            if($old_page_sections) {
                foreach ($old_page_sections as $ps_item) {
                    if($ps_item) {
                        $new_page_section = $ps_item->replicate();
                        $new_page_section->div_image_background = ($ps_item->div_image_background)? $this->duplicateImage($ps_item->div_image_background, $request->new_page_slug) : null;
                        $new_page_section->blog_id = $new_page->id;
                        $new_page_section->save();

                        // Check and duplicate all page section components
                        // BlogContent
                        $old_ps_page_contents = BlogContent::where('blog_section_id', $ps_item->id)->get();
                        if($old_ps_page_contents) {
                            foreach ($old_ps_page_contents as $old_ps_page_content) {
                                $new_ps_page_content = $old_ps_page_content->replicate();
                                $new_ps_page_content->blog_section_id = $new_page_section->id;
                                $new_ps_page_content->save();
                            }
                        }
                        // BlogFeatureBlock
                        $old_ps_page_feature_blocks = BlogFeatureBlock::where('blog_section_id', $ps_item->id)->get();
                        if($old_ps_page_feature_blocks) {
                            foreach ($old_ps_page_feature_blocks as $old_ps_page_feature_block) {
                                $new_ps_page_feature_block = $old_ps_page_feature_block->replicate();
                                $new_ps_page_feature_block->blog_section_id = $new_page_section->id;
                                $new_ps_page_feature_block->feat_image = ($old_ps_page_feature_block->feat_image)? $this->duplicateImage($old_ps_page_feature_block->feat_image) : null;
                                $new_ps_page_feature_block->save();
                            }
                        }
                        // BlogFeatureSection
                        $old_ps_page_feature_sections = BlogFeatureSection::where('blog_section_id', $ps_item->id)->get();
                        if($old_ps_page_feature_sections) {
                            foreach ($old_ps_page_feature_sections as $old_ps_page_feature_section) {
                                $new_ps_page_feature_section = $old_ps_page_feature_section->replicate();
                                $new_ps_page_feature_section->blog_section_id = $new_page_section->id;
                                $new_ps_page_feature_section->feat_image = ($old_ps_page_feature_section->feat_image)? $this->duplicateImage($old_ps_page_feature_section->feat_image) : null;
                                $new_ps_page_feature_section->save();
                            }
                        }
                    }
                }
            }

            return back()->with([
                'success' => 'Blog Duplicated Successfully!'
            ]);
        } else {
            return back()->with([
                'error' => 'Error while duplicating blog. Please try again.'
            ]);
        }
    }

    public function duplicateSection(Request $request) {
        // Get all page sections and duplicate
        $old_page_section = BlogSection::find($request->blog_section_id);

        if($old_page_section) {
            $new_page_section = $old_page_section->replicate();
            $new_page_section->div_image_background = ($old_page_section->div_image_background)? $this->duplicateImage($old_page_section->div_image_background) : null;
            $new_page_section->blog_id = $old_page_section->blog_id;
            $new_page_section->arrangement = BlogSection::where('blog_id', $old_page_section->blog_id)->count() + 1;
            $new_page_section->save();

            // Check and duplicate all page section components
            // BlogContent
            $old_ps_page_contents = BlogContent::where('blog_section_id', $old_page_section->id)->get();
            if($old_ps_page_contents) {
                foreach ($old_ps_page_contents as $old_ps_page_content) {
                    $new_ps_page_content = $old_ps_page_content->replicate();
                    $new_ps_page_content->blog_section_id = $new_page_section->id;
                    $new_ps_page_content->save();
                }
            }
            // BlogFeatureBlock
            $old_ps_page_feature_blocks = BlogFeatureBlock::where('blog_section_id', $old_page_section->id)->get();
            if($old_ps_page_feature_blocks) {
                foreach ($old_ps_page_feature_blocks as $old_ps_page_feature_block) {
                    $new_ps_page_feature_block = $old_ps_page_feature_block->replicate();
                    $new_ps_page_feature_block->blog_section_id = $new_page_section->id;
                    $new_ps_page_feature_block->feat_image = ($old_ps_page_feature_block->feat_image)? $this->duplicateImage($old_ps_page_feature_block->feat_image) : null;
                    $new_ps_page_feature_block->save();
                }
            }
            // BlogFeatureSection
            $old_ps_page_feature_sections = BlogFeatureSection::where('blog_section_id', $old_page_section->id)->get();
            if($old_ps_page_feature_sections) {
                foreach ($old_ps_page_feature_sections as $old_ps_page_feature_section) {
                    $new_ps_page_feature_section = $old_ps_page_feature_section->replicate();
                    $new_ps_page_feature_section->blog_section_id = $new_page_section->id;
                    $new_ps_page_feature_section->feat_image = ($old_ps_page_feature_section->feat_image)? $this->duplicateImage($old_ps_page_feature_section->feat_image) : null;
                    $new_ps_page_feature_section->save();
                }
            }
            return back()->with([
                'success' => 'Section Duplicated Successfully!'
            ]);
        } else {
            return back()->with([
                'error' => 'Error while duplicating section. Please try again.'
            ]);
        }
    }

    public function duplicateComponent(Request $request) {
        // Check model 
        switch($request->model) {
            case 'BlogContent':
                $old_page_content = BlogContent::find($request->component_id);
                if($old_page_content) {
                    $new_page_content = $old_page_content->replicate();
                    $new_page_content->arrangement = BlogContent::where('blog_section_id', $old_page_content->blog_section_id)->count() + 1;
                    $new_page_content->save();

                    return back()->with([
                        'success' => 'Content Duplicated Successfully!'
                    ]);
                } else {
                    return back()->with([
                        'error' => 'Error while duplicating content. Please try again.'
                    ]);
                }
                break;
            case 'BlogFeatureBlock':
                $old_page_feature_block = BlogFeatureBlock::find($request->component_id);
                if($old_page_feature_block) {
                    $new_page_feature_block = $old_page_feature_block->replicate();
                    $new_page_feature_block->arrangement = BlogFeatureBlock::where('blog_section_id', $old_page_feature_block->blog_section_id)->count() + 1;
                    $new_page_feature_block->feat_image = ($old_page_feature_block->feat_image)? $this->duplicateImage($old_page_feature_block->feat_image) : null;
                    $new_page_feature_block->save();

                    return back()->with([
                        'success' => 'Feature Block Duplicated Successfully!'
                    ]);
                } else {
                    return back()->with([
                        'error' => 'Error while duplicating feature block. Please try again.'
                    ]);
                }
                break;
            case 'BlogFeatureSection':
                $old_page_feature_section = BlogFeatureSection::find($request->component_id);
                if($old_page_feature_section) {
                    $new_page_feature_section = $old_page_feature_section->replicate();
                    $new_page_feature_section->arrangement = BlogFeatureSection::where('blog_section_id', $old_page_feature_section->blog_section_id)->count() + 1;
                    $new_page_feature_section->feat_image = ($old_page_feature_section->feat_image)? $this->duplicateImage($old_page_feature_section->feat_image) : null;
                    $new_page_feature_section->save();

                    return back()->with([
                        'success' => 'Feature Section Duplicated Successfully!'
                    ]);
                } else {
                    return back()->with([
                        'error' => 'Error while duplicating feature section. Please try again.'
                    ]);
                }
                break;
            default:
                return back()->with([
                    'error' => 'Error while duplicating component. Please try again.'
                ]);
                break;
        }
    }

    // Dynamic Validation Function accepts $request->type
    public function getvalidation($model, $update = false) {
        $rules = [];
        $messages = [];

        switch($model) {
            
            
            // case 'service':
            //     $rules = [
            //         'name' => 'required',
            //         // 'icon' => 'required',
            //         // 'details' => 'required',
            //         // 'content_1' => 'required',
            //         // 'seo_serp' => 'required',
            //         // 'seo_keywords' => 'required',
            //         'service_type' => 'required',
            //     ];
        
            //     $messages = [
            //         'name.required' => 'Service name is required.',
            //         'icon.required' => 'Header color is required.',
            //         'details.required' => 'Details is required.',
            //         'content_1.required' => 'Content is required.',
            //         'seo_serp.required' => 'SEO serp is required.',
            //         'seo_keywords.required' => 'SEO keywords is required.',
            //         'service_type.required' => 'Service type is required.',
            //     ];
            //     break;
            
        }

        return (object) [ 'rules' => $rules, 'messages' => $messages ];
    }
}
