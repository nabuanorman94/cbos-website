<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\SiteAboutUs;
use App\SiteBanner;
use App\SiteContent;
use App\SiteDetails;
use App\SiteFaq;
use App\SitePage;
use App\SitePartner;
use App\SitePricing;
// use App\SiteService;
use App\SiteSocial;
use App\SiteTeamMember;
use App\SiteTestimonial;
use App\ContactUs;
use App\Inquiry;
use App\PageContent;
use App\PageFeatureBlock;
use App\PageFeatureSection;
use App\PageReviewSection;
use App\PageSection;
use App\PageType;
use App\PageBanner;
use App\Mail\ContactUsToUser;
use App\Mail\ContactUsToAdmin;
use App\Mail\GetStarted;
use App\Mail\GetStartedToAdmin;
use App\Mail\SubscribeNewsletter;
use App\Blog;
use App\BlogCategory;
use App\BlogSection;
use App\BlogContent;
use App\BlogFeatureBlock;
use App\BlogFeatureSection;
use App\Qotd;
use App\Newsletter;
use App\EmailContent;
use Carbon\Carbon;
use App\Rules\Captcha;

use App\CallSchedule;
use App\CallScheduleBooking;
use App\Mail\VerifyCallSchedule;

class WebsiteController extends Controller
{
    // Controllers for the website

    // Homepage
    public function index() {
        $details = SiteDetails::first();

        if($details->site_under_maintenance) {
            return view('updating');
        } else {
            return view('homepage')->with([
                'breadcrumbs_title' => 'Home',
                'breadcrumbs_links' => [
                    [
                        'name' => 'Home',
                        'route' => 'home'
                    ]
                ],
                'details' => SiteDetails::first(),
                'about_us' => SiteAboutUs::first(),
                'site_content' => SiteContent::first(),
                'banners' => SiteBanner::where('show', 1)->orderBy('id', 'asc')->get(),
                'partners' => SitePartner::orderBy('id', 'asc')->get(),
                // 'services' => SiteService::where('show', 1)->orderBy('id', 'asc')->get(),
                'testimonials' => SiteTestimonial::where('show', 1)->orderBy('id', 'asc')->get(),
                'teams' => SiteTeamMember::where('show', 1)->orderBy('id', 'asc')->get(),
                'faqs' => SiteFaq::where('faq_type', 'homepage')->orderBy('id', 'asc')->get(),
                'pricings' => SitePricing::where('show', 1)->orderBy('id', 'asc')->get(),
                'socials' => SiteSocial::orderBy('id', 'asc')->get(),
                'qotd' => Qotd::where('start', Carbon::now()->setTimezone('Asia/Manila')->format('Y-m-d'))->first(),
                'blogs' => Blog::latest('created_at')->limit(3)->get(),
            ]);
        }
    }

    public function updating() {
        return view('homepage')->with([
            'breadcrumbs_title' => 'Home',
            'breadcrumbs_links' => [
                [
                    'name' => 'Home',
                    'route' => 'home'
                ]
            ],
            'details' => SiteDetails::first(),
            'about_us' => SiteAboutUs::first(),
            'site_content' => SiteContent::first(),
            'banners' => SiteBanner::where('show', 1)->orderBy('id', 'asc')->get(),
            'partners' => SitePartner::orderBy('id', 'asc')->get(),
            // 'services' => SiteService::where('show', 1)->orderBy('id', 'asc')->get(),
            'testimonials' => SiteTestimonial::where('show', 1)->orderBy('id', 'asc')->get(),
            'teams' => SiteTeamMember::where('show', 1)->orderBy('id', 'asc')->get(),
            'faqs' => SiteFaq::where('faq_type', 'homepage')->orderBy('id', 'asc')->get(),
            'pricings' => SitePricing::where('show', 1)->orderBy('id', 'asc')->get(),
            'socials' => SiteSocial::orderBy('id', 'asc')->get(),
        ]);
    }

    public function viewService($slug) {
        $page = SitePage::where([
            ['slug', '=', $slug],
            ['page_type', '!=', 'stand-alone'],
            ['show', '=', 1]
        ])
        ->with('pageSection')
        ->first();

        if($page) {
            return view('view_page')->with([
                'page' => $page,
                'page_sections' => PageSection::where('page_id', $page->id)->orderBy('arrangement', 'asc')->get(),
                'faqs' => SiteFaq::where('faq_type', $slug)->get(),
                'details' => SiteDetails::first(),
                'site_content' => SiteContent::first(),
                'socials' => SiteSocial::orderBy('id', 'asc')->get(),
                'page_type' => 'article'
            ]);
        } else {
            return abort(404);
        }
    }

    // Send contact-us
    public function contactUsSend(Request $request) {
        // $admin_emails = ['support@cbos.com.ph', 'mark.lofranco@cbos.com.ph', 'norman.nabua@cbos.com.ph'];
        // // $admin_emails = ['norman.nabua@cbos.com.ph'];

        // $rules = [
        //     'name' => 'required',
        //     'email' => 'required',
        //     'message' => 'required',
        //     // 'g-recaptcha-response' => new Captcha()
        // ];

        // $messages = [
        //     'title.required' => 'Title is required.',
        //     'email.required' => 'Email is required.',
        //     'message.required' => 'Message is required.',
        // ];

        // $this->validate($request, $rules, $messages);

        // // Send to multiple email listed
        // foreach ($admin_emails as $email) {
        //     Mail::to($email)->send(new ContactUsToAdmin($request, SiteDetails::first()));
        // }

        // $email_content = EmailContent::where('email_type', 'Contact Us')->first();
        // Mail::to($request->email)->send(new ContactUsToUser($request->name, $email_content, SiteDetails::first()));

        // $request['replied'] = 0;
        ContactUs::create($request->all());

        // // return response()->json([
        // //     'success' => 'Thank you for reaching on us.'
        // // ]);

        return back()->with([
            'success' => 'Thank you for reaching on us.'
        ]);
    }

    public function getStarted() {
        return view('auth.get-started');
    }

    public function getStartedSend(Request $request) {
        $admin_emails = ['support@cbos.com.ph', 'mark.lofranco@cbos.com.ph', 'norman.nabua@cbos.com.ph'];
        // $admin_emails = ['norman.nabua@cbos.com.ph'];

        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'contact_no' => 'required',
            'email' => 'required'
        ];

        $messages = [
            'first_name.required' => 'First name is required.',
            'last_name.required' => 'Last name is required.',
            'contact_no.required' => 'Contact number is required.',
            'email.required' => 'Email is required.',
        ];

        $this->validate($request, $rules, $messages);

        // Send to multiple email listed
        foreach ($admin_emails as $email) {
            Mail::to($email)->send(new GetStartedToAdmin($request, SiteDetails::first()));
        }

        $email_content = EmailContent::where('email_type', 'Get Started')->first();
        Mail::to($request->email)->send(new GetStarted($request->first_name, $email_content, SiteDetails::first()));

        $request['replied'] = 0;
        Inquiry::create($request->all());

        return response()->json([
            'success' => 'Thank you for your interest. We will contact you as soon as possible!'
        ]);
    }

    public function subscribeSend(Request $request) {
        if(!Newsletter::where('email', $request->email)->first()) {
            $request['status'] = 1;
            Newsletter::create($request->except('_token'));
            $email_content = EmailContent::where('email_type', 'Subscribe Newsletter')->first();
            Mail::to($request->email)->send(new SubscribeNewsletter($email_content, SiteDetails::first()));
        }

        return response()->json([
            'success' => 'Success!'
        ]);
    }

    public function page($page) {
        $my_page = SitePage::where([
                ['slug', '=', $page],
                ['page_type', '=', 'stand-alone'],
                ['show', '=', 1]
            ])
            ->with('pageSection')
            ->first();
        
        $slug_not_allowed = ['blogs', 'question-of-the-day'];

        if($my_page && !in_array($page, $slug_not_allowed)) {
            return view('view_page')->with([
                'page' => $my_page,
                'page_sections' => PageSection::where('page_id', $my_page->id)->orderBy('arrangement', 'asc')->get(),
                'faqs' => SiteFaq::where('faq_type', $page)->get(),
                'details' => SiteDetails::first(),
                'site_content' => SiteContent::first(),
                'socials' => SiteSocial::orderBy('id', 'asc')->get(),
                'page_type' => 'page'
            ]);
        } else {
            return abort(404);
        }
    }

    public function viewAllBlogs($category, Request $request) {
        $page = SitePage::where([
            ['slug', '=', 'blogs'],
            ['page_type', '=', 'stand-alone'],
            ['show', '=', 1]
        ])
        ->with('pageSection')
        ->first();

        $results = 0;

        switch($category) {
            case 'all':
                if(isset($request->search)) {
                    $blogs = Blog::where([
                        ['title', 'like', '%' .$request->search . '%'],
                        ['show', '=', 1]
                    ])
                    ->with('blogCategory')
                    ->orderBy('created_at', 'desc')
                    ->paginate(12)
                    ->appends(['search' => $request->search]);

                    $results = Blog::where([
                        ['title', 'like', '%' .$request->search . '%'],
                        ['show', '=', 1]
                    ])->count();
                } else {
                    $blogs = Blog::where([
                        ['show', '=', 1]
                    ])
                    ->with('blogCategory')
                    ->orderBy('created_at', 'desc')
                    ->paginate(9);
                    
                    $results = Blog::where([
                        ['show', '=', 1]
                    ])->count();
                }

                break;
            default:
                $blog_category = BlogCategory::where([
                    ['show', '=', 1],
                    ['slug', '=', $category]
                ])->first();

                if($blog_category) {
                    if(isset($request->search)) {
                        $blogs = Blog::where([
                            ['title', 'like', '%' .$request->search . '%'],
                            ['blog_category_id', '=', $blog_category->id],
                            ['show', '=', 1]
                        ])
                        ->with('blogCategory')
                        ->orderBy('created_at', 'desc')
                        ->paginate(12)
                        ->appends(['search' => $request->search]);

                        $results = Blog::where([
                            ['title', 'like', '%' .$request->search . '%'],
                            ['blog_category_id', '=', $blog_category->id],
                            ['show', '=', 1]
                        ])->count();
                    } else {
                        $blogs = Blog::where([
                            ['blog_category_id', '=', $blog_category->id],
                            ['show', '=', 1]
                        ])
                        ->with('blogCategory')
                        ->orderBy('created_at', 'desc')
                        ->paginate(9);

                        $results = Blog::where([
                            ['blog_category_id', '=', $blog_category->id],
                            ['show', '=', 1]
                        ])->count();
                    }
                } else {
                    $blogs = [];
                }
                break;
        }

        if($page && $blogs) {
            return view('news_blogs')->with([
                'items' => $blogs,
                'results' => $results,
                'breadcrumbs_title' => 'News & Blogs',
                'breadcrumbs_links' => [
                    [
                        'name' => 'Home',
                        'route' => 'homepage'
                    ]
                ],
                'page' => $page,
                'page_sections' => PageSection::where('page_id', $page->id)->orderBy('arrangement', 'asc')->get(),
                'faqs' => SiteFaq::where('faq_type', $page)->get(),
                'details' => SiteDetails::first(),
                'site_content' => SiteContent::first(),
                'socials' => SiteSocial::orderBy('id', 'asc')->get(),
                'categories' => BlogCategory::where('show', 1)->orderBy('name', 'asc')->get(),
                'my_category' => $category
            ]);
        } else {
            return abort(404);
        }
    }

    public function viewBlog($slug) {
        $blog = Blog::where([
            ['slug', '=', $slug],
            ['show', '=', 1]
        ])
        ->with('blogSection')
        ->first();

        if($blog) {
            $blog->increment('count_view');
            
            return view('view_blog')->with([
                'blog' => $blog,
                'blog_sections' => BlogSection::where('blog_id', $blog->id)->orderBy('arrangement', 'asc')->get(),
                'faqs' => SiteFaq::where('faq_type', $blog->slug)->get(),
                'details' => SiteDetails::first(),
                'site_content' => SiteContent::first(),
                'socials' => SiteSocial::orderBy('id', 'asc')->get(),
                'page_type' => 'article',
                'breadcrumbs_title' => $blog->title,
                'breadcrumbs_links' => [
                    [
                        'name' => 'Home',
                        'route' => 'homepage'
                    ],
                    [
                        'name' => 'Home',
                        'route' => 'homepage'
                    ]
                ],
                'recent_blogs' => Blog::where([['id', '!=', $blog->id]])->latest('created_at')->limit(3)->get(),
                'related_blogs' => Blog::where([
                    ['id', '!=', $blog->id],
                    ['blog_category_id', '=', $blog->blog_category_id]
                ])->latest('created_at')->limit(3)->get(),
                'categories' => BlogCategory::where('show', 1)->orderBy('name', 'asc')->get()
            ]);
        } else {
            return abort(404);
        }
    }

    public function getBlogTitleAutocomplete(Request $request) {
        $search = $request->search;

        if($search == ''){
            $autocomplate = Blog::orderby('title', 'asc')->select('id','title')->limit(5)->get();
        } else {
            $autocomplate = Blog::orderby('title', 'asc')->select('id','title')->where('title', 'like', '%' .$search . '%')->limit(5)->get();
        }

        $response = array();
        foreach($autocomplate as $autocomplate){
            $response[] = array("value" => $autocomplate->id, "label" => $autocomplate->title);
        }

        echo json_encode($response);
        exit;
    }

    public function viewAllQotds(Request $request) {
        $page = SitePage::where([
            ['slug', '=', 'question-of-the-day'],
            ['page_type', '=', 'stand-alone'],
            ['show', '=', 1]
        ])
        ->with('pageSection')
        ->first();

        $results = 0;

        // $qotd = Qotd::where('start', $date)->first();

        // if($page) {
        //     if($qotd && $date <= Carbon::now()->setTimezone('Asia/Manila')->format('Y-m-d')) {
        //         return view('qotds')->with([
        //             'qotd' => $qotd,
        //             'breadcrumbs_title' => 'Question of the Day',
        //             'breadcrumbs_links' => [
        //                 [
        //                     'name' => 'Home',
        //                     'route' => 'homepage'
        //                 ]
        //             ],
        //             'page' => $page,
        //             'page_sections' => PageSection::where('page_id', $page->id)->orderBy('arrangement', 'asc')->get(),
        //             'faqs' => SiteFaq::where('faq_type', $page)->get(),
        //             'details' => SiteDetails::first(),
        //             'site_content' => SiteContent::first(),
        //             'socials' => SiteSocial::orderBy('id', 'asc')->get()
        //         ]);
        //     } else {
        //         return view('qotds')->with([
        //             'qotd' => Qotd::where([[ 'start', '<=', Carbon::now()->setTimezone('Asia/Manila')->format('Y-m-d') ]])->latest('created_at')->first(),
        //             'breadcrumbs_title' => 'Question of the Day',
        //             'breadcrumbs_links' => [
        //                 [
        //                     'name' => 'Home',
        //                     'route' => 'homepage'
        //                 ]
        //             ],
        //             'page' => $page,
        //             'page_sections' => PageSection::where('page_id', $page->id)->orderBy('arrangement', 'asc')->get(),
        //             'faqs' => SiteFaq::where('faq_type', $page)->get(),
        //             'details' => SiteDetails::first(),
        //             'site_content' => SiteContent::first(),
        //             'socials' => SiteSocial::orderBy('id', 'asc')->get(),
        //             'info' => 'No data found on ' . Carbon::createFromTimeStamp(strtotime($date))->isoFormat('LL') . '.'
        //         ]);
        //     }
        // } else {
        //     return abort(404);
        // }
        if(isset($request->search)) {
            $qotds = Qotd::where('title', 'like', '%' .$request->search . '%')->orderBy('created_at', 'desc')->paginate(12)->appends(['search' => $request->search]);
            $results = Qotd::where('title', 'like', '%' .$request->search . '%')->count();
        } else {
            $qotds = Qotd::orderBy('created_at', 'desc')->paginate(12);
            $results = Qotd::count();
        }

        return view('qotds')->with([
            'qotds' => $qotds,
            'results' => $results,
            'breadcrumbs_title' => 'Question of the Day',
            'breadcrumbs_links' => [
                [
                    'name' => 'Home',
                    'route' => 'homepage'
                ]
            ],
            'page' => $page,
            'page_sections' => PageSection::where('page_id', $page->id)->orderBy('arrangement', 'asc')->get(),
            'faqs' => SiteFaq::where('faq_type', $page)->get(),
            'details' => SiteDetails::first(),
            'site_content' => SiteContent::first(),
            'socials' => SiteSocial::orderBy('id', 'asc')->get()
        ]);
    }

    public function getQotdQuestionAutocomplete(Request $request) {
        $search = $request->search;

        if($search == ''){
            $autocomplate = Qotd::orderby('title', 'asc')->select('id','title')->limit(5)->get();
        } else {
            $autocomplate = Qotd::orderby('title', 'asc')->select('id','title')->where('title', 'like', '%' .$search . '%')->limit(5)->get();
        }

        $response = array();
        foreach($autocomplate as $autocomplate){
            $response[] = array("value" => $autocomplate->id, "label" => $autocomplate->title);
        }

        echo json_encode($response);
        exit;
    }

    public function scheduleACall(Request $request) {
        $active_dates = CallSchedule::groupBy('date')->select('date')->get()->pluck('date');
        $schedules = ($request->date)? CallSchedule::where([
            ['date', '=', $request->date],
            ['status', '=', '1']
        ])->get() : [];

        return view('auth.schedule_a_call')->with([
            'active_dates' => $active_dates,
            'schedules' => $schedules,
            'date' => ($request->date)? $request->date : null,
            'time' => ($request->time)? $request->time : null,
            'sched_id' => ($request->id)? $request->id : null,
        ]);
    }

    public function scheduleACallSubmit(Request $request) {
        $admin_emails = ['support@cbos.com.ph', 'mark.lofranco@cbos.com.ph', 'norman.nabua@cbos.com.ph'];

        $rules = [
            'name' => 'required',
            'email' => 'required'
        ];

        $messages = [
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required.'
        ];

        $this->validate($request, $rules, $messages);

        $token = md5(uniqid(rand(), true));

        // // Send to multiple email listed
        // foreach ($admin_emails as $email) {
        //     Mail::to($email)->send(new GetStartedToAdmin($request, SiteDetails::first()));
        // }

        // Check if already booked
        $booked = CallScheduleBooking::where([
            ['call_schedule_id', '=', $request->sched_id],
            ['email', '=', $request->email]
        ])->first();

        if($booked) {
            return back()->withInput($request->all())->with([
                'info' => 'Already scheduled a call. Please check your inbox to verify the schedule. Thank you.'
            ]);
        }

        CallScheduleBooking::create([
            'call_schedule_id' => $request->sched_id,
            'name' => $request->name,
            'email' => $request->email,
            'details' => $request->details,
            'token' => $token
        ]);

        $email_content = EmailContent::where('email_type', 'Verify Call Schedule')->first();
        Mail::to($request->email)->send(new VerifyCallSchedule($request->name, $email_content, SiteDetails::first(), '<a href="link_here='.$token.'">Verify Email</a>'));

        // return response()->json([
        //     'success' => 'We sent you a verification link. Please verify your email.'
        // ]);

        return back()->with([
            'active_dates' => [],
            'schedules' => [],
            'date' => null,
            'time' => null,
            'sched_id' => null,
            'success' => 'We sent you a verification link. Please verify your email.'
        ]);
    }
}
