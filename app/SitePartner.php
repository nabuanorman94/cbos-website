<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SitePartner extends Model
{
		public $timestamps = false;

    protected $fillable = [
    	'name',
    	'logo',
	];
}
