<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    
    protected $fillable = [
        'username', 
        'verify_using',
        'status', 
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'verified_at' => 'datetime',
    ];

    /**
     * Get the role
     */
	public function role()
	{
		return $this->belongsToMany('App\Role', 'user_roles', 'user_id', 'role_id');
    }

    /**
     * Get the user role
     */
    public function userRole() {
        return $this->hasOne('App\UserRole', 'user_id', 'id');
    }

    /**
     * Get the user role name
     */
    public function getRoleNameAttribute()
    {
        return auth()->user()->role->first()->name;
    }
}
