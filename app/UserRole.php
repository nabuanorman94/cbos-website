<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    /**
     * The timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    
    protected $fillable = [
    	'user_id',
    	'role_id'
	];
}
