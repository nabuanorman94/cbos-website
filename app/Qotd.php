<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qotd extends Model
{
    protected $fillable = [
        'title',
        'start',
        'description',
        'show',
        'video'
	];
}
