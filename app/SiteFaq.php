<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteFaq extends Model
{
    protected $fillable = [
        'question',
        'answer',
        'faq_type'
	];

}
