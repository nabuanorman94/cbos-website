<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyCallSchedule extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $content, $details, $link)
    {
        $this->name = $name;
        $this->details = $details;
        $this->content = $content;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->content->subject)
        ->from(['address' => 'no-reply@cbos.com.ph', 'name' => 'CBOS Accounting and Taxation'])
        ->markdown('emails.verify-call-schedule')
        ->with([
            'name' => $this->name,
            'content' => str_replace(array('&lt;user&gt;', '&lt;link&gt;'), array($this->name, $this->link), $this->content->content),
            'details' => $this->details
        ]);
    }
}
