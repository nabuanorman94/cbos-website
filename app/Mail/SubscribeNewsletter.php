<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubscribeNewsletter extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content, $details)
    {
        $this->details = $details;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->content->subject)
        ->from(['address' => 'support@cbos.com.ph', 'name' => 'CBOS Accounting and Taxation'])
        ->markdown('emails.subscribe-newsletter')
        ->with([
            'content' => $this->content->content,
            'details' => $this->details
        ]);
    }
}
