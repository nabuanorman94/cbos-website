<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUsToUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $content, $details)
    {
        $this->name = $name;
        $this->details = $details;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->content->subject)
        ->from(['address' => 'no-reply@cbos.com.ph', 'name' => 'CBOS Accounting and Taxation'])
        ->markdown('emails.contact-us')
        ->with([
            'name' => $this->name,
            'content' => str_replace('&lt;user&gt;', $this->name, $this->content->content),
            'details' => $this->details
        ]);
    }
}
