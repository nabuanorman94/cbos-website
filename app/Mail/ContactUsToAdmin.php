<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUsToAdmin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $details)
    {
        $this->user = $user;
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New Message from CBOS Website')
        ->from(['address' => 'no-reply@cbos.com.ph', 'name' => 'CBOS Accounting and Taxation'])
        ->markdown('emails.contact-us-to-admin')
        ->with([
            'user' => $this->user,
            'details' => $this->details
        ]);
    }
}
