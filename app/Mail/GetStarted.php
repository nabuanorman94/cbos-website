<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GetStarted extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $content, $details)
    {
        $this->name = $name;
        $this->details = $details;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->content->subject)
        ->from(['address' => 'support@cbos.com.ph', 'name' => 'CBOS Accounting and Taxation'])
        ->markdown('emails.get-started')
        ->with([
            'name' => $this->name,
            'content' => str_replace('&lt;user&gt;', $this->name, $this->content->content),
            'details' => $this->details
        ]);
    }
}
