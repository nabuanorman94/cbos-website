<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageReviewSection extends Model
{
    protected $fillable = [
        'page_section_id',
        'arrangement',
        'div_styling',
        'div_class',
        'div_id',
        'image_src',
        'image_alt',
        'rev_name',
        'rev_position',
        'rev_content'
	];
}
