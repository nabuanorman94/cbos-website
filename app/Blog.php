<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Blog extends Model
{
    protected $appends = ['seo_serp_trim'];

    protected $fillable = [
        'blog_category_id',
        'title',
        'sub_title',
        'slug',
        'banner_image',
        'banner_image_mobile',
        'heading_position',
        'bg_color',
        'seo_serp',
        'seo_keywords',
        'seo_image',
        'show',
        'count_view',
        'count_likes',
        'author'
	];

    /**
     * Get the blog sections
     */
    public function blogSection() {
        return $this->hasMany('App\BlogSection', 'blog_id', 'id')->orderBy('arrangement');
    }

    /**
     * Get the blog category
     */
    public function blogCategory() {
        return $this->belongsTo('App\BlogCategory');
    }

    // Get serp trim
    public function getSeoSerpTrimAttribute() {
        return Str::limit($this->seo_serp, 200, '...');
    }
}
