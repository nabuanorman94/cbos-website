<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteAboutUs extends Model
{
    protected $fillable = [
        'section_1_details',
        'section_1_list',
        'section_2_details',
        'cta_name',
        'cta_link',
        'count_1_number',
        'count_1_icon',
        'count_1_details',
        'count_2_number',
        'count_2_icon',
        'count_2_details',
        'count_3_number',
        'count_3_icon',
        'count_3_details',
        'count_4_number',
        'count_4_icon',
        'count_4_details',
        'image'
    ];
}
