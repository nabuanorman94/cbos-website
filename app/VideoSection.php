<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoSection extends Model
{
    protected $fillable = [
        'video_id',
        'arrangement',
        'div_styling',
        'div_class',
        'div_id',
        'div_image_background',
        'section_style'
	];

    /**
     * Get parent video
     */
    public function video() {
        return $this->belongsTo('App\Blog');
    }

    /**
     * Get content children
     */
    public function contents() {
        return $this->hasMany('App\PageContent', 'page_section_id', 'id')->orderBy('arrangement');
    }

    /**
     * Get feature block children
     */
    public function feature_blocks() {
        return $this->hasMany('App\PageFeatureBlock', 'page_section_id', 'id')->orderBy('arrangement');
    }

    /**
     * Get feature section children
     */
    public function feature_sections() {
        return $this->hasMany('App\PageFeatureSection', 'page_section_id', 'id')->orderBy('arrangement');
    }
}
