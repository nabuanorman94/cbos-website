<ul class="nav-main">
    @switch(auth()->user() && auth()->user()->getRoleNameAttribute())
        @case('Admin')
            <li>
                <a class="{{ request()->is('admin') ? ' active' : '' }}" href="{{ route('admin-home') }}">
                    <i class="si si-cup"></i>
                    <span class="sidebar-mini-hide">Dashboard</span>
                </a>
            </li>
            <li>
                <a class="{{ request()->is('notifications') ? ' active' : '' }}" href="{{ route('admin-notifications') }}">
                    <i class="si si-bell"></i>
                    <span class="sidebar-mini-hide">Notifications</span>
                </a>
            </li>
            <li class="{{ request()->is('admin/manage-site/*') ? ' open' : '' }}">
                <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                    <i class="fa fa-sitemap"></i>
                    <span class="sidebar-mini-hide">Manage Site</span>
                </a>
                <ul>
                    <li class="{{ request()->is('admin/manage-site/pages/*') ? ' open' : '' }}">
                        <a class="{{ request()->is('admin/manage-site/pages') ? 'nav-submenu active' : 'nav-submenu' }}" class="nav-submenu" data-toggle="nav-submenu" href="#">Pages</a>
                        <ul>
                            <li>
                                <a class="{{ request()->is('admin/manage-site/pages/add') ? ' active' : '' }}" href="{{ route('admin-manage-site-page-add') }}">Add New Page</a>
                            </li>
                            <li>
                                <a class="{{ request()->is('admin/manage-site/pages/all') ? ' active' : '' }}" href="{{ route('admin-manage-site-pages-all') }}">All Pages</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="{{ request()->is('admin/manage-site/details') ? ' active' : '' }}" href="{{ route('admin-manage-site-details') }}" onclick="removeActiveTab()">Details</a>
                    </li>
                    <li>
                        <a class="{{ request()->is('admin/manage-site/banners') ? ' active' : '' }}" href="{{ route('admin-manage-site-banners') }}" onclick="removeActiveTab()">Banners</a>
                    </li>
                    <li>
                        <a class="{{ request()->is('admin/manage-site/content') ? ' active' : '' }}" href="{{ route('admin-manage-site-content') }}" onclick="removeActiveTab()">Content</a>
                    </li>
                    <li>
                        <a class="{{ request()->is('admin/manage-site/about-us') ? ' active' : '' }}" href="{{ route('admin-manage-site-about-us') }}" onclick="removeActiveTab()">About Us</a>
                    </li>
                    <li>
                        <a class="{{ request()->is('admin/manage-site/partners') ? ' active' : '' }}" href="{{ route('admin-manage-site-partners') }}" onclick="removeActiveTab()">Partners</a>
                    </li>
                    {{-- <li>
                        <a class="{{ request()->is('admin/manage-site/services') ? ' active' : '' }}" href="{{ route('admin-manage-site-services') }}" onclick="removeActiveTab()">Services</a>
                    </li> --}}
                    <li>
                        <a class="{{ request()->is('admin/manage-site/testimonials') ? ' active' : '' }}" href="{{ route('admin-manage-site-testimonials') }}" onclick="removeActiveTab()">Testimonials</a>
                    </li>
                    <li>
                        <a class="{{ request()->is('admin/manage-site/team') ? ' active' : '' }}" href="{{ route('admin-manage-site-team') }}" onclick="removeActiveTab()">Team</a>
                    </li>
                    <li>
                        <a class="{{ request()->is('admin/manage-site/pricing') ? ' active' : '' }}" href="{{ route('admin-manage-site-pricing') }}" onclick="removeActiveTab()">Pricing</a>
                    </li>
                    <li>
                        <a class="{{ request()->is('admin/manage-site/faqs') ? ' active' : '' }}" href="{{ route('admin-manage-site-faqs') }}" onclick="removeActiveTab()">FAQs</a>
                    </li>
                    <li>
                        <a class="{{ request()->is('admin/manage-site/social-links') ? ' active' : '' }}" href="{{ route('admin-manage-site-socials') }}" onclick="removeActiveTab()">Social Links</a>
                    </li>
                    <li>
                        <a class="{{ request()->is('admin/manage-site/email-contents') ? ' active' : '' }}" href="{{ route('admin-manage-site-email-contents') }}" onclick="removeActiveTab()">Email Contents</a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('admin/resources/*') ? ' open' : '' }}">
                <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                    <i class="fa fa-file-movie-o"></i>
                    <span class="sidebar-mini-hide">Resources</span>
                </a>
                <ul>
                    <li>
                        <a class="{{ request()->is('admin/resources/blogs') || request()->is('admin/resources/blogs/*') ? ' active' : '' }}" href="{{ route('admin-resources-blogs', ['category' => 'all']) }}" onclick="removeActiveTab()">News & Blogs</a>
                    </li>
                    
                    <li>
                        <a class="{{ request()->is('admin/resources/videos') || request()->is('admin/resources/videos/*') ? ' active' : '' }}" href="{{ route('admin-resources-videos') }}" onclick="removeActiveTab()">Articles & Videos</a>
                    </li>
                    <li>
                        <a class="{{ request()->is('admin/resources/question-for-the-day') || request()->is('admin/resources/question-for-the-day/*') ? ' active' : '' }}" href="{{ route('admin-resources-question-for-the-day') }}" onclick="removeActiveTab()">Question for the Day</a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('admin/call-schedule/*') ? ' open' : '' }}">
                <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                    <i class="si si-phone"></i>
                    <span class="sidebar-mini-hide">Call Schedule</span>
                </a>
                <ul>
                    <li>
                        <a class="{{ request()->is('admin/call-schedule-current') ? ' active' : '' }}" href="{{ route('admin-call-schedule-current') }}" onclick="removeActiveTab()">Booked</a>
                    </li>
                    <li>
                        <a class="{{ request()->is('admin/call-schedule-manage') ? ' active' : '' }}" href="{{ route('admin-call-schedule-manage', [ 'date' =>  Carbon\Carbon::now()->setTimezone('Asia/Manila')->format('Y-m-d')]) }}" onclick="removeActiveTab()">Manage</a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="{{ request()->is('admin/inquiries') ? ' active' : '' }}" href="{{ route('admin-inquiries') }}">
                    <i class="si si-bubbles"></i>
                    <span class="sidebar-mini-hide">Inquiries</span>
                </a>
            </li>
            <li>
                <a class="{{ request()->is('admin/contact-us') ? ' active' : '' }}" href="{{ route('admin-contact-us') }}">
                    <i class="si si-action-undo"></i>
                    <span class="sidebar-mini-hide">Contact Us</span>
                </a>
            </li>
            <li>
                <a class="{{ request()->is('admin/manage-clients') ? ' active' : '' }}" href="{{ route('admin-manage-clients') }}">
                    <i class="fa fa-building-o"></i>
                    <span class="sidebar-mini-hide">Manage Clients</span>
                </a>
            </li>
            <li>
                <a class="{{ request()->is('admin/manage-users') ? ' active' : '' }}" href="{{ route('admin-manage-users') }}">
                    <i class="si si-users"></i>
                    <span class="sidebar-mini-hide">Manage Users</span>
                </a>
            </li>
            <li>
                <a class="{{ request()->is('admin/my-team') ? ' active' : '' }}" href="{{ route('admin-my-team') }}">
                    <i class="si si-star"></i>
                    <span class="sidebar-mini-hide">My Team</span>
                </a>
            </li>
            <li>
                <a class="{{ request()->is('admin/newsletters') ? ' active' : '' }}" href="{{ route('admin-newsletters') }}">
                    <i class="si si-envelope"></i>
                    <span class="sidebar-mini-hide">Newsletters</span>
                </a>
            </li>
            <li>
                <a class="{{ request()->is('admin/webminars') ? ' active' : '' }}" href="{{ route('admin-webminars') }}">
                    <i class="si si-badge"></i>
                    <span class="sidebar-mini-hide">Webminars</span>
                </a>
            </li>
            <li>
                <a class="{{ request()->is('admin/invoices') ? ' active' : '' }}" href="{{ route('admin-invoices') }}">
                    <i class="si si-note"></i>
                    <span class="sidebar-mini-hide">Invoices</span>
                </a>
            </li>
            <li>
                <a class="{{ request()->is('admin/reports') ? ' active' : '' }}" href="{{ route('admin-reports') }}">
                    <i class="si si-graph"></i>
                    <span class="sidebar-mini-hide">Reports</span>
                </a>
            </li>
            @break
        @case(2)
            
            @break
        @default
            
    @endswitch
</ul>