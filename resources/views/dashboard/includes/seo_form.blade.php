<small>SEO Section</small>
<div class="form-group">
    <div class="form-material">
        <input type="text" class="form-control" id="seo_keywords" name="seo_keywords" placeholder="" value="{{ ($item && $item->seo_keywords)? $item->seo_keywords : old('seo_keywords') }}">
        <label for="seo_keywords">Keywords</label>
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <label for="seo_serp">Serp</label>
        <textarea class="form-control" id="seo_serp" name="seo_serp" rows="8">{{ ($item && $item->seo_serp)? $item->seo_serp : old('seo_serp') }}</textarea>
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <label for="seo_image">Image</label>
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="seo_image" name="seo_image_file">
            <label class="custom-file-label" for="seo_image">Choose file</label>
        </div>
    </div>
    @if ($item && $item->seo_image)
        <hr>
        <div class="text-center mx-3">
            <img src="{{ $item->seo_image }}" alt="{{ $item->name }} SEO Image" class="w-100 mt-2">
        </div>
    @endif
</div>