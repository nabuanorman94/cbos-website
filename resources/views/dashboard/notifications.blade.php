@extends('layouts.backend')

@section('title')
    Notifications - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-bell"></i> Notifications</h2>

        {{-- <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Banner List</h3>
                <div class="block-options">
                    <a href="" type="button" class="btn btn-sm btn-success">
                        <i class="fa fa-plus"></i> Banner
                    </a>
                </div>
            </div>
            <div class="block-content">
                
                @if ($items)
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 3%;">#</th>
                                <th style="width: 25%;">Question</th>
                                <th>Answer</th>
                                <th style="width: 10%;">Type</th>
                                <th class="text-center" style="width: 15%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td>{{ $item->question }}</td>
                                    <td>{{ $item->answer }}</td>
                                    <td>{{ $item->banner_type }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('admin-manage-site-banners-edit', ['id' => $item->id]) }}" class="btn btn-sm btn-alt-info"><i class="fa fa-pencil"></i> Edit</a>
                                        <button class="btn btn-sm btn-alt-secondary" onclick="removePage({{ $item->id }})"><i class="fa fa-trash"></i> Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif

            </div>
        </div> --}}
    </div>
@endsection

@section('js_after')
    <!-- Page JS Code -->
    {{-- <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>
    <script>
        function removePage(pageId) {
            swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover action!",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, delete it!",
                html: !1,
            })
            .then((result) => {
                if(result.value) {
                    $.ajax({
                        url: "{{ route('remove-data') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: pageId,
                            model: 'SiteBanner',
                            type: 'banner',
                        }
                    }).done(function() {
                        swal.fire({
                            title: "Success!",
                            text: "Deleted successfully.",
                            icon: "success",
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            html: !1,
                            timer: 1000
                        })
                        .then(() => {
                            window.location.reload(true)
                        })
                    });
                }
            })
        }
    </script> --}}
@endsection