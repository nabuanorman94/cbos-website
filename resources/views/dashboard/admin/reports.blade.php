@extends('layouts.backend')

@section('title')
    Reports - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="si si-graph"></i> Reports</h2>
    </div>
@endsection