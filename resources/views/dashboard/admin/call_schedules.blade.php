@extends('layouts.backend')

@section('title')
    All Call Schedule - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-phone"></i> Call Schedule > All</h2>

        <div class="row">
            <div class="col-12 col-md-9">
                <div class="p-5">
                    <div class="card">
                        <div class="card-body p-0">
                            <div id="calendar"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          
        <!-- calendar modal -->
        <div id="modal-view-qotd" class="modal modal-top fade calendar-modal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <h4 class="modal-title"><span class="event-icon"></span><span class="event-title"></span></h4>
                        <div class="event-body"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="modal-view-qotd-add" class="modal modal-top fade calendar-modal">
            <div class="modal-dialog modal-xl modal-dialog-centered">
                <div class="modal-content">
                    {{-- <form id="add-qotd" method="POST" action="{{ route('admin-resources-question-for-the-day-create-update') }}">
                        @csrf
                        <input type="hidden" name="id">
                        <input type="hidden" name="show" value="1">
                        <input type="hidden" name="start">
                        <div class="modal-body">
                        <h4 id="add-qotd-title">Add Question of the Day</h4>    
                            <div class="form-group">
                                <div class="form-material">
                                    <input name="title" id="qotd_title" class="form-control" required>
                                    <label for="qotd_title">Question</label>
                                </div>
                            </div>     
                            <div class="form-group" id="qotd_description">
                                <textarea name="description" class="form-control init-summernote"></textarea>
                            </div>        
                        </div>
                        <div class="modal-footer">
                            <div class="mx-auto">
                                <button type="submit" class="btn btn-alt-info" id="submitQotd"><i class="fa fa-floppy-o"></i> Save</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                            </div>
                        </div>
                    </form> --}}
                    <div class="alert alert-danger alert-dismissable mb-0" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        {{-- <h4 class="alert-heading font-size-h4 font-w400">Error</h4> --}}
                        <p class="mb-0">Server Error 500</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js_after')
    <script>
        var my_events = @json($schedules)

        $(document).ready(function(){
            // $("#add-qotd").submit(function(){
            //     alert("Submitted");
            //     var values = {};
            //     $.each($('#add-qotd').serializeArray(), function(i, field) {
            //         values[field.name] = field.value;
            //     });
            //     console.log(
            //         values
            //     );
            // });
        });

        (function () {    
            'use strict';
            // ------------------------------------------------------- //
            // Calendar
            // ------------------------------------------------------ //
            $(function() {
                // page is ready
                $('#calendar').fullCalendar({
                    themeSystem: 'bootstrap4',
                    businessHours: false,
                    defaultView: 'month',
                    editable: true,
                    header: {
                        left: 'title',
                        right: "prev,next today month,agendaWeek,agendaDay,listWeek"
                    },
                    events: my_events,
                    // events: [
                    //     {
                    //         title: 'My Event',
                    //         start: '2021-04-06',
                    //         description: '<p style="text-align: center; "><iframe frameborder="0" src="//www.youtube.com/embed/NzRTIvidxSQ" width="640" height="360" class="note-video-clip"></iframe><br></p>'
                    //     }
                    // ],
                    eventRender: function(event, element) {
                        if(event.icon){
                            element.find(".fc-title").prepend("<i class='fa fa-"+event.icon+"'></i>");
                        }
                        element.popover({
                            title: event.title,
                            // content: event.description,
                            trigger: 'hover',
                            placement: 'top',
                            container: 'body',
                            // html: true,
                            // sanitize: false 
                        });
                    },
                    // dayClick: function(date) {
                    //     // resetQotdForm();
                    //     // Check if selected date has already an event
                    //     var my_date = date.format();
                    //     var found = false;
                    //     var my_event = null;

                    //     my_events.map((item) => {
                    //         if(item.start == my_date) {
                    //             found = true;
                    //             my_event = item;
                    //         }
                    //     })

                    //     // if(found) {
                    //     //     $('#add-qotd-title').html('Update Question of the Day');
                    //     //     $('#add-qotd input[name="id"]').val(my_event.id);
                    //     //     $('#add-qotd input[name="title"]').val(my_event.title);
                    //     //     $('textarea[name="description"]').val(my_event.description);
                    //     //     $("#qotd_description .note-editable").html(my_event.description);
                    //     //     $('#submitQotd').html('<i class="fa fa-floppy-o"></i> Update');
                    //     // }

                    //     // $('#modal-view-qotd-add input[name="start"]').val(date.format());
                    //     $('#modal-view-qotd-add').modal();
                    // },
                    eventClick: function(event, jsEvent, view) {
                            $('.event-icon').html("<i class='fa fa-"+event.icon+"'></i>");
                            $('.event-title').html(event.title);
                            $('.event-body').html(event.description);
                            $('.eventUrl').attr('href',event.url);
                            $('#modal-view-qotd').modal();
                    },
                })
            });
        
        })($);
        
        function resetQotdForm() {
            $('#add-qotd-title').html('Add Question of the Day');
            $('#add-qotd input[name="id"]').val('');
            $('#add-qotd input[name="title"]').val('');
            $('textarea[name="description"]').val('');
            $("#qotd_description .note-editable").html('');
            $('#submitQotd').html('<i class="fa fa-floppy-o"></i> Save');
            $('#modal-view-qotd-add input[name="start"]').val('');
        }
    </script>
@endsection