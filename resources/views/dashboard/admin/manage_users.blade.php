@extends('layouts.backend')

@section('title')
    Manage Users - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="si si-users"></i> Manage Users</h2>
    </div>
@endsection