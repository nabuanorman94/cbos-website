@extends('layouts.backend')

@section('title')
    Manage Call Schedule - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-phone"></i> Call Schedule > Manage</h2>

        <form action="{{ route('admin-call-schedule-create') }}" method="post">
            @csrf
            <div class="row">
                {{-- <div class="col-12 col-lg-3">
                    <div class="p-5">
                        <div class="card">
                            <div class="card-body p-0">
                                <div class="form-group col-12">
                                    <label for="schedule_date">Select Date</label>
                                    <input type="text" class="form-control w-100" id="schedule_date" name="schedule_date" data-inline="true">
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-12 col-lg-8 d-flex">
                    <div class="form-group">
                        <label for="schedule_date">Select Date</label>
                        <input type="text" class="form-control w-100" id="schedule_date" name="schedule_date" data-inline="true">
                    </div>
                    @if (count($schedules) == 0)
                        <div class="w-100 ml-4">
                            <div class="d-flex">
                                <button type="submit" class="btn btn-success ml-auto"><i class="fa fa-plus"></i> Create Schedule</button>
                            </div>
                            <div class="alert alert-info mb-0 mt-3" role="alert">
                                <p class="mb-0">No Schedule Found</p>
                            </div>
                        </div>
                    @else
                        <div class="w-100 ml-4">
                            <div class="row ml-4">
                                <div class="col-12 col-sm-4">
                                    <h4 class="text-center">Available</h4>
                                    @foreach ($schedules as $item)
                                        @if ($item->status == 1)
                                            <div class="d-flex align-items-center mb-3">
                                                <button type="button" class="btn btn-rounded btn-warning min-width-125 mr-3">{{ $item->time_start }}</button>
                                                <label class="css-control css-control-sm css-control-info css-switch" for="value_{{ $item->id }}">
                                                    <input type="checkbox" class="css-control-input" id="value_{{ $item->id }}" name="show_{{ $item->id }}" data-id="{{ $item->id }}" true-value="1" false-value="0" {{ ($item && $item->status == 1)? 'checked' : '' }}>
                                                    <span class="css-control-indicator"></span>
                                                </label>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="col-12 col-sm-4">
                                    <h4 class="text-center">Not Available</h4>
                                    @foreach ($schedules as $item)
                                        @if ($item->status == 0)
                                            <div class="d-flex align-items-center mb-3">
                                                <button type="button" class="btn btn-rounded btn-secondary min-width-125 mr-3">{{ $item->time_start }}</button>
                                                <label class="css-control css-control-sm css-control-info css-switch" for="value_{{ $item->id }}">
                                                    <input type="checkbox" class="css-control-input" id="value_{{ $item->id }}" name="show_{{ $item->id }}" data-id="{{ $item->id }}" true-value="1" false-value="0" {{ ($item && $item->status == 1)? 'checked' : '' }}>
                                                    <span class="css-control-indicator"></span>
                                                </label>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="col-12 col-sm-4">
                                    <h4 class="text-center">Booked</h4>
                                    @foreach ($schedules as $item)
                                        @if ($item->status == 3)
                                            <div class="d-flex align-items-center text-center mb-3">
                                                <button type="button" class="btn btn-rounded btn-info min-width-125 mx-auto">{{ $item->time_start }}</button>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </form>
    </div>
@endsection

@section('css_after')
    <style>
        .event {
            position: absolute;
            width: 3px;
            height: 3px;
            border-radius: 150px;
            bottom: 3px;
            left: calc(50% - 1.5px);
            content: " ";
            display: block;
            background: #3d8eb9;
        }

        .event.busy {
            background: #f64747;
        }
    </style>
@endsection

@section('js_after')
    <script>
        var active_dates = @json($active_dates);

        console.log(active_dates)
        
        // Better always use a two digit format in your dates obj
        function get2DigitFmt(val) {
            return ('0' + val).slice(-2);
        }

        function getClass(value) {
        // Here you could put any logic you want. Ifs, add the value to a string, whatever...
            return value === 4 ? 'redClass' : 'blueClass';
        }

        flatpickr("#schedule_date", {
            altInput: true,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d",
            defaultDate: "{{ $date }}",
            onChange: function(selectedDates, dateStr, instance) {
                var url = '{{ route("admin-call-schedule-manage", [ "date" => ":date" ]) }}';
                url = url.replace(':date', dateStr);
                window.location.href = url;
            },
            onDayCreate: function(dObj, dStr, fp, dayElem) {
                var date = dayElem.dateObj;
                var my_date = moment(date).format('Y-MM-DD')
                // console.log(String(my_date))
                if(active_dates.indexOf(my_date) >= 0) {
                    console.log(my_date)
                    dayElem.innerHTML += "<span class='event'></span>";
                }
            }
        });

        $('input:checkbox').change(function(){
            var itemId = $(this).data('id')
            var value = (this.checked)? 1 : 0

            $.ajax({
                url: "{{ route('update-switch') }}",
                method: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: itemId,
                    model: 'CallSchedule',
                    type: 'schedule',
                    status: value
                }
            }).done(function() {
                swal.fire({
                    title: "Success!",
                    text: "Updated successfully.",
                    icon: "success",
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    html: !1,
                    timer: 1000
                })
                .then(() => {
                    window.location.reload(true)
                })
            });
        });
    </script>
@endsection