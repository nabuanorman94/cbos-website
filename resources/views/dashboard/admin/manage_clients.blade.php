@extends('layouts.backend')

@section('title')
    Manage Clients - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-building-o"></i> Manage Clients</h2>
    </div>
@endsection