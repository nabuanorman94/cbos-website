@extends('layouts.backend')

@section('title')
    Add Feature Section - Page Builder - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="si si-book-open"></i> Pages > Page Builder > Add Feature Section</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">{{ $blog->title }} <small>(<i>https://cbos.com.ph/{{ $blog->slug }}</i>)</small></h3>
                <div class="block-options">
                    <a target="_blank" href="{{ route('bb-preview-page', ['slug' => $blog->slug]) }}" type="button" class="btn btn-sm btn-alt-success">
                        <i class="fa fa-eye"></i> Preview
                    </a>
                    <a href="{{ route('admin-resources-blog-builder', ['slug' => $blog->slug]) }}" type="button" class="btn btn-sm btn-alt-secondary">
                        <i class="fa fa-arrow-left"></i> back
                    </a>
                </div>
            </div>
            <div class="block-content">
                <form action="{{ route('bb-update-data') }}" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-12">
                            @csrf
                            <input type="hidden" name="id" value="{{ $feature_section->id }}">
                            @foreach ($data_array as $key => $value)
                                <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                            @endforeach
                            <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#btabs-basic">Basic</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#btabs-advance">Advanced</a>
                                </li>
                            </ul>
                            <div class="block-content tab-content">
                                <div class="tab-pane active" id="btabs-basic" role="tabpanel">
                                    
                                </div>
                                <div class="tab-pane" id="btabs-advance" role="tabpanel">
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="ps_div_image_background">Background Image</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="ps_div_image_background" name="div_image_background_file">
                                                <label class="custom-file-label" for="ps_div_image_background">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="ps_div_class" name="div_class" value="{{ ($feature_section && $feature_section->div_class)? $feature_section->div_class : old('div_class') }}">
                                                <label for="ps_div_class">Class Name</label>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="ps_div_id" name="div_id" value="{{ ($feature_section && $feature_section->div_id)? $feature_section->div_id : old('div_id') }}">
                                                <label for="ps_div_id">ID Name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="ps_div_styling">Styling</label>
                                            <textarea class="form-control" id="ps_div_styling" name="div_styling" rows="8">{{ ($feature_section && $feature_section->div_styling)? $feature_section->div_styling : old('div_styling') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-3">
                                    <label for="image_placement">Image Position</label>
                                    <select name="image_placement" id="image_placement" class="form-control" required>
                                        <option value="left" {{ ($feature_section && $feature_section->image_placement == 'left')? 'selected' : '' }}>Left</option>
                                        <option value="right" {{ ($feature_section && $feature_section->image_placement == 'right')? 'selected' : '' }}>Right</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="feat_image">Feature Image</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="feat_image" name="feat_image_file">
                                                <label class="custom-file-label" for="feat_image">Choose file</label>
                                            </div>
                                        </div>
                                        @if ($feature_section && $feature_section->feat_image)
                                            <hr>
                                            <div class="text-center mx-3">
                                                <img src="{{ $feature_section->feat_image }}" alt="{{ $feature_section->feat_title }} Feature Image" class="w-100 mt-2">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="feat_content">Feature Content</label>
                                            <textarea class="form-control  init-summernote" id="feat_content" name="feat_content" rows="8">{{ ($feature_section && $feature_section->feat_content)? $feature_section->feat_content : old('feat_content') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="mt-4 mb-3 text-center">
                        <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> Save & Continue</button>
                        <a href="{{ route('admin-resources-blog-builder', ['slug' => $blog->slug]) }}" type="button" class="btn btn-secondary ml-2"><i class="fa fa-times"></i> Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection