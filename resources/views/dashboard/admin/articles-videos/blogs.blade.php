@extends('layouts.backend')

@section('title')
    News & Blogs - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-file-text-o"></i> News & Blogs</h2>

        <div class="form-group row">
            <div class="col-12 d-flex">
                <a href="{{ route('admin-resources-manage-blog-categories') }}" class="btn btn-alt-secondary ml-auto" data-toggle="popover" data-placement="top" title="Manage Categories">
                    <i class="fa fa-list-ul"></i>
                </a>
                <select name="blog_category" id="blog_category" class="form-control ml-3 w-50">
                    @if (count($categories) > 0)
                        <option value="all" selected>All</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->slug }}" {{ ($slug == $category->slug)? 'selected' : '' }}>{{ $category->name }}</option>
                        @endforeach
                    @else 
                        <option value="" selected disabled>No Categories Found</option>
                    @endif
                </select>
                <div class="ml-auto w-100 text-right">
                    
                    <a href="{{ route('admin-resources-blog-add') }}" type="button" class="btn btn-sm btn-success">
                        <i class="fa fa-plus"></i> Add New Blog
                    </a>
                </div>
            </div>
        </div>
        {{-- List all blogs here --}}
        @if (count($blogs) > 0)
        <div class="row">
            @foreach ($blogs as $item)
                <div class="col-12 col-md-2 col-xl-3">
                    <div class="block d-flex flex-column">
                        <div class="block-content block-content-full bg-image min-height-175 flex-grow-0" style="background-color: {{ $item->bg_color }};background-image: url('{{ $item->seo_image }}');background-position: center; background-repeat: no-repeat;">
                            <a class="badge badge-secondary font-w700 p-2 text-uppercase" href="javascript:void(0)">
                                {{ $item->blogCategory->name }}
                            </a>
                        </div>
                        <div class="block-content flex-grow-1">
                            <h5 class="mb-5">
                                <a class="text-dark" href="javascript:void(0)">
                                    {{ $item->title }}
                                </a>
                            </h5>
                            <div class="d-flex mb-3">
                                <p class="text-muted mb-0">
                                    {{ $item->author }}
                                </p>
                                <div class="ml-auto">
                                    {{ \Carbon\Carbon::createFromTimeStamp(strtotime($item->created_at))->diffForHumans() }}
                                </div>
                            </div>
                        </div>
                        <div class="block-content py-15 bg-body-light flex-grow-0">
                            <div class="row no-gutters font-size-sm text-center d-flex align-items-center">
                                <div class="col-2">
                                    <a class="text-muted font-w600" href="javascript:void(0)" data-toggle="popover" data-placement="top" title="Number of Views">
                                        <i class="fa fa-fw fa-eye mr-5"></i> {{ $item->count_view }}
                                    </a>
                                </div>
                                <div class="col-2">
                                    <a class="text-muted font-w600" href="javascript:void(0)" data-toggle="popover" data-placement="top" title="Number of Likes">
                                        <i class="fa fa-fw fa-heart mr-5"></i> {{ $item->count_likes }}
                                    </a>
                                </div>
                                <div class="col-3 w-100 ml-auto">
                                    {{-- <a href="javascript:void(0)" class="ml-2 text-muted">
                                        <i class="si si-docs"></i>
                                    </a> --}}
                                    <a href="{{ route('admin-resources-blog-edit', ['slug' => $item->slug]) }}" class="ml-2 text-muted" data-toggle="popover" data-placement="top" title="Manage Blog Heading">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="{{ route('admin-resources-blog-builder', ['slug' => $item->slug]) }}" class="ml-2 text-muted" data-toggle="popover" data-placement="top" title="Go to Blog Builder">
                                        <i class="fa fa-clipboard"></i>
                                    </a>
                                    <a href="javascript:void(0)" onclick="removePage({{ $item->id }})" class="ml-2 text-muted" data-toggle="popover" data-placement="top" title="Remove Blog">
                                        <i class="fa fa-trash"></i>
                                    </a> 
                                </div>
                                <div class="col-2">
                                    <label class="css-control css-control-sm css-control-info css-switch" for="value_{{ $item->id }}">
                                        <input type="checkbox" class="css-control-input" id="value_{{ $item->id }}" name="show_{{ $item->id }}" data-id="{{ $item->id }}" true-value="1" false-value="0" {{ ($item && $item->show == 1)? 'checked' : '' }}>
                                        <span class="css-control-indicator"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            @else
                No Data Found
            @endif
        </div>
        <div class="mt-3">
            {{ $blogs->render("pagination::bootstrap-4") }}
        </div>
    </div>
@endsection

@section('js_after')
    <script>
        $('select[name="blog_category"]').change(function(){
            let category = $(this).val();
            let url = "{{ route('admin-resources-blogs', ['category' => 'category_value']) }}";
            window.location.href = url.replace('category_value', category);
        })

        $('input:checkbox').change(function(){
            var itemId = $(this).data('id')
            var value = (this.checked)? 1 : 0

            $.ajax({
                url: "{{ route('update-switch') }}",
                method: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: itemId,
                    model: 'Blog',
                    show: value
                }
            }).done(function() {
                swal.fire({
                    title: "Success!",
                    text: "Updated successfully.",
                    icon: "success",
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    html: !1,
                    timer: 1000
                })
                .then(() => {
                    window.location.reload(true)
                })
            });
        });
    </script>
    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>
    <script>
        function removePage(pageId) {
            swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover action!",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, delete it!",
                html: !1,
            })
            .then((result) => {
                if(result.value) {
                    $.ajax({
                        url: "{{ route('bb-remove-data') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: pageId,
                            model: 'Blog',
                        }
                    }).done(function() {
                        swal.fire({
                            title: "Success!",
                            text: "Deleted successfully.",
                            icon: "success",
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            html: !1,
                            timer: 1000
                        })
                        .then(() => {
                            window.location.reload(true)
                        })
                    });
                }
            })
        }
    </script>
@endsection