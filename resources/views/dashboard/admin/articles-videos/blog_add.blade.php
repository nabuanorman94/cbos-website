@extends('layouts.backend')

@section('title')
    Add Blog - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="si si-book-open"></i> News & Blogs > Add</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Add Blog</h3>
                <div class="block-options">
                    <a href="{{ route('admin-resources-blogs', ['category' => 'all']) }}" type="button" class="btn btn-sm btn-secondary">
                        <i class="fa fa-arrow-left"></i> All Blogs
                    </a>
                </div>
            </div>
            <div class="block-content">
                <form action="{{ route('admin-resources-blog-add-submit') }}" method="POST" enctype="multipart/form-data" id="blog-add-form">
                    <div class="row">
                        <div class="col-sm-12 col-md-8 col-lg-8">
                            @csrf

                            @foreach ($data_array as $key => $value)
                                <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                            @endforeach

                            <input type="hidden" name="id" value="{{ ($item) ? $item->id : null }}">

                            <div class="form-group row">
                                <div class="col-3">
                                    <label for="blog_category_id">Blog Type</label>
                                    <select name="blog_category_id" id="blog_category_id" class="form-control" required>
                                        <option value="" selected disabled>Select Blog Type</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}" {{ ($item && $category->id == $item->blog_category_id)? 'selected' : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-5">
                                    <div class="form-material">
                                        <input type="text" class="form-control" id="p_title" name="title" placeholder="" value="{{ ($item && $item->title)? $item->title : old('title') }}">
                                        <label for="p_title">Blog Title</label>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-material">
                                        <input type="text" class="form-control" id="p_slug" name="slug" placeholder="" value="{{ ($item && $item->slug)? $item->slug : old('slug') }}">
                                        <label for="p_slug">Slug</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-3">
                                    <label for="heading_position">Heading Position</label>
                                    <select name="heading_position" id="heading_position" class="form-control" required>
                                        <option value="left" {{ ($item && $item->heading_position == 'left')? 'selected' : '' }}>Left</option>
                                        <option value="right" {{ ($item && $item->heading_position == 'right')? 'selected' : '' }}>Right</option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <label for="color-picker">Header Color</label>
                                    <input type="text" class="form-control" id="color-picker" name="bg_color" placeholder="" value="{{ ($item && $item->bg_color)? $item->bg_color : '#174579' }}">
                                </div>
                                <div class="col-2">
                                    <p></p>
                                    <label class="css-control css-control-info css-switch mt-5">
                                        <input type="checkbox" class="css-control-input" name="show" true-value="1" false-value="0" {{ ($item && $item->show == 1)? 'checked' : '' }}>
                                        <span class="css-control-indicator"></span> Show
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 col-md-6">
                                    <label for="banner_image">Banner Image</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="banner_image" name="banner_image_file">
                                        <label class="custom-file-label" for="banner_image">Choose file</label>
                                    </div>
                                    @if ($item && $item->banner_image)
                                        <div>
                                            <img src="{{ $item->banner_image }}" alt="Banner Image" class="w-50 mt-2">
                                        </div>
                                    @endif
                                </div>
                                <div class="col-12 col-md-6">
                                    <label for="banner_image_mobile">Banner Image for Mobile</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="banner_image_mobile" name="banner_image_mobile_file">
                                        <label class="custom-file-label" for="banner_image_mobile">Choose file</label>
                                    </div>
                                    @if ($item && $item->banner_image_mobile)
                                        <div>
                                            <img src="{{ $item->banner_image_mobile }}" alt="Banner Image Mobile" class="w-50 mt-2">
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material">
                                        <input type="text" class="form-control" id="author" name="author" placeholder="" value="{{ ($item && $item->author)? $item->author : old('author') }}">
                                        <label for="author">Blog Author</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 for-banner">
                                    <label for="sub_title">Details</label>
                                    <textarea class="form-control init-summernote" id="sub_title" name="sub_title" rows="8">{{ ($item && $item->sub_title)? $item->sub_title : old('sub_title') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-3">
                            {{-- SEO FORM --}}
                            @include('dashboard.includes.seo_form')
                        </div>
                    </div>
                    
                    <div class="mt-4 mb-3 text-center">
                        <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> {{ ($item && $item->id != null)? 'Update' : 'Save & Continue' }}</button>
                        <a href="{{ route('admin-resources-blogs', ['category' => 'all']) }}" type="button" class="btn btn-secondary ml-2"><i class="fa fa-times"></i> Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js_after')
    <script>
        $('#color-picker').spectrum({
            type: "component"
        });

        $('#blog-add-form input[name="title"]').keyup(function() {
            $('#blog-add-form input[name="slug"]').val(convertToSlug(this.value))
        });
    </script>
@endsection