@extends('layouts.backend')

@section('title')
    Manage Categories - News & Blogs - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-file-text-o"></i> News & Blogs > Manage Categories</h2>
        
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Banner List</h3>
                <div class="block-options">
                    <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#add-blog-category-modal">
                        <i class="fa fa-plus"></i> Add Blog Category
                    </button>
                    <a href="{{ route('admin-resources-blogs', ['category' => 'all']) }}" type="button" class="btn btn-sm btn-secondary">
                        <i class="fa fa-arrow-left"></i> All Blogs
                    </a>
                </div>
            </div>
            <div class="block-content">
                
                @if ($items)
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 3%;">#</th>
                                <th style="width: 10%;">Image</th>
                                <th style="width: 25%;">Name</th>
                                <th>Description</th>
                                <th class="text-center"  style="width: 10%;">Show to Live</th>
                                <th class="text-center" style="width: 15%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td>
                                        @if ($item->image)
                                            <img src="{{ $item->image }}" alt="{{ $item->name }}" class="w-100">
                                        @endif
                                    </td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->description }}</td>
                                    <td class="text-center">
                                        <label class="css-control css-control-sm css-control-info css-switch" for="value_{{ $item->id }}">
                                            <input type="checkbox" class="css-control-input" id="value_{{ $item->id }}" name="show_{{ $item->id }}" data-id="{{ $item->id }}" true-value="1" false-value="0" {{ ($item && $item->show == 1)? 'checked' : '' }}>
                                            <span class="css-control-indicator"></span>
                                        </label>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-sm btn-alt-info" onclick="editBlogCategory({{ $item }})"><i class="fa fa-pencil"></i> Edit</button>
                                        <button class="btn btn-sm btn-alt-secondary" onclick="removeBlogCategory({{ $item->id }})"><i class="fa fa-trash"></i> Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif

            </div>
        </div>
        <div class="modal fade" id="add-blog-category-modal" tabindex="-1" role="dialog" aria-labelledby="add-blog-category-modal" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-dialog-popout" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent">
                        <div class="block-header bg-primary">
                            <h3 class="block-title">Add Blog Category</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <form action="{{ route('create-other-data') }}" method="POST" enctype="multipart/form-data" id="blog-category-form">
                                @csrf
                                <input type="hidden" name="model" value="BlogCategory">
    
                                <div class="block">
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="add_name" name="name">
                                            <label for="add_name">Name</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="add_slug" name="slug">
                                            <label for="add_slug">Slug</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <textarea type="text" class="form-control" id="add_description" name="description"></textarea>
                                            <label for="add_description">Description</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add_image">Image</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="add_image" name="image_file">
                                            <label class="custom-file-label" for="add_image">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                
                                <hr>
                                <div class="form-group mt-3 text-center">
                                    <button type="submit" class="btn btn-alt-info">
                                        <i class="fa fa-floppy-o"></i> Add Category
                                    </button>
                                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">
                                        <i class="fa fa-times"></i> Cancel
                                    </button>
                                </div>
        
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="edit-blog-category-modal" tabindex="-1" role="dialog" aria-labelledby="edit-blog-category-modal" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-dialog-popout" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent">
                        <div class="block-header bg-primary">
                            <h3 class="block-title">Edit Blog Category</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <form action="{{ route('update-other-data') }}" method="POST" enctype="multipart/form-data" id="edit-blog-category-form">
                                @csrf
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="model" value="BlogCategory">
    
                                <div class="block">
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="add_name" name="name">
                                            <label for="add_name">Name</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="add_slug" name="slug">
                                            <label for="add_slug">Slug</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <textarea type="text" class="form-control" id="add_description" name="description"></textarea>
                                            <label for="add_description">Description</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add_image">Image</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="add_image" name="image_file">
                                            <label class="custom-file-label" for="add_image">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                
                                <hr>
                                <div class="form-group mt-3 text-center">
                                    <button type="submit" class="btn btn-alt-info">
                                        <i class="fa fa-floppy-o"></i> Update Category
                                    </button>
                                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">
                                        <i class="fa fa-times"></i> Cancel
                                    </button>
                                </div>
        
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js_after')
    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>
    <script>
        function editBlogCategory(blog) {
            $('#edit-blog-category-form input[name="id"]').val(blog.id);
            $('#edit-blog-category-form input[name="name"]').val(blog.name);
            $('#edit-blog-category-form input[name="slug"]').val(blog.slug);
            $('#edit-blog-category-form textarea[name="description"]').html(blog.description);
            $('#edit-blog-category-modal').modal('toggle');
        }

        function removeBlogCategory(blogCategoryId) {
            swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover action!",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, delete it!",
                html: !1,
            })
            .then((result) => {
                if(result.value) {
                    $.ajax({
                        url: "{{ route('remove-other-data') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: blogCategoryId,
                            model: 'BlogCategory',
                        }
                    }).done(function() {
                        swal.fire({
                            title: "Success!",
                            text: "Deleted successfully.",
                            icon: "success",
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            html: !1,
                            timer: 1000
                        })
                        .then(() => {
                            window.location.reload(true)
                        })
                    });
                }
            })
        }

        $('input:checkbox').change(function(){
            var itemId = $(this).data('id')
            var value = (this.checked)? 1 : 0

            $.ajax({
                url: "{{ route('update-switch') }}",
                method: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: itemId,
                    model: 'BlogCategory',
                    type: 'page',
                    show: value
                }
            }).done(function() {
                swal.fire({
                    title: "Success!",
                    text: "Updated successfully.",
                    icon: "success",
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    html: !1,
                    timer: 1000
                })
                .then(() => {
                    window.location.reload(true)
                })
            });
        });

        $('#blog-category-form input[name="name"]').keyup(function() {
            $('#blog-category-form input[name="slug"]').val(convertToSlug(this.value))
        });

        $('#edit-blog-category-form input[name="name"]').keyup(function() {
            $('#edit-blog-category-form input[name="slug"]').val(convertToSlug(this.value))
        });

    </script>
@endsection