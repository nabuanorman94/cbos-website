@if (isset($faq_type))
    {{-- Get All Faqs per type --}}
    @php
        $faqs = App\SiteFaq::where('faq_type', $faq_type)->get();
    @endphp
    {{-- FAQs List  --}}
    <div class="pt-0">
        <h2 class="content-heading"><i class="fa fa-cogs"></i> Frequently Asked Questions</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">FAQs List</h3>
                <div class="block-options">
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add-faq-section-modal"><i class="fa fa-plus"></i> Add Frequently Asked Question</button>
                </div>
            </div>
            <div class="block-content">
                
                @if ($faqs)
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 3%;">#</th>
                                <th style="width: 25%;">Question</th>
                                <th>Answer</th>
                                <th class="text-center" style="width: 15%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($faqs as $faq)
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td>{{ $faq->question }}</td>
                                    <td>
                                        <div class="html-content">
                                            {!! $faq->answer !!}
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-sm btn-alt-info" onclick="editFaq({{ $faq }})"><i class="fa fa-pencil"></i> Edit</button>
                                        <button class="btn btn-sm btn-alt-secondary" onclick="removeFaq({{ $faq->id }})"><i class="fa fa-trash"></i> Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif

            </div>
        </div>
    </div>
@endif

{{-- Manage FAQs per blog Modal --}}
<div class="modal fade" id="add-faq-section-modal" tabindex="-1" role="dialog" aria-labelledby="add-faq-section-modal" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Add Section</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <form action="{{ route('create-data') }}" method="POST" id="faq-section-form">
                        @csrf
                        <input type="hidden" name="id" value="">
                        <input type="hidden" name="faq_type" value="{{ $faq_type }}">
                        <input type="hidden" name="model" value="SiteFaq">

                        <div class="block">
                            <div class="form-group">
                                <div class="form-material ">
                                    <input type="text" class="form-control" id="question" name="question" placeholder="" value="">
                                    <label for="question">Question</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12" id="faq_answer">
                                    <label for="answer">Answer</label>
                                    <textarea class="form-control init-summernote" id="answer" name="answer" rows="8"></textarea>
                                </div>
                            </div>
                        </div>
                        
                        <hr>
                        <div class="form-group mt-3 text-center">
                            <button type="submit" class="btn btn-alt-info" id="submitFaqForm">
                                <i class="fa fa-floppy-o"></i> Add FAQ
                            </button>
                            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@section('js_after_faq')
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>
    <script>
        function editFaq(item) {
            $('#faq-section-form').attr('action', '{{ route('update-data') }}');
            $('input[name="id"]').val(item.id);
            $('input[name="faq_type"]').val('{{ $faq_type }}');
            $('input[name="question"]').val(item.question);
            $('textarea[name="answer"]').val(item.answer);
            $("#faq_answer .note-editable").html(item.answer);
            $('#submitFaqForm').html('<i class="fa fa-floppy-o"></i> Update FAQ');

            $('#add-faq-section-modal').modal('show');
        }

        function removeFaq(faqId) {
            swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover action!",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, delete it!",
                html: !1,
            })
            .then((result) => {
                if(result.value) {
                    $.ajax({
                        url: "{{ route('remove-data') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: faqId,
                            model: 'SiteFaq',
                            type: 'faq',
                        }
                    }).done(function() {
                        swal.fire({
                            title: "Success!",
                            text: "Deleted successfully.",
                            icon: "success",
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            html: !1,
                            timer: 1000
                        })
                        .then(() => {
                            window.location.reload(true)
                        })
                    });
                }
            })
        }
    </script>
@endsection