@extends('layouts.backend')

@section('title')
    About Us - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-cogs"></i> About Us</h2>

        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-6">
                <form class="container" action="{{ ($item && $item->id != null)? route('update-data') : route('create-data') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{-- {{ json_encode($data_array) }} --}}
                    @foreach ($data_array as $key => $value)
                        <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                    @endforeach

                    <input type="hidden" name="id" value="{{ ($item) ? $item->id : null }}">
                    
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="as_section_1_details">Section 1 Details</label>
                            <textarea class="form-control  init-summernote" id="section_1_details" name="section_1_details" rows="8">{{ ($item && $item->section_1_details)? $item->section_1_details : old('section_1_details') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="as_section_2_details">Section 2 Details</label>
                            <textarea class="form-control  init-summernote" id="as_section_2_details" name="section_2_details" rows="8">{{ ($item && $item->section_2_details)? $item->section_2_details : old('section_2_details') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-material ">
                                <input type="text" class="form-control" id="as_cta_name" name="cta_name" placeholder="" value="{{ ($item && $item->cta_name)? $item->cta_name : old('cta_name') }}">
                                <label for="as_cta_name">Button Label</label>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-material ">
                                <input type="text" class="form-control" id="as_cta_link" name="cta_link" placeholder="" value="{{ ($item && $item->cta_link)? $item->cta_link : old('cta_link') }}">
                                <label for="as_cta_link">Button Link</label>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <div class="col-12">
                            <label for="as_image">Image</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="as_image" name="image_file">
                                <label class="custom-file-label" for="as_image">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 col-md-6">
                            <label>Count 1 Details</label>
                            <input type="number" class="form-control mb-2" name="count_1_number" placeholder="Input Number" value="{{ ($item && $item->count_1_number)? $item->count_1_number : old('count_1_number') }}">
                            <input type="text" class="form-control mb-2" name="count_1_icon" placeholder="Icon" value="{{ ($item && $item->count_1_icon)? $item->count_1_icon : old('count_1_icon') }}">
                            <textarea class="form-control" name="count_1_details" rows="8" placeholder="Content">{{ ($item && $item->count_1_details)? $item->count_1_details : old('count_1_details') }}</textarea>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <label>Count 2 Details</label>
                            <input type="number" class="form-control mb-2" name="count_2_number" placeholder="Input Number" value="{{ ($item && $item->count_2_number)? $item->count_2_number : old('count_2_number') }}">
                            <input type="text" class="form-control mb-2" name="count_2_icon" placeholder="Icon" value="{{ ($item && $item->count_2_icon)? $item->count_2_icon : old('count_2_icon') }}">
                            <textarea class="form-control" name="count_2_details" rows="8" placeholder="Content">{{ ($item && $item->count_2_details)? $item->count_2_details : old('count_2_details') }}</textarea>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <label>Count 3 Details</label>
                            <input type="number" class="form-control mb-2" name="count_3_number" placeholder="Input Number" value="{{ ($item && $item->count_3_number)? $item->count_3_number : old('count_3_number') }}">
                            <input type="text" class="form-control mb-2" name="count_3_icon" placeholder="Icon" value="{{ ($item && $item->count_3_icon)? $item->count_3_icon : old('count_3_icon') }}">
                            <textarea class="form-control" name="count_3_details" rows="8" placeholder="Content">{{ ($item && $item->count_3_details)? $item->count_3_details : old('count_3_details') }}</textarea>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <label>Count 4 Details</label>
                            <input type="number" class="form-control mb-2" name="count_4_number" placeholder="Input Number" value="{{ ($item && $item->count_4_number)? $item->count_4_number : old('count_4_number') }}">
                            <input type="text" class="form-control mb-2" name="count_4_icon" placeholder="Icon" value="{{ ($item && $item->count_4_icon)? $item->count_4_icon : old('count_4_icon') }}">
                            <textarea class="form-control" name="count_4_details" rows="8" placeholder="Content">{{ ($item && $item->count_4_details)? $item->count_4_details : old('count_4_details') }}</textarea>
                        </div>
                    </div> --}}
                    <div class="mt-4 mb-3 text-center">
                        <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> {{ ($item && $item->id != null)? 'Update' : 'Save' }}</button>
                        <button type="button" class="btn btn-secondary ml-2" onclick="cancelEdit()"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                </form>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-3">
                {{-- @if ($item && $item->image)
                    <div class="for-main-image">
                        <img src="{{ $item->image }}" class="w-100">
                    </div>
                    <p>Image</p>
                @endif --}}
            </div>
        </div>
    </div>
@endsection