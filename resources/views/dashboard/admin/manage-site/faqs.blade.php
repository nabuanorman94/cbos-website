@extends('layouts.backend')

@section('title')
    FAQs - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        {{-- This is the FAQ CRUD section accepts faq_type --}}
            @php
            $faq_type = 'homepage';
        @endphp
        @include('dashboard.admin.common.faqs_section')
        {{-- End FAQ CRUD section --}}
    </div>
@endsection