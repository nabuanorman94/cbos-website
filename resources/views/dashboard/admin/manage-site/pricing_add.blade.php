@extends('layouts.backend')

@section('title')
    Add Pricing - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-cogs"></i> Pricing > Add</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Add Pricing</h3>
                <div class="block-options">
                    <a href="{{ route('admin-manage-site-pricing') }}" type="button" class="btn btn-sm btn-secondary">
                        <i class="fa fa-arrow-left"></i> back
                    </a>
                </div>
            </div>
            <div class="block-content">
                
              <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-6">
                    <form class="container" action="{{ ($item && $item->id != null)? route('update-data') : route('create-data') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        @foreach ($data_array as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                        @endforeach
    
                        <input type="hidden" name="id" value="{{ ($item) ? $item->id : null }}">

                        <div class="form-group row">
                            <div class="col-6">
                                <label for="pricing_type">Type</label>
                                <select name="pricing_type" id="pricing_type" class="form-control" required>
                                    <option value="" selected disabled>Select Pricing Type</option>
                                    <option value="business_registration" {{ ($item && $item->pricing_type == 'business_registration')? 'selected' : '' }}>Business Registration</option>
                                    <option value="bookkeeping_taxation" {{ ($item && $item->pricing_type == 'bookkeeping_taxation')? 'selected' : '' }}>Bookkeeping & Taxation</option>
                                    <option value="accounting_taxation" {{ ($item && $item->pricing_type == 'accounting_taxation')? 'selected' : '' }}>Accounting & Taxation</option>
                                </select>
                            </div>
                            <div class="col-3">
                                <p></p>
                                <label class="css-control css-control-info css-switch mt-5">
                                    <input type="checkbox" class="css-control-input" name="show" true-value="1" false-value="0" {{ ($item && $item->show == 1)? 'checked' : '' }}>
                                    <span class="css-control-indicator"></span> Show to Live
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material ">
                                <input type="text" class="form-control" id="package_name" name="package_name" placeholder="" value="{{ ($item && $item->package_name)? $item->package_name : old('package_name') }}">
                                <label for="package_name">Package Name</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="inclusions">Inclusions</label>
                                <textarea class="form-control  init-summernote" id="inclusions" name="inclusions" rows="8">{{ ($item && $item->inclusions)? $item->inclusions : old('inclusions') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material ">
                                <input type="text" class="form-control" id="price_one_time" name="price_one_time" placeholder="" value="{{ ($item && $item->price_one_time)? $item->price_one_time : old('price_one_time') }}">
                                <label for="price_one_time">Price One Time</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material ">
                                <input type="text" class="form-control" id="price_per_month" name="price_per_month" placeholder="" value="{{ ($item && $item->price_per_month)? $item->price_per_month : old('price_per_month') }}">
                                <label for="price_per_month">Price Per Month</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material ">
                                <input type="text" class="form-control" id="price_per_year" name="price_per_year" placeholder="" value="{{ ($item && $item->price_per_year)? $item->price_per_year : old('price_per_year') }}">
                                <label for="price_per_year">Price Per year</label>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <div class="form-material ">
                                <input type="text" class="form-control" id="price_per_year" name="price_per_year" placeholder="" value="{{ ($item && $item->price_per_year)? $item->price_per_year : old('price_per_year') }}">
                                <label for="price_per_year">Is Best Value?</label>
                            </div>
                        </div> --}}
                        
                        <div class="mt-4 mb-3 text-center">
                            <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> {{ ($item && $item->id != null)? 'Update' : 'Save' }}</button>
                            <a href="{{ route('admin-manage-site-pricing') }}" type="button" class="btn btn-secondary ml-2"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                    </form>
                </div>
            </div>

            </div>
        </div>
    </div>
@endsection