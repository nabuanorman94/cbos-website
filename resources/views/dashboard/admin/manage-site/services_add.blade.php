@extends('layouts.backend')

@section('title')
    Add Service - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-cogs"></i> Services > {{ ($item && $item->id == null)? 'Add' : 'Edit' }}</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Add Service Page</h3>
                <div class="block-options">
                    <a href="{{ route('admin-manage-site-services') }}" type="button" class="btn btn-sm btn-secondary">
                        <i class="fa fa-arrow-left"></i> back
                    </a>
                </div>
            </div>
            <div class="block-content">
                <form action="{{ ($item && $item->id != null)? route('update-data') : route('create-data') }}" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12 col-md-8 col-lg-9">
                            @csrf

                            @foreach ($data_array as $key => $value)
                                <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                            @endforeach

                            <input type="hidden" name="id" value="{{ ($item) ? $item->id : null }}">

                            <div class="form-group row">
                                <div class="col-6">
                                    <label for="service_type">Service Type</label>
                                    <select name="service_type" id="service_type" class="form-control" required>
                                        <option value="" selected disabled>Select Service Type</option>
                                        <option value="ph-registrations" {{ ($item && $item->service_type == 'ph-registrations')? 'selected' : '' }}>Ph Registrations</option>
                                        <option value="bookkeeping-accounting" {{ ($item && $item->service_type == 'bookkeeping-accounting')? 'selected' : '' }}>Bookkeeping & Accounting</option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <label for="color-picker">Header Color</label>
                                    <input type="text" class="form-control" id="color-picker" name="bg_color" placeholder="" value="{{ ($item && $item->bg_color)? $item->bg_color : '#174579' }}">
                                </div>
                                <div class="col-3">
                                    <p></p>
                                    <label class="css-control css-control-info css-switch mt-5">
                                        <input type="checkbox" class="css-control-input" name="show" true-value="1" false-value="0" {{ ($item && $item->show == 1)? 'checked' : '' }}>
                                        <span class="css-control-indicator"></span> Show to Live
                                    </label>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-12 col-md-6">
                                    <label for="image_1">Header Image</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image_1" name="image_1_file">
                                        <label class="custom-file-label" for="image_1">Choose file</label>
                                    </div>
                                    
                                    @if ($item && $item->image_1)
                                        <div>
                                            <img src="{{ $item->image_1 }}" alt="{{ $item->name }} Image 1" class="w-50 mt-2">
                                        </div>
                                    @endif
                                </div>
                                <div class="col-12 col-md-6">
                                    <label for="image_mobile">Banner Image for Mobile</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image" name="image_mobile_file">
                                        <label class="custom-file-label" for="image_mobile">Choose file</label>
                                    </div>
                                    @if ($item && $item->image_mobile)
                                        <div>
                                            <img src="{{ $item->image_mobile }}" alt="{{ $item->name }} Image Mobile" class="mt-2" style="max-height: 112px;">
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-material">
                                    <input type="text" class="form-control" id="ss_name" name="name" placeholder="" value="{{ ($item && $item->name)? $item->name : old('name') }}">
                                    <label for="ss_name">Service Name</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="details">Details <small><i>(placed under service name in service page header)</i></small></label>
                                    <textarea class="form-control" id="details" name="details" rows="5">{{ ($item && $item->details)? $item->details : old('details') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="content_1">Content 1</label>
                                    <textarea class="form-control  init-summernote" id="content_1" name="content_1" rows="8">{{ ($item && $item->content_1)? $item->content_1 : old('content_1') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="image_2">Content Image</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image_2" name="image_2_file">
                                        <label class="custom-file-label" for="image_2">Choose file</label>
                                    </div>
                                    @if ($item && $item->image_2)
                                        <div>
                                            <img src="{{ $item->image_2 }}" alt="{{ $item->name }} Image 2" class="w-50 mt-2">
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="content_2">Content 2</label>
                                    <textarea class="form-control  init-summernote" id="content_2" name="content_2" rows="8">{{ ($item && $item->content_2)? $item->content_2 : old('content_2') }}</textarea>
                                </div>
                            </div>

                            {{-- Insert Page Builder --}}

                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-3">
                            {{-- SEO FORM --}}
                            @include('dashboard.includes.seo_form')
                        </div>
                    </div>
                    <div class="mt-4 mb-3 text-center">
                        <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> {{ ($item && $item->id != null)? 'Update' : 'Save' }}</button>
                        <a href="{{ route('admin-manage-site-services') }}" type="button" class="btn btn-secondary ml-2"><i class="fa fa-times"></i> Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js_after')
    <script>
        $('#color-picker').spectrum({
            type: "component"
        });
    </script>
@endsection