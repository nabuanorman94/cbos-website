@extends('layouts.backend')

@section('title')
    Site Content - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-cogs"></i> Site Content</h2>

        <div class="row">
            <div class="col-sm-12 col-md-10 col-lg-10">
                <form class="container-fluid" action="{{ ($item && $item->id != null)? route('update-data') : route('create-data') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{-- {{ json_encode($data_array) }} --}}
                    @foreach ($data_array as $key => $value)
                        <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                    @endforeach

                    <input type="hidden" name="id" value="{{ ($item) ? $item->id : null }}">

                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="partners_heading" name="partners_heading" placeholder="" value="{{ ($item && $item->partners_heading)? $item->partners_heading : old('partners_heading') }}">
                            <label for="partners_heading">Partner's Section Heading</label>
                        </div>
                    </div>
                    {{-- Content 1 --}}
                    <div class="block">
                        <div class="row">
                            <div class="col-sm-12 col-md-8">
                                <div class="block-header block-header-default p-2">
                                    <h3 class="block-title">Content 1</h3>
                                </div>
                                <div class="block-content">
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="content_1_heading" name="content_1_heading" placeholder="" value="{{ ($item && $item->content_1_heading)? $item->content_1_heading : old('content_1_heading') }}">
                                            <label for="sd_name">Heading</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="content_1_details">Details</label>
                                            <textarea class="form-control" id="content_1_details" name="content_1_details" rows="8">{{ ($item && $item->content_1_details)? $item->content_1_details : old('content_1_details') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-2">
                                            <div class="form-material">
                                                <select name="content_1_cta_type" id="content_1_cta_type" class="form-control" onchange="getCtaType('content_1_cta_type', 'to_upload_video_1', 'content_1_cta_link')" required>
                                                    <option value="link" {{ ($item && $item->content_1_cta_type == 'link')? 'selected' : '' }}>Link</option>
                                                    <option value="video" {{ ($item && $item->content_1_cta_type == 'video')? 'selected' : '' }}>Video</option>
                                                    <option value="youtube" {{ ($item && $item->content_1_cta_type == 'youtube')? 'selected' : '' }}>Youtube Video</option>
                                                </select>
                                                <label for="content_1_cta_type">Button 1 Type</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="content_1_cta_name" name="content_1_cta_name" placeholder="" value="{{ ($item && $item->content_1_cta_name)? $item->content_1_cta_name : old('content_1_cta_name') }}">
                                                <label for="content_1_cta_name">Button 1 Label</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="content_1_cta_link" name="content_1_cta_link" placeholder="" value="{{ ($item && $item->content_1_cta_link)? $item->content_1_cta_link : old('content_1_cta_link') }}">
                                                <input type="file" class="form-control" name="to_upload_video_1" onchange="uploadVideo('to_upload_video_1', 'content_1_cta_link')" placeholder="" style="display: none;" accept="video/*">
                                                <label for="content_1_cta_link">Button 1 Link</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-2">
                                            <div class="form-material">
                                                <select name="content_1_cta_type_2" id="content_1_cta_type_2" class="form-control" onchange="getCtaType('content_1_cta_type_2', 'to_upload_video_2', 'content_1_cta_link_2')" required>
                                                    <option value="link" {{ ($item && $item->content_1_cta_type_2 == 'link')? 'selected' : '' }}>Link</option>
                                                    <option value="video" {{ ($item && $item->content_1_cta_type_2 == 'video')? 'selected' : '' }}>Video</option>
                                                    <option value="youtube" {{ ($item && $item->content_1_cta_type_2 == 'youtube')? 'selected' : '' }}>Youtube Video</option>
                                                </select>
                                                <label for="content_1_cta_type">Button 2 Type</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="content_1_cta_name_2" name="content_1_cta_name_2" placeholder="" value="{{ ($item && $item->content_1_cta_name_2)? $item->content_1_cta_name_2 : old('content_1_cta_name_2') }}">
                                                <label for="content_1_cta_name_2">Button 2 Label</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="content_1_cta_link_2" name="content_1_cta_link_2" placeholder="" value="{{ ($item && $item->content_1_cta_link_2)? $item->content_1_cta_link_2 : old('content_1_cta_link_2') }}">
                                                <input type="file" class="form-control" name="to_upload_video_2" onchange="uploadVideo('to_upload_video_2', 'content_1_cta_link_2')" placeholder="" style="display: none;" accept="video/*">
                                                <label for="content_1_cta_link_2">Button 2 Link</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="block-header block-header-default p-2">
                                    <h3 class="block-title">Homepage Sections</h3>
                                </div>
                                <div class="block-content">
                                    <div class="row">
                                        <div class="col-12">
                                            <label class="css-control css-control-info css-switch mt-2">
                                                <input type="checkbox" class="css-control-input" name="show_about_us_section" true-value="1" false-value="0" {{ ($item && $item->show_about_us_section == 1)? 'checked' : '' }}>
                                                <span class="css-control-indicator"></span> About Us
                                            </label>
                                        </div>
                                        <div class="col-12">
                                            <label class="css-control css-control-info css-switch mt-2">
                                                <input type="checkbox" class="css-control-input" name="show_testimonials_section" true-value="1" false-value="0" {{ ($item && $item->show_testimonials_section == 1)? 'checked' : '' }}>
                                                <span class="css-control-indicator"></span> Testimonials
                                            </label>
                                        </div>
                                        <div class="col-12">
                                            <label class="css-control css-control-info css-switch mt-2">
                                                <input type="checkbox" class="css-control-input" name="show_partners_section" true-value="1" false-value="0" {{ ($item && $item->show_partners_section == 1)? 'checked' : '' }}>
                                                <span class="css-control-indicator"></span> Partners
                                            </label>
                                        </div>
                                        <div class="col-12">
                                            <label class="css-control css-control-info css-switch mt-2">
                                                <input type="checkbox" class="css-control-input" name="show_content_1_section" true-value="1" false-value="0" {{ ($item && $item->show_content_1_section == 1)? 'checked' : '' }}>
                                                <span class="css-control-indicator"></span> Content 1
                                            </label>
                                        </div>
                                        <div class="col-12">
                                            <label class="css-control css-control-info css-switch mt-2">
                                                <input type="checkbox" class="css-control-input" name="show_content_2_section" true-value="1" false-value="0" {{ ($item && $item->show_content_2_section == 1)? 'checked' : '' }}>
                                                <span class="css-control-indicator"></span> Content 2
                                            </label>
                                        </div>
                                        <div class="col-12">
                                            <label class="css-control css-control-info css-switch mt-2">
                                                <input type="checkbox" class="css-control-input" name="show_faq_section" true-value="1" false-value="0" {{ ($item && $item->show_faq_section == 1)? 'checked' : '' }}>
                                                <span class="css-control-indicator"></span> FAQs
                                            </label>
                                        </div>
                                        <div class="col-12">
                                            <label class="css-control css-control-info css-switch mt-2">
                                                <input type="checkbox" class="css-control-input" name="show_pricing_section" true-value="1" false-value="0" {{ ($item && $item->show_pricing_section == 1)? 'checked' : '' }}>
                                                <span class="css-control-indicator"></span> Pricing
                                            </label>
                                        </div>
                                        <div class="col-12">
                                            <label class="css-control css-control-info css-switch mt-2">
                                                <input type="checkbox" class="css-control-input" name="show_team_section" true-value="1" false-value="0" {{ ($item && $item->show_team_section == 1)? 'checked' : '' }}>
                                                <span class="css-control-indicator"></span> Team
                                            </label>
                                        </div>
                                        <div class="col-12">
                                            <label class="css-control css-control-info css-switch mt-2">
                                                <input type="checkbox" class="css-control-input" name="show_qotd_section" true-value="1" false-value="0" {{ ($item && $item->show_qotd_section == 1)? 'checked' : '' }}>
                                                <span class="css-control-indicator"></span> Question of the Day
                                            </label>
                                        </div>
                                        <div class="col-12">
                                            <label class="css-control css-control-info css-switch mt-2">
                                                <input type="checkbox" class="css-control-input" name="show_recent_blogs_section" true-value="1" false-value="0" {{ ($item && $item->show_recent_blogs_section == 1)? 'checked' : '' }}>
                                                <span class="css-control-indicator"></span> Recent Blogs
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Content 2 --}}
                    <div class="block">
                        <div class="row">
                            <div class="col-sm-12 col-md-8">
                                <div class="block-header block-header-default p-2">
                                    <h3 class="block-title">Content 2</h3>
                                </div>
                                <div class="block-content">
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="content_2_heading" name="content_2_heading" placeholder="" value="{{ ($item && $item->content_2_heading)? $item->content_2_heading : old('content_2_heading') }}">
                                            <label for="content_2_heading">Heading</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="content_2_details">Details</label>
                                            <textarea class="form-control" id="content_2_details" name="content_2_details" rows="8">{{ ($item && $item->content_2_details)? $item->content_2_details : old('content_2_details') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-2">
                                            <div class="form-material">
                                                <select name="content_2_cta_type" id="content_2_cta_type" class="form-control" onchange="getCtaType('content_2_cta_type', 'to_upload_video_3', 'content_2_cta_link')" required>
                                                    <option value="link" {{ ($item && $item->content_2_cta_type == 'link')? 'selected' : '' }}>Link</option>
                                                    <option value="video" {{ ($item && $item->content_2_cta_type == 'video')? 'selected' : '' }}>Video</option>
                                                    <option value="youtube" {{ ($item && $item->content_2_cta_type == 'youtube')? 'selected' : '' }}>Youtube Video</option>
                                                </select>
                                                <label for="content_2_cta_type">Button 1 Type</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="content_2_cta_name" name="content_2_cta_name" placeholder="" value="{{ ($item && $item->content_2_cta_name)? $item->content_2_cta_name : old('content_2_cta_name') }}">
                                                <label for="content_2_cta_name">Button 1 Label</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="content_2_cta_link" name="content_2_cta_link" placeholder="" value="{{ ($item && $item->content_2_cta_link)? $item->content_2_cta_link : old('content_2_cta_link') }}">
                                                <input type="file" class="form-control" name="to_upload_video_3" onchange="uploadVideo('to_upload_video_3', 'content_2_cta_link')" placeholder="" style="display: none;" accept="video/*">
                                                <label for="content_2_cta_link">Button 1 Link</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-2">
                                            <div class="form-material">
                                                <select name="content_2_cta_type_2" id="content_2_cta_type_2" class="form-control" onchange="getCtaType('content_2_cta_type_2', 'to_upload_video_4', 'content_2_cta_link_2')" required>
                                                    <option value="link" {{ ($item && $item->content_2_cta_type_2 == 'link')? 'selected' : '' }}>Link</option>
                                                    <option value="video" {{ ($item && $item->content_2_cta_type_2 == 'video')? 'selected' : '' }}>Video</option>
                                                    <option value="youtube" {{ ($item && $item->content_2_cta_type_2 == 'youtube')? 'selected' : '' }}>Youtube Video</option>
                                                </select>
                                                <label for="content_2_cta_type_2">Button 2 Type</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="content_2_cta_name_2" name="content_2_cta_name_2" placeholder="" value="{{ ($item && $item->content_2_cta_name_2)? $item->content_2_cta_name_2 : old('content_2_cta_name_2') }}">
                                                <label for="content_2_cta_name_2">Button 2 Label</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="content_2_cta_link_2" name="content_2_cta_link_2" placeholder="" value="{{ ($item && $item->content_2_cta_link_2)? $item->content_2_cta_link_2 : old('content_2_cta_link_2') }}">
                                                <input type="file" class="form-control" name="to_upload_video_4" onchange="uploadVideo('to_upload_video_4', 'content_2_cta_link_2')" placeholder="" style="display: none;" accept="video/*">
                                                <label for="content_2_cta_link_2">Button 2 Link</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Newsletter Section --}}
                    <div class="block">
                        <div class="row">
                            <div class="col-sm-12 col-md-8">
                                <div class="block-header block-header-default p-2">
                                    <h3 class="block-title">Newsletter Section</h3>
                                </div>
                                <div class="block-content">
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="newsletter_heading" name="newsletter_heading" placeholder="" value="{{ ($item && $item->newsletter_heading)? $item->newsletter_heading : old('newsletter_heading') }}">
                                            <label for="newsletter_heading">Heading</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="newsletter_details">Details</label>
                                            <textarea class="form-control" id="newsletter_details" name="newsletter_details" rows="8">{{ ($item && $item->newsletter_details)? $item->newsletter_details : old('newsletter_details') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <h5 class="text-center"><b>Features</b></h5>
                    {{-- Content 2 --}}
                    <div class="block">
                        <div class="block-header block-header-default p-2">
                            <h3 class="block-title">Feature 1</h3>
                        </div>
                        <div class="block-content">
                            <div class="row">
                                <div class="col-sm-12 col-md-8">
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="feature_1_heading" name="feature_1_heading" placeholder="" value="{{ ($item && $item->feature_1_heading)? $item->feature_1_heading : old('feature_1_heading') }}">
                                            <label for="feature_1_heading">Heading</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="feature_1_details">Details</label>
                                            <textarea class="form-control" id="feature_1_details" name="feature_1_details" rows="8">{{ ($item && $item->feature_1_details)? $item->feature_1_details : old('feature_1_details') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="feature_1_image">Image</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="feature_1_image" name="feature_1_image_file">
                                                <label class="custom-file-label" for="feature_1_image">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-2">
                                            <div class="form-material">
                                                <select name="feature_1_cta_type" id="feature_1_cta_type" class="form-control" onchange="getCtaType('feature_1_cta_type', 'to_upload_video_5', 'feature_1_cta_link')" required>
                                                    <option value="link" {{ ($item && $item->feature_1_cta_type == 'link')? 'selected' : '' }}>Link</option>
                                                    <option value="video" {{ ($item && $item->feature_1_cta_type == 'video')? 'selected' : '' }}>Video</option>
                                                    <option value="youtube" {{ ($item && $item->feature_1_cta_type == 'youtube')? 'selected' : '' }}>Youtube Video</option>
                                                </select>
                                                <label for="feature_1_cta_type">Button 1 Type</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="feature_1_cta_name" name="feature_1_cta_name" placeholder="" value="{{ ($item && $item->feature_1_cta_name)? $item->feature_1_cta_name : old('feature_1_cta_name') }}">
                                                <label for="feature_1_cta_name">Button 1 Label</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="feature_1_cta_link" name="feature_1_cta_link" placeholder="" value="{{ ($item && $item->feature_1_cta_link)? $item->feature_1_cta_link : old('feature_1_cta_link') }}">
                                                <input type="file" class="form-control" name="to_upload_video_5" onchange="uploadVideo('to_upload_video_5', 'feature_1_cta_link')" placeholder="" style="display: none;" accept="video/*">
                                                <label for="feature_1_cta_link">Button 1 Link</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-2">
                                            <div class="form-material">
                                                <select name="feature_1_cta_type_2" id="feature_1_cta_type_2" class="form-control" onchange="getCtaType('feature_1_cta_type_2', 'to_upload_video_6', 'feature_1_cta_link_2')" required>
                                                    <option value="link" {{ ($item && $item->feature_1_cta_type_2 == 'link')? 'selected' : '' }}>Link</option>
                                                    <option value="video" {{ ($item && $item->feature_1_cta_type_2 == 'video')? 'selected' : '' }}>Video</option>
                                                    <option value="youtube" {{ ($item && $item->feature_1_cta_type_2 == 'youtube')? 'selected' : '' }}>Youtube Video</option>
                                                </select>
                                                <label for="feature_1_cta_type_2">Button 2 Type</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="feature_1_cta_name_2" name="feature_1_cta_name_2" placeholder="" value="{{ ($item && $item->feature_1_cta_name_2)? $item->feature_1_cta_name_2 : old('feature_1_cta_name_2') }}">
                                                <label for="feature_1_cta_name_2">Button 2 Label</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="feature_1_cta_link_2" name="feature_1_cta_link_2" placeholder="" value="{{ ($item && $item->feature_1_cta_link_2)? $item->feature_1_cta_link_2 : old('feature_1_cta_link_2') }}">
                                                <input type="file" class="form-control" name="to_upload_video_6" onchange="uploadVideo('to_upload_video_6', 'feature_1_cta_link_2')" placeholder="" style="display: none;" accept="video/*">
                                                <label for="feature_1_cta_link_2">Button 2 Link</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    @if ($item && $item->feature_1_image)
                                        <div>
                                            <img src="{{ $item->feature_1_image }}" alt="{{ $item->feature_1_heading }} " class="w-100">
                                        </div>
                                        <p>Feature Image 1</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    {{-- Content 2 --}}
                    <div class="block">
                        <div class="block-header block-header-default p-2">
                            <h3 class="block-title">Feature 2</h3>
                        </div>
                        <div class="block-content">
                            <div class="row">
                                <div class="col-sm-12 col-md-8">
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="feature_2_heading" name="feature_2_heading" placeholder="" value="{{ ($item && $item->feature_2_heading)? $item->feature_2_heading : old('feature_2_heading') }}">
                                            <label for="feature_2_heading">Heading</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="feature_2_details">Details</label>
                                            <textarea class="form-control" id="feature_2_details" name="feature_2_details" rows="8">{{ ($item && $item->feature_2_details)? $item->feature_2_details : old('feature_2_details') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="feature_2_image">Image</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="feature_2_image" name="feature_2_image_file">
                                                <label class="custom-file-label" for="feature_2_image">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-2">
                                            <div class="form-material">
                                                <select name="feature_2_cta_type" id="feature_2_cta_type" class="form-control" onchange="getCtaType('feature_2_cta_type', 'to_upload_video_7', 'feature_2_cta_link')" required>
                                                    <option value="link" {{ ($item && $item->feature_2_cta_type == 'link')? 'selected' : '' }}>Link</option>
                                                    <option value="video" {{ ($item && $item->feature_2_cta_type == 'video')? 'selected' : '' }}>Video</option>
                                                    <option value="youtube" {{ ($item && $item->feature_2_cta_type == 'youtube')? 'selected' : '' }}>Youtube Video</option>
                                                </select>
                                                <label for="feature_2_cta_type">Button 1 Type</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="feature_2_cta_name" name="feature_2_cta_name" placeholder="" value="{{ ($item && $item->feature_2_cta_name)? $item->feature_2_cta_name : old('feature_2_cta_name') }}">
                                                <label for="feature_2_cta_name">Button 1 Label</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="feature_2_cta_link" name="feature_2_cta_link" placeholder="" value="{{ ($item && $item->feature_2_cta_link)? $item->feature_2_cta_link : old('feature_2_cta_link') }}">
                                                <input type="file" class="form-control" name="to_upload_video_7" onchange="uploadVideo('to_upload_video_7', 'feature_2_cta_link')" placeholder="" style="display: none;" accept="video/*">
                                                <label for="feature_2_cta_link">Button 1 Link</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-2">
                                            <div class="form-material">
                                                <select name="feature_2_cta_type_2" id="feature_2_cta_type_2" class="form-control" onchange="getCtaType('feature_2_cta_type_2', 'to_upload_video_8', 'feature_2_cta_link_2')" required>
                                                    <option value="link" {{ ($item && $item->feature_2_cta_type_2 == 'link')? 'selected' : '' }}>Link</option>
                                                    <option value="video" {{ ($item && $item->feature_2_cta_type_2 == 'video')? 'selected' : '' }}>Video</option>
                                                    <option value="youtube" {{ ($item && $item->feature_2_cta_type_2 == 'youtube')? 'selected' : '' }}>Youtube Video</option>
                                                </select>
                                                <label for="feature_2_cta_type_2">Button 2 Type</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="feature_2_cta_name_2" name="feature_2_cta_name_2" placeholder="" value="{{ ($item && $item->feature_2_cta_name_2)? $item->feature_2_cta_name_2 : old('feature_2_cta_name_2') }}">
                                                <label for="feature_2_cta_name_2">Button 2 Label</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="feature_2_cta_link_2" name="feature_2_cta_link_2" placeholder="" value="{{ ($item && $item->feature_2_cta_link_2)? $item->feature_2_cta_link_2 : old('feature_2_cta_link_2') }}">
                                                <input type="file" class="form-control" name="to_upload_video_8" onchange="uploadVideo('to_upload_video_8', 'feature_2_cta_link_2')" placeholder="" style="display: none;" accept="video/*">
                                                <label for="feature_2_cta_link_2">Button 2 Link</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    @if ($item && $item->feature_2_image)
                                        <hr>
                                        <div>
                                            <img src="{{ $item->feature_2_image }}" alt="{{ $item->feature_2_heading }} " class="w-100">
                                        </div>
                                        <p>Feature Image 2</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Content 2 --}}
                    <div class="block">
                        <div class="block-header block-header-default p-2">
                            <h3 class="block-title">Feature 3</h3>
                        </div>
                        <div class="block-content">
                            <div class="row">
                                <div class="col-sm-12 col-md-8">
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="feature_3_heading" name="feature_3_heading" placeholder="" value="{{ ($item && $item->feature_3_heading)? $item->feature_3_heading : old('feature_3_heading') }}">
                                            <label for="feature_3_heading">Heading</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="feature_3_details">Details</label>
                                            <textarea class="form-control" id="feature_3_details" name="feature_3_details" rows="8">{{ ($item && $item->feature_3_details)? $item->feature_3_details : old('feature_3_details') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="feature_3_image">Image</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="feature_3_image" name="feature_3_image_file">
                                                <label class="custom-file-label" for="feature_3_image">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-2">
                                            <div class="form-material">
                                                <select name="feature_3_cta_type" id="feature_3_cta_type" class="form-control" onchange="getCtaType('feature_3_cta_type', 'to_upload_video_9', 'feature_3_cta_link')" required>
                                                    <option value="link" {{ ($item && $item->feature_3_cta_type == 'link')? 'selected' : '' }}>Link</option>
                                                    <option value="video" {{ ($item && $item->feature_3_cta_type == 'video')? 'selected' : '' }}>Video</option>
                                                    <option value="youtube" {{ ($item && $item->feature_3_cta_type == 'youtube')? 'selected' : '' }}>Youtube Video</option>
                                                </select>
                                                <label for="feature_3_cta_type">Button 1 Type</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="feature_3_cta_name" name="feature_3_cta_name" placeholder="" value="{{ ($item && $item->feature_3_cta_name)? $item->feature_3_cta_name : old('feature_3_cta_name') }}">
                                                <label for="feature_3_cta_name">Button 1 Label</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="feature_3_cta_link" name="feature_3_cta_link" placeholder="" value="{{ ($item && $item->feature_3_cta_link)? $item->feature_3_cta_link : old('feature_3_cta_link') }}">
                                                <input type="file" class="form-control" name="to_upload_video_9" onchange="uploadVideo('to_upload_video_9', 'feature_3_cta_link')" placeholder="" style="display: none;" accept="video/*">
                                                <label for="feature_3_cta_link">Button 1 Link</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-2">
                                            <div class="form-material">
                                                <select name="feature_3_cta_type_2" id="feature_3_cta_type_2" class="form-control" onchange="getCtaType('feature_3_cta_type_2', 'to_upload_video_10', 'feature_3_cta_link_2')" required>
                                                    <option value="link" {{ ($item && $item->feature_3_cta_type_2 == 'link')? 'selected' : '' }}>Link</option>
                                                    <option value="video" {{ ($item && $item->feature_3_cta_type_2 == 'video')? 'selected' : '' }}>Video</option>
                                                    <option value="youtube" {{ ($item && $item->feature_3_cta_type_2 == 'youtube')? 'selected' : '' }}>Youtube Video</option>
                                                </select>
                                                <label for="feature_3_cta_type_2">Button 2 Type</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="feature_3_cta_name_2" name="feature_3_cta_name_2" placeholder="" value="{{ ($item && $item->feature_3_cta_name_2)? $item->feature_3_cta_name_2 : old('feature_3_cta_name_2') }}">
                                                <label for="feature_3_cta_name_2">Button 2 Label</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="feature_3_cta_link_2" name="feature_3_cta_link_2" placeholder="" value="{{ ($item && $item->feature_3_cta_link_2)? $item->feature_3_cta_link_2 : old('feature_3_cta_link_2') }}">
                                                <input type="file" class="form-control" name="to_upload_video_10" onchange="uploadVideo('to_upload_video_10', 'feature_3_cta_link_2')" placeholder="" style="display: none;" accept="video/*">
                                                <label for="feature_3_cta_link_2">Button 2 Link</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    @if ($item && $item->feature_3_image)
                                        <hr>
                                        <div>
                                            <img src="{{ $item->feature_3_image }}" alt="{{ $item->feature_3_heading }}" class="w-100">
                                        </div>
                                        <p>Feature Image 3</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="mt-4 mb-3 text-center">
                        <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> {{ ($item && $item->id != null)? 'Update' : 'Save' }}</button>
                        <button type="button" class="btn btn-secondary ml-2" onclick="cancelEdit()"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection