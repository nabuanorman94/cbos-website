@extends('layouts.backend')

@section('title')
    Site Details - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-cogs"></i> Site Details</h2>

        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-6">
                <form class="container" action="{{ ($item && $item->id != null)? route('update-data') : route('create-data') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{-- {{ json_encode($data_array) }} --}}
                    @foreach ($data_array as $key => $value)
                        <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                    @endforeach

                    <input type="hidden" name="id" value="{{ ($item) ? $item->id : null }}">

                    <div class="form-group">
                        <h5 class="mb-0">Website Status</h5>
                        <label class="css-control css-control-primary css-radio">
                            <input type="radio" class="css-control-input" name="site_under_maintenance" value="0" @if($item->site_under_maintenance == 0) checked @endif>
                            <span class="css-control-indicator"></span> Live
                        </label>
                        <label class="css-control css-control-primary css-radio">
                            <input type="radio" class="css-control-input" name="site_under_maintenance" value="1" @if($item->site_under_maintenance == 1) checked @endif>
                            <span class="css-control-indicator"></span> Under Maintenance
                        </label>
                    </div>

                    {{-- <div class="form-group">
                        <label class="css-control css-control-primary css-switch">
                            <input type="checkbox" class="css-control-input" checked="">
                            <span class="css-control-indicator"></span> Primary
                        </label>
                    </div> --}}
                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="sd_name" name="name" placeholder="" value="{{ ($item && $item->name)? $item->name : old('name') }}">
                            <label for="sd_name">Company Name</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="description">Short Description</label>
                            <textarea class="form-control  init-summernote" id="description" name="description" rows="8">{{ ($item && $item->description)? $item->description : old('description') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input type="email" class="form-control" id="sd_email" name="email" placeholder="" value="{{ ($item && $item->email)? $item->email : old('email') }}">
                            <label for="sd_email">Email</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="sd_phone" name="phone" placeholder="" value="{{ ($item && $item->phone)? $item->phone : old('phone') }}">
                            <label for="sd_phone">Phone</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="sd_open_on" name="open_on" placeholder="" value="{{ ($item && $item->open_on)? $item->open_on : old('open_on') }}">
                            <label for="sd_open_on">Office Hours</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="sd_address" name="address" placeholder="" value="{{ ($item && $item->address)? $item->address : old('address') }}">
                            <label for="sd_address">Company Address</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="sd_logo">Main Logo</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="sd_logo" name="logo_file">
                                <label class="custom-file-label" for="sd_logo">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="sd_logo_2">Dashboard Logo</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="sd_logo_2" name="logo_2_file">
                                <label class="custom-file-label" for="sd_logo_2">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="sd_logo_3">Footer Logo</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="sd_logo_3" name="logo_3_file">
                                <label class="custom-file-label" for="sd_logo_3">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4 mb-3 text-center">
                        <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> {{ ($item && $item->id != null)? 'Update' : 'Save' }}</button>
                        <button type="button" class="btn btn-secondary ml-2" onclick="cancelEdit()"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                </form>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-3">
                @if ($item && $item->logo)
                    <div class="for-main-logo">
                        <img src="{{ $item->logo }}" alt="{{ $item->name }} Main Logo" class="w-100">
                    </div>
                    <p>Main Logo</p>
                @endif
                @if ($item && $item->logo_2)
                    <hr>
                    <div class="for-dashboard-logo">
                        <img src="{{ $item->logo_2 }}" alt="{{ $item->name }} Dashboard Logo" class="w-100">
                    </div>
                    <p>Dashboard Logo</p>
                @endif
                @if ($item && $item->logo_3)
                    <hr>
                    <div class="for-footer-logo">
                        <img src="{{ $item->logo_3 }}" alt="{{ $item->name }} Footer Logo" class="w-100">
                    </div>
                    <p>White Logo</p>
                @endif
            </div>
        </div>
    </div>
@endsection