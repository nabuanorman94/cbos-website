@extends('layouts.backend')

@section('title')
    Add Frequently Asked Questions - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-cogs"></i> Frequently Asked Questions > Add</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Add Frequently Asked Questions</h3>
                <div class="block-options">
                    <a href="{{ route('admin-manage-site-faqs') }}" type="button" class="btn btn-sm btn-secondary">
                        <i class="fa fa-arrow-left"></i> back
                    </a>
                </div>
            </div>
            <div class="block-content">
                
              <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-6">
                    <form class="container" action="{{ ($item && $item->id != null)? route('update-data') : route('create-data') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        @foreach ($data_array as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                        @endforeach
    
                        <input type="hidden" name="id" value="{{ ($item) ? $item->id : null }}">

                        <div class="form-group row">
                            <div class="col-6">
                                <label for="faq_type">Where this FAQ to be displayed</label>
                                <select name="faq_type" id="faq_type" class="form-control" required>
                                    <option value="" selected disabled>Select Page</option>
                                    <option value="homepage" {{ ($item && $item->faq_type == 'homepage')? 'selected' : '' }}>Homepage</option>
                                    @if ($pages && count($pages) > 0)
                                        @foreach ($pages as $page)
                                            <option value="{{ $page->slug }}" {{ ($page && $page->faq_type == $page->slug)? 'selected' : '' }}>{{ $page->title }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material ">
                                <input type="text" class="form-control" id="question" name="question" placeholder="" value="{{ ($item && $item->question)? $item->question : old('question') }}">
                                <label for="question">Question</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="answer">Answer</label>
                                <textarea class="form-control" id="answer" name="answer" rows="8">{{ ($item && $item->answer)? $item->answer : old('answer') }}</textarea>
                            </div>
                        </div>
                        
                        <div class="mt-4 mb-3 text-center">
                            <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> {{ ($item && $item->id != null)? 'Update' : 'Save' }}</button>
                            <a href="{{ route('admin-manage-site-faqs') }}" type="button" class="btn btn-secondary ml-2"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                    </form>
                </div>
            </div>

            </div>
        </div>
    </div>
@endsection