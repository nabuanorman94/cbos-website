@extends('layouts.backend')

@section('title')
    Add Content - Page Builder - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="si si-book-open"></i> Pages > Page Builder > Add Content</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">{{ $page->title }} <small>(<i>https://cbos.com.ph/{{ $page->slug }}</i>)</small></h3>
                <div class="block-options">
                    <a target="_blank" href="{{ route('pb-preview-page', ['slug' => $page->slug]) }}" type="button" class="btn btn-sm btn-alt-success">
                        <i class="fa fa-eye"></i> Preview
                    </a>
                    <a href="{{ route('admin-manage-site-page-builder', ['type' => 'stand-alone', 'slug' => $page->slug]) }}" type="button" class="btn btn-sm btn-alt-secondary">
                        <i class="fa fa-arrow-left"></i> back
                    </a>
                </div>
            </div>
            <div class="block-content">
                <form action="{{ route('pb-update-data') }}" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-12">
                            @csrf
                            <input type="hidden" name="id" value="{{ $content->id }}">
                            @foreach ($data_array as $key => $value)
                                <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                            @endforeach
                            <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#btabs-basic">Basic</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#btabs-advance">Advanced</a>
                                </li>
                            </ul>
                            <div class="block-content tab-content">
                                <div class="tab-pane active" id="btabs-basic" role="tabpanel">
                                    
                                </div>
                                <div class="tab-pane" id="btabs-advance" role="tabpanel">
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="ps_div_image_background">Background Image</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="ps_div_image_background" name="div_image_background_file">
                                                <label class="custom-file-label" for="ps_div_image_background">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="ps_div_class" name="div_class" value="{{ ($content && $content->div_class)? $content->div_class : old('div_class') }}">
                                                <label for="ps_div_class">Class Name</label>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-material">
                                                <input type="text" class="form-control" id="ps_div_id" name="div_id" value="{{ ($content && $content->div_id)? $content->div_id : old('div_id') }}">
                                                <label for="ps_div_id">ID Name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="ps_div_styling">Styling</label>
                                            <textarea class="form-control" id="ps_div_styling" name="div_styling" rows="8">{{ ($content && $content->div_styling)? $content->div_styling : old('div_styling') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="value">Content</label>
                                    <textarea class="form-control init-summernote" id="value" name="value" rows="8">{{ ($content && $content->value)? $content->value : old('value') }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="mt-4 mb-3 text-center">
                        <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> Save & Continue</button>
                        <a href="{{ route('admin-manage-site-page-builder', ['type' => 'stand-alone', 'slug' => $page->slug]) }}" type="button" class="btn btn-secondary ml-2"><i class="fa fa-times"></i> Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection