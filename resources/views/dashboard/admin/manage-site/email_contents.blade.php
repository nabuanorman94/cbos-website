@extends('layouts.backend')

@section('title')
    Email Contents - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="si si-envelope"></i> Email Contents</h2>

        <div class="form-group row">
            <div class="col-12 d-flex">
                
                <div class="ml-auto w-100 text-right">
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-view-email-content-add" onclick="resetForm()"><i class="fa fa-plus"></i> Add Email Content</button>
                </div>
            </div>
        </div>

        {{-- List all email contents here --}}
        @if (count($items) > 0)
            <div class="row">
            @foreach ($items as $item)
                <div class="col-12">
                    <div class="block d-flex flex-column">
                        <div class="block-content flex-grow-1">
                            <a class="badge badge-secondary font-w700 p-2 mb-4 text-uppercase" href="javascript:void(0)">
                                {{ $item->email_type }}
                            </a>
                            <h5 class="mb-3">Subject: <b>{{ $item->subject }}</b></h5>
                            <div class="row" style="background-color: #edf2f7;padding: 20px;">
                                <div class="col-12 col-md-6 offset-md-3">
                                    <div class="text-center mb-3">
                                        <img src="{{ $details->logo }}" alt="{{ $details->name }}" style="width: 120px;">
                                    </div>
                                    <div style="padding: 32px;background-color: #ffffff;">
                                        <div class="html-content">
                                            {!! $item->content !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-content py-15 bg-body-light flex-grow-0">
                            <div class="row no-gutters font-size-sm text-center align-items-center">
                                <div class="col-6 w-100">
                                    <form action="{{ route('admin-manage-site-email-contents-test-email') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $item->id }}">

                                        <div class="form-group row mb-0">
                                            <div class="col-4 pr-1">
                                                <input type="text" name="name" class="form-control" placeholder="Enter name" required>
                                            </div>
                                            <div class="col-6 pl-1 pr-1">
                                                <input type="email" name="to_email" class="form-control" placeholder="Enter email address and send test email" required>
                                            </div>
                                            <div class="col-2 pl-1">
                                                <button type="submit" class="btn btn-alt-warning btn-block" data-toggle="popover" data-placement="top" title="Send Test Email"><i class="fa fa-paper-plane"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-6 w-100 text-right">
                                    <a href="javascript:void(0)" onclick="editPage({{ $item }})" class="ml-2 text-muted" data-toggle="popover" data-placement="top" title="Edit Email Content">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="javascript:void(0)" onclick="removePage({{ $item->id }})" class="ml-2 text-muted" data-toggle="popover" data-placement="top" title="Remove Email Content">
                                        <i class="fa fa-trash"></i>
                                    </a> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
            @else
                <br><br>
                No Data Found
            @endif
        </div>
        <div class="mt-3">
            {{ $items->render("pagination::bootstrap-4") }}
        </div>
        
        <div id="modal-view-email-content-add" class="modal modal-top fade calendar-modal">
            <div class="modal-dialog modal-xl modal-dialog-centered">
                <div class="modal-content">
                    <form id="add-email-content" method="POST" action="{{ route('create-data') }}">
                        @csrf
                        <input type="hidden" name="id">
                        @foreach ($data_array as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                        @endforeach
                        <div class="modal-body">
                        <h4 id="add-email-content-title">Add Email Content</h4>    
                            <div class="form-group">
                                <div class="form-material">
                                    <input name="email_type" id="email_type" class="form-control" required>
                                    <label for="email_type">For Email</label>
                                </div>
                            </div>     
                            <div class="form-group">
                                <div class="form-material">
                                    <input name="subject" id="subject" class="form-control" required>
                                    <label for="subject">Subject</label>
                                </div>
                            </div> 
                            <div class="form-group" id="email_content">
                                <textarea name="content" class="form-control init-summernote"></textarea>
                            </div>        
                        </div>
                        <div class="modal-footer">
                            <div class="mx-auto">
                                <button type="submit" class="btn btn-alt-info" id="submitEmailContent"><i class="fa fa-floppy-o"></i> Save</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection

@section('js_after')
    <script>
        function resetForm() {
            $('#add-email-content').attr('action', '{{ route('bb-create-data') }}');
            $('#add-email-content-title').html('Add Email Content');
            $('#add-email-content input[name="id"]').val('');
            $('#add-email-content input[name="email_type"]').val('');
            $('#add-email-content input[name="subject"]').val('');
            $('textarea[name="content"]').val('');
            $("#email_content .note-editable").html('');
            $('#submitEmailContent').html('<i class="fa fa-floppy-o"></i> Save');
        }

        function editPage(item) {
            $('#add-email-content').attr('action', '{{ route('bb-update-data') }}');
            $('#add-email-content-title').html('Update Email Content');
            $('#add-email-content input[name="id"]').val(item.id);
            $('#add-email-content input[name="email_type"]').val(item.email_type).attr('readonly', 'readonly');
            $('#add-email-content input[name="subject"]').val(item.subject);
            $('textarea[name="content"]').val(item.content);
            $("#email_content .note-editable").html(item.content);
            $('#submitEmailContent').html('<i class="fa fa-floppy-o"></i> Update');
            $('#modal-view-email-content-add').modal();
        }
        
        function removePage(pageId) {
            swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover action!",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, delete it!",
                html: !1,
            })
            .then((result) => {
                if(result.value) {
                    $.ajax({
                        url: "{{ route('remove-data') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: pageId,
                            model: 'EmailContent',
                            type: 'email_content',
                        }
                    }).done(function() {
                        swal.fire({
                            title: "Success!",
                            text: "Deleted successfully.",
                            icon: "success",
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            html: !1,
                            timer: 1000
                        })
                        .then(() => {
                            window.location.reload(true)
                        })
                    });
                }
            })
        }
    </script>
@endsection