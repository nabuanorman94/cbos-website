@extends('layouts.backend')

@section('title')
    Partners - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-cogs"></i> Partners</h2>

        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-5">
                <form class="container" action="{{ route('create-data') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @foreach ($data_array as $key => $value)
                        <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                    @endforeach

                    <div class="form-group">
                        <div class="form-material ">
                            <input type="text" class="form-control" id="p_name" name="name" placeholder="" value="{{ old('name') }}">
                            <label for="p_name">Partner Name</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="as_image">Image</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="as_image" name="image_file">
                                <label class="custom-file-label" for="as_image">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4 mb-3 text-center">
                        <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> {{ 'Add' }}</button>
                        <button type="button" class="btn btn-secondary ml-2" onclick="cancelEdit()"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                </form>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-5">
                <h5>List of Partners</h5>
                @if (count($items) > 0)
                    <div class="row">
                        @foreach ($items as $item)
                            <div class="col-sm-12 col-md-3">
                                <div class="for-main-image">
                                    <img src="{{ $item->logo }}" class="w-100">
                                </div>
                                <div class="text-center">
                                    <p class="mb-1">{{ $item->name }}</p>
                                    <form action="{{ route('remove-data') }}" method="post" id="remove-partner-form">
                                        @csrf
                                        @foreach ($data_array as $key => $value)
                                            <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                                        @endforeach
                                        <input type="hidden" name="id" value="{{ $item->id }}">
                                        <button type="button" class="btn btn-sm btn-danger" onclick="removePartner({{ $item->id }})">Remove</button>
                                    </form>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="alert alert-info mb-0" role="alert">
                        <p class="mb-0">No Partners Added</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('js_after')
    <script>
        function removePartner(id) {
            swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover action!",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, remove it!",
                html: !1,
            })
            .then((result) => {
                if(result.value) {
                    document.getElementById("remove-partner-form").submit();
                }
            })
        }
    </script>
@endsection