@extends('layouts.backend')

@section('title')
    Add Social Link - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-cogs"></i> Social Link > Add</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Add Social Link</h3>
                <div class="block-options">
                    <a href="{{ route('admin-manage-site-socials') }}" type="button" class="btn btn-sm btn-secondary">
                        <i class="fa fa-arrow-left"></i> back
                    </a>
                </div>
            </div>
            <div class="block-content">
                
              <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-6">
                    <form class="container" action="{{ ($item && $item->id != null)? route('update-data') : route('create-data') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        @foreach ($data_array as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                        @endforeach
    
                        <input type="hidden" name="id" value="{{ ($item) ? $item->id : null }}">

                        <div class="form-group">
                            <div class="form-material ">
                                <input type="text" class="form-control" id="link" name="link" placeholder="" value="{{ ($item && $item->link)? $item->link : old('link') }}">
                                <label for="link">Link</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material ">
                                <input type="text" class="form-control" id="icon" name="icon" placeholder="" value="{{ ($item && $item->icon)? $item->icon : old('icon') }}">
                                <label for="icon">Icon</label>
                            </div>
                        </div>
                        
                        <div class="mt-4 mb-3 text-center">
                            <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> {{ ($item && $item->id != null)? 'Update' : 'Save' }}</button>
                            <a href="{{ route('admin-manage-site-socials') }}" type="button" class="btn btn-secondary ml-2"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                    </form>
                </div>
            </div>

            </div>
        </div>
    </div>
@endsection