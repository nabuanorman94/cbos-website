@extends('layouts.backend')

@section('title')
    Add Banners - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-cogs"></i> Banners > Add</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Add Banner</h3>
                <div class="block-options">
                    <a href="{{ route('admin-manage-site-banners') }}" type="button" class="btn btn-sm btn-secondary">
                        <i class="fa fa-arrow-left"></i> back
                    </a>
                </div>
            </div>
            <div class="block-content">
                
              <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <form class="container" action="{{ ($item && $item->id != null)? route('update-data') : route('create-data') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        @foreach ($data_array as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                        @endforeach
    
                        <input type="hidden" name="id" value="{{ ($item) ? $item->id : null }}">

                        <div class="form-group row">
                            <div class="col-3">
                                <label for="heading_position">Heading Position</label>
                                <select name="heading_position" id="heading_position" class="form-control" required>
                                    <option value="left" {{ ($item && $item->heading_position == 'left')? 'selected' : '' }}>Left</option>
                                    <option value="right" {{ ($item && $item->heading_position == 'right')? 'selected' : '' }}>Right</option>
                                </select>
                            </div>
                            <div class="col-3">
                                <label for="color-picker">Header Color</label>
                                <input type="text" class="form-control" id="color-picker" name="bg_color" placeholder="" value="{{ ($item && $item->bg_color)? $item->bg_color : '#174579' }}">
                            </div>
                            <div class="col-3">
                                <p></p>
                                <label class="css-control css-control-info css-switch mt-5">
                                    <input type="checkbox" class="css-control-input" name="show" true-value="1" false-value="0" {{ ($item && $item->show == 1)? 'checked' : '' }}>
                                    <span class="css-control-indicator"></span> Show to Live
                                </label>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="image">Banner Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="image" name="image_file">
                                    <label class="custom-file-label" for="image">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="image_mobile">Banner Image for Mobile</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="image" name="image_mobile_file">
                                    <label class="custom-file-label" for="image_mobile">Choose file</label>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <div class="form-material">
                                <input type="text" class="form-control" id="ss_title" name="title" placeholder="" value="{{ ($item && $item->title)? $item->title : old('title') }}">
                                <label for="ss_title">Title</label>
                            </div>
                        </div> --}}
                        {{-- <div class="form-group row">
                            <div class="col-12">
                                <label for="ss_title">Title</label>
                                <textarea class="form-control  init-summernote" id="ss_title" name="title" rows="8">{{ ($item && $item->title)? $item->title : old('title') }}</textarea>
                            </div>
                        </div> --}}
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="sub_title">Details</label>
                                <textarea class="form-control  init-summernote" id="sub_title" name="sub_title" rows="8">{{ ($item && $item->sub_title)? $item->sub_title : old('sub_title') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 col-md-2">
                                <div class="form-material">
                                    <select name="cta_type" id="as_cta_type" class="form-control" onchange="getCtaType('cta_type', 'to_upload_video', 'cta_link')" required>
                                        <option value="link" {{ ($item && $item->cta_type == 'link')? 'selected' : '' }}>Link</option>
                                        <option value="video" {{ ($item && $item->cta_type == 'video')? 'selected' : '' }}>Video</option>
                                        <option value="youtube" {{ ($item && $item->cta_type == 'youtube')? 'selected' : '' }}>Youtube Video</option>
                                    </select>
                                    <label for="as_cta_type">Button 1 Type</label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-5">
                                <div class="form-material">
                                    <input type="text" class="form-control" id="as_cta_name" name="cta_name" placeholder="" value="{{ ($item && $item->cta_name)? $item->cta_name : old('cta_name') }}">
                                    <label for="as_cta_name">Button 1 Label</label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-5">
                                <div class="form-material">
                                    <input type="text" class="form-control" name="cta_link" placeholder="" value="{{ ($item && $item->cta_link)? $item->cta_link : old('cta_link') }}">
                                    <input type="file" class="form-control" name="to_upload_video" onchange="uploadVideo('to_upload_video', 'cta_link')" placeholder="" style="display: none;" accept="video/*">
                                    <label>Button 1 Link</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 col-md-2">
                                <div class="form-material">
                                    <select name="cta_type_2" id="as_cta_type_2" class="form-control" onchange="getCtaType('cta_type_2', 'to_upload_video_2', 'cta_link_2')" required>
                                        <option value="link" {{ ($item && $item->cta_type_2 == 'link')? 'selected' : '' }}>Link</option>
                                        <option value="video" {{ ($item && $item->cta_type_2 == 'video')? 'selected' : '' }}>Video</option>
                                        <option value="youtube" {{ ($item && $item->cta_type_2 == 'youtube')? 'selected' : '' }}>Youtube Video</option>
                                    </select>
                                    <label for="as_cta_type_2">Button 2 Type</label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-5">
                                <div class="form-material">
                                    <input type="text" class="form-control" id="as_cta_name_2" name="cta_name_2" placeholder="" value="{{ ($item && $item->cta_name_2)? $item->cta_name_2 : old('cta_name_2') }}">
                                    <label for="as_cta_name_2">Button 2 Label</label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-5">
                                <div class="form-material">
                                    <input type="text" class="form-control" name="cta_link_2" placeholder="" value="{{ ($item && $item->cta_link_2)? $item->cta_link_2 : old('cta_link_2') }}">
                                    <input type="file" class="form-control" name="to_upload_video_2"  onchange="uploadVideo('to_upload_video_2', 'cta_link_2')" placeholder="" style="display: none;" accept="video/*">
                                    <label>Button 2 Link</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="mt-4 mb-3 text-center">
                            <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> {{ ($item && $item->id != null)? 'Update' : 'Save' }}</button>
                            <a href="{{ route('admin-manage-site-banners') }}" type="button" class="btn btn-secondary ml-2"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                    </form>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-3">
                    @if ($item && $item->image)
                        <div>
                            <img src="{{ $item->image }}" alt="{{ $item->name }} Image 1" class="w-100">
                        </div>
                        <p>Image</p>
                    @endif
                    @if ($item && $item->image_mobile)
                        <div>
                            <img src="{{ $item->image_mobile }}" alt="{{ $item->name }} Image 1" class="w-100">
                        </div>
                        <p>Image Mobile</p>
                    @endif
                </div>
            </div>

            </div>
        </div>
    </div>
@endsection

@section('js_after')
    <script>
        $('#color-picker').spectrum({
            type: "component"
        });
    </script>
@endsection