@extends('layouts.backend')

@section('title')
    Add Team Member - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-cogs"></i> Team > Add</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Add Team Member</h3>
                <div class="block-options">
                    <a href="{{ route('admin-manage-site-team') }}" type="button" class="btn btn-sm btn-secondary">
                        <i class="fa fa-arrow-left"></i> back
                    </a>
                </div>
            </div>
            <div class="block-content">
                
              <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-6">
                    <form class="container" action="{{ ($item && $item->id != null)? route('update-data') : route('create-data') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        @foreach ($data_array as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                        @endforeach
    
                        <input type="hidden" name="id" value="{{ ($item) ? $item->id : null }}">

                        <div class="form-group row">
                            <div class="col-3">
                                <p></p>
                                <label class="css-control css-control-info css-switch mt-5">
                                    <input type="checkbox" class="css-control-input" name="show" true-value="1" false-value="0" {{ ($item && $item->show == 1)? 'checked' : '' }}>
                                    <span class="css-control-indicator"></span> Show to Live
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-material ">
                                <input type="text" class="form-control" id="st_name" name="name" placeholder="" value="{{ ($item && $item->name)? $item->name : old('name') }}">
                                <label for="st_name">Name</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material ">
                                <input type="text" class="form-control" id="st_position" name="position" placeholder="" value="{{ ($item && $item->position)? $item->position : old('position') }}">
                                <label for="st_position">Position</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="image">Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="image" name="image_file">
                                    <label class="custom-file-label" for="image">Choose file</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="mt-4 mb-3 text-center">
                            <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> {{ ($item && $item->id != null)? 'Update' : 'Save' }}</button>
                            <a href="{{ route('admin-manage-site-team') }}" type="button" class="btn btn-secondary ml-2"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                    </form>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-2">
                    @if ($item && $item->image)
                        <div>
                            <img src="{{ $item->image }}" alt="{{ $item->name }} Image 1" class="w-100">
                        </div>
                        <p>Image</p>
                    @endif
                </div>
            </div>

            </div>
        </div>
    </div>
@endsection