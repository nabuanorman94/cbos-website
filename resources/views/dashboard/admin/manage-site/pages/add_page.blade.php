@extends('layouts.backend')

@section('title')
    Add Page - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="si si-book-open"></i> Pages > Add</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Add Page</h3>
                <div class="block-options">
                    <a href="{{ route('admin-manage-site-pages-all') }}" type="button" class="btn btn-sm btn-secondary">
                        <i class="fa fa-arrow-left"></i> All Pages
                    </a>
                </div>
            </div>
            <div class="block-content">
                <form action="{{ route('admin-manage-site-page-submit') }}" method="POST" enctype="multipart/form-data" id="page-add-form">
                    <div class="row">
                        <div class="col-sm-12 col-md-8 col-lg-8">
                            @csrf

                            @foreach ($data_array as $key => $value)
                                <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                            @endforeach

                            <input type="hidden" name="id" value="{{ ($item) ? $item->id : null }}">

                            <div class="form-group row">
                                <div class="col-3">
                                    <label for="page_type">Page Type</label>
                                    <select name="page_type" id="page_type" class="form-control" required>
                                        <option value="" selected disabled>Select Service Type</option>
                                        <option value="stand-alone" {{ ($item && $item->page_type == 'stand-alone')? 'selected' : '' }}>Site Page</option>
                                        <option value="ph-registrations" {{ ($item && $item->page_type == 'ph-registrations')? 'selected' : '' }}>Ph Registrations</option>
                                        <option value="bookkeeping-accounting" {{ ($item && $item->page_type == 'bookkeeping-accounting')? 'selected' : '' }}>Bookkeeping & Accounting</option>
                                    </select>
                                </div>
                                <div class="col-5">
                                    <div class="form-material">
                                        <input type="text" class="form-control" id="p_title" name="title" placeholder="" value="{{ ($item && $item->title)? $item->title : old('title') }}">
                                        <label for="p_title">Page Title</label>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-material">
                                        <input type="text" class="form-control" id="p_slug" name="slug" placeholder="" value="{{ ($item && $item->slug)? $item->slug : old('slug') }}">
                                        <label for="p_slug">Slug</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-3">
                                    <label for="heading_position">Heading Position</label>
                                    <select name="heading_position" id="heading_position" class="form-control" required>
                                        <option value="left" {{ ($item && $item->heading_position == 'left')? 'selected' : '' }}>Left</option>
                                        <option value="right" {{ ($item && $item->heading_position == 'right')? 'selected' : '' }}>Right</option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <label for="color-picker">Header Color</label>
                                    <input type="text" class="form-control" id="color-picker" name="bg_color" placeholder="" value="{{ ($item && $item->bg_color)? $item->bg_color : '#174579' }}">
                                </div>
                                <div class="col-2">
                                    <p></p>
                                    <label class="css-control css-control-info css-switch mt-5">
                                        <input type="checkbox" class="css-control-input" name="show" true-value="1" false-value="0" {{ ($item && $item->show == 1)? 'checked' : '' }}>
                                        <span class="css-control-indicator"></span> Show
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 col-md-6">
                                    <label for="banner_image">Banner Image</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="banner_image" name="banner_image_file">
                                        <label class="custom-file-label" for="banner_image">Choose file</label>
                                    </div>
                                    @if ($item && $item->banner_image)
                                        <div>
                                            <img src="{{ $item->banner_image }}" alt="Banner Image" class="w-50 mt-2">
                                        </div>
                                    @endif
                                </div>
                                <div class="col-12 col-md-6">
                                    <label for="banner_image_mobile">Banner Image for Mobile</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="banner_image_mobile" name="banner_image_mobile_file">
                                        <label class="custom-file-label" for="banner_image_mobile">Choose file</label>
                                    </div>
                                    @if ($item && $item->banner_image_mobile)
                                        <div>
                                            <img src="{{ $item->banner_image_mobile }}" alt="Banner Image Mobile" class="w-50 mt-2">
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 for-banner">
                                    <label for="sub_title">Details</label>
                                    <textarea class="form-control  init-summernote" id="sub_title" name="sub_title" rows="8">{{ ($item && $item->sub_title)? $item->sub_title : old('sub_title') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-3">
                            {{-- SEO FORM --}}
                            @include('dashboard.includes.seo_form')
                        </div>
                    </div>
                    
                    <div class="mt-4 mb-3 text-center">
                        <button type="submit" class="btn btn-alt-info"><i class="fa fa-floppy-o"></i> {{ ($item && $item->id != null)? 'Update' : 'Save & Continue' }}</button>
                        <a href="{{ route('admin-manage-site-pages-all') }}" type="button" class="btn btn-secondary ml-2"><i class="fa fa-times"></i> Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js_after')
    <script>
        $('#color-picker').spectrum({
            type: "component"
        });

        $('#page-add-form input[name="title"]').keyup(function() {
            $('#page-add-form input[name="slug"]').val(convertToSlug(this.value))
        });
    </script>
@endsection