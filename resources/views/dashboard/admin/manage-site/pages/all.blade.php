@extends('layouts.backend')

@section('title')
    All Pages - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-cogs"></i> All Pages</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Page List</h3>
                <div class="block-options">
                    <a href="{{ route('admin-manage-site-page-add') }}" type="button" class="btn btn-sm btn-success">
                        <i class="fa fa-plus"></i> Add Page
                    </a>
                </div>
            </div>
            <div class="block-content">
                
                @if ($items)
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 3%;">#</th>
                                <th style="width: 5%;">Color</th>
                                <th style="width: 10%;">Header Image</th>
                                <th>Title</th>
                                <th style="width: 15%;">Type</th>
                                <th class="text-center"  style="width: 10%;">Show to Live</th>
                                <th class="text-center" style="width: 15%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td>
                                        <div class="m-2" style="background-color: {{ $item->bg_color }}; height: 20px; border-radius: 3px;"></div>
                                    </td>
                                    <td>
                                        @if ($item->banner_image)
                                            <img src="{{ $item->banner_image }}" alt="{{ $item->title }}" class="w-100">
                                        @endif
                                    </td>
                                    <td>
                                        <b data-toggle="popover" title="{{ $item->title }}" data-placement="top" data-content="{{ strip_tags($item->sub_title) }}">
                                            {{ $item->title }}
                                        </b>
                                    </td>
                                    <td>{{ $item->page_type }}</td>
                                    <td class="text-center">
                                        <label class="css-control css-control-sm css-control-info css-switch" for="value_{{ $item->id }}">
                                            <input type="checkbox" class="css-control-input" id="value_{{ $item->id }}" name="show_{{ $item->id }}" data-id="{{ $item->id }}" true-value="1" false-value="0" {{ ($item && $item->show == 1)? 'checked' : '' }}>
                                            <span class="css-control-indicator"></span>
                                        </label>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-sm btn-alt-warning" onclick="duplicatePage({{ $item }})" data-toggle="popover" data-placement="top" title="Duplicate Page">
                                            <i class="si si-docs"></i>
                                        </button>
                                        <a target="_blank" href="{{ route('pb-preview-page', ['slug' => $item->slug]) }}" type="button" class="btn btn-sm btn-alt-success" data-toggle="popover" data-placement="top" title="Preview Page">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('admin-manage-site-page-edit', ['slug' => $item->slug]) }}" class="btn btn-sm btn-alt-info" data-toggle="popover" data-placement="top" title="Manage Page Heading">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a href="{{ route('admin-manage-site-page-builder', ['type' => $item->page_type, 'slug' => $item->slug]) }}" type="button" class="btn btn-sm btn-alt-secondary" data-toggle="popover" data-placement="top" title="Go to Page Builder">
                                            <i class="fa fa-clipboard"></i>
                                        </a>
                                        <button class="btn btn-sm btn-alt-danger" onclick="removePage({{ $item->id }})" data-toggle="popover" data-placement="top" title="Remove Page">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif

            </div>
        </div>
        {{-- Duplicate Page Modal --}}
        <div class="modal fade" id="duplicate-page-modal-top" tabindex="-1" role="dialog" aria-labelledby="duplicate-page-modal-top" aria-hidden="true">
            <div class="modal-dialog modal-dialog-top" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-warning-light">
                            <h3 class="block-title text-black">Duplicate Page</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <form action="{{ route('pb-duplicate-page') }}" method="POST" id="duplicate-page-form">
                                @csrf
                                <input type="hidden" name="old_page_id" value="">
                                <input type="hidden" name="old_page_slug" value="">
                                <input type="hidden" name="model" value="SitePage">

                                <div class="form-group row">
                                    <div class="col-12">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="new_title" name="new_page_title" required>
                                            <label for="new_title">New Page Title</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="new_slug" name="new_page_slug" required>
                                            <label for="new_slug">New Page Slug</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mt-3 text-center">
                                    <button type="submit" class="btn btn-alt-info" id="submitPageSection">
                                        <i class="fa fa-floppy-o"></i> Duplicate Now
                                    </button>
                                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">
                                        <i class="fa fa-times"></i> Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    {{-- <div class="modal-footer">
                        <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                            <i class="fa fa-check"></i> Perfect
                        </button>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js_after')
    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>
    <script>
        function duplicatePage(page) {
            $('#duplicate-page-form input[name="old_page_id"]').val(page.id);
            $('#duplicate-page-form input[name="old_page_slug"]').val(page.slug);
            $('#duplicate-page-modal-top .block-title').html('Duplicate Page(' + page.title + ')');
            $('#duplicate-page-modal-top').modal('toggle');
        }

        function removePage(pageId) {
            swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover action!",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, delete it!",
                html: !1,
            })
            .then((result) => {
                if(result.value) {
                    $.ajax({
                        url: "{{ route('pb-remove-data') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: pageId,
                            model: 'SitePage',
                        }
                    }).done(function() {
                        swal.fire({
                            title: "Success!",
                            text: "Deleted successfully.",
                            icon: "success",
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            html: !1,
                            timer: 1000
                        })
                        .then(() => {
                            window.location.reload(true)
                        })
                    });
                }
            });
        }

        $('input:checkbox').change(function(){
            var itemId = $(this).data('id')
            var value = (this.checked)? 1 : 0

            $.ajax({
                url: "{{ route('update-switch') }}",
                method: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: itemId,
                    model: 'SitePage',
                    type: 'page',
                    show: value
                }
            }).done(function() {
                swal.fire({
                    title: "Success!",
                    text: "Updated successfully.",
                    icon: "success",
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    html: !1,
                    timer: 1000
                })
                .then(() => {
                    window.location.reload(true)
                })
            });
        });

        $('#duplicate-page-form input[name="new_page_title"]').keyup(function() {
            $('#duplicate-page-form input[name="new_page_slug"]').val(convertToSlug(this.value))
        });

        function convertToSlug(title)
        {
            return title
                .toLowerCase()
                .replace(/[^\w ]+/g,'')
                .replace(/ +/g,'-')
                ;
        }
    </script>
@endsection