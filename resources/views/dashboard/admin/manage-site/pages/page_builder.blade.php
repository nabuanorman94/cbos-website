@extends('layouts.backend')

@section('title')
    Page Builder - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="si si-book-open"></i> Pages > Page Builder</h2>

        <div class="block block-transparent">
            <div class="block-header block-header-default">
                <h3 class="block-title">{{ $page->title }} <small>(<i>https://cbos.com.ph/{{ $page->slug }}</i>)</small></h3>
                <div class="block-options">
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add-page-section-modal"><i class="fa fa-plus"></i> Add Section</button>
                    <a target="_blank" href="{{ route('pb-preview-page', ['slug' => $page->slug]) }}" type="button" class="btn btn-sm btn-alt-success">
                        <i class="fa fa-eye"></i> Preview
                    </a>
                    <a href="{{ route('admin-manage-site-page-edit', ['slug' => $page->slug]) }}" type="button" class="btn btn-sm btn-alt-info">
                        <i class="fa fa-pencil"></i> Manage Page Heading
                    </a>
                    <a href="{{ route('admin-manage-site-pages-all') }}" type="button" class="btn btn-sm btn-alt-secondary">
                        <i class="fa fa-arrow-left"></i> All Pages
                    </a>
                </div>
            </div>
            <div class="block-content">
                <h6>Page Sections <small><i><b>(Sections right after the page banner)</b></i></small></h6>
            </div>
        </div>

        @if ($page_sections)
            @foreach ($page_sections as $item)
            <div class="row js-draggable-items">
                <div class="col-12 draggable-column ui-sortable">
                    <div class="block draggable-item">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Section {{ $loop->iteration }}</h3>
                            <div class="block-options">
                                <button class="btn btn-sm btn-alt-warning" onclick="duplicateSection({{ $item->id }}, 'Duplicate Section?', 'This will duplicate all components found under this section to new one.')"><i class="si si-docs"></i> Duplicate Section</button>
                                <button class="btn btn-sm btn-alt-info" onclick="editData({{ $item }})"><i class="fa fa-pencil"></i> Edit Section</button>
                                <button class="btn btn-sm btn-alt-danger" onclick="removeData('PageSection', {{ $item->id }}, 'Remove Section?', 'Removing page section will remove its child components.')"><i class="fa fa-trash"></i> Delete Section</button>
                                <a class="btn-block-option draggable-handler ui-sortable-handle" href="javascript:void(0)">
                                    <i class="si si-cursor-move"></i>
                                </a>
                            </div>
                        </div>
                        <div class="block-content pb-4">
                            <section class="inner-page {{ $item->section_style }}" style="{{ $item->div_styling }} background-image: url({{ $page->div_image_background }});">
                                <div class="{{ $item->div_class }}" id="{{ $item->div_id }}">
                                    @if ($item->contents)
                                        @foreach ($item->contents as $content)
                                            <div class="{{ $content->div_class }}" id="{{ $content->div_id }}" style="{{ $content->div_styling }}">
                                                <div class="html-content">
                                                    {!! $content->value !!} 
                                                </div>
                                                <div class="text-center">
                                                    <a href="javascript:void(0)" class="ml-3" onclick="duplicateComponent('PageContent', {{ $content->id }}, 'Duplicate Content?', 'This will duplicate the selected content under the same section.')" data-toggle="popover" data-placement="top" title="Duplicate Content">
                                                        <i class="si si-docs"></i>
                                                    </a>
                                                    <a href="{{ route('pb-content', ['section_id' => $item->id, 'content_id' => $content->id]) }}" class="ml-1" data-toggle="popover" data-placement="top" title="Edit Content">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a href="javascript:void(0)" onclick="removeData('PageContent', {{ $content->id }}, 'Remove Page Content?', 'You cannot undo this process.')" class="ml-1" data-toggle="popover" data-placement="top" title="Remove Content">
                                                        <i class="fa fa-trash"></i>
                                                    </a> 
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                    @if ($item->feature_blocks)
                                        @foreach ($item->feature_blocks as $feature_block)
                                            <div class="{{ $feature_block->div_class }}" id="{{ $feature_block->div_id }}" style="{{ $feature_block->div_styling }}">
                                                @if ($feature_block->feat_image)
                                                    <div class="mb-2">
                                                        <img src="{{ $feature_block->feat_image }}" alt="{{ $feature_block->feat_title }} Feature Image" class="w-100">
                                                    </div>
                                                @endif
                                                {{-- <h4 class="mb-2">{{ $feature_block->feat_title }}</h4> --}}
                                                <div class="html-content">
                                                    {!! $feature_block->feat_content !!} 
                                                </div>
                                                <div class="text-center">
                                                    <a href="javascript:void(0)" class="ml-3" onclick="duplicateComponent('PageFeatureBlock', {{ $feature_block->id }}, 'Duplicate Feature Block?', 'This will duplicate the feature block under the same section.')" data-toggle="popover" data-placement="top" title="Duplicate Feature Block">
                                                        <i class="si si-docs"></i>
                                                    </a>
                                                    <a href="{{ route('pb-feature-block', ['section_id' => $item->id, 'feature_block_id' => $feature_block->id]) }}" class="ml-1" data-toggle="popover" data-placement="top" title="Edit Feature Block">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a href="javascript:void(0)" onclick="removeData('PageFeatureBlock', {{ $feature_block->id }}, 'Remove Page Feature Block?', 'You cannot undo this process.')" class="ml-1" data-toggle="popover" data-placement="top" title="Remove Feature Block">
                                                        <i class="fa fa-trash"></i>
                                                    </a> 
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                    @if ($item->feature_sections)
                                        @foreach ($item->feature_sections as $feature_section)
                                            <div class="{{ $feature_section->div_class }}" id="{{ $feature_section->div_id }}" style="{{ $feature_section->div_styling }}">
                                                @if ($feature_section->image_placement == 'left')
                                                    <div class="row mb-3 d-flex align-items-center">
                                                        <div class="col-sm-12 col-lg-6 order-1 order-lg-1 hero-img">
                                                            <img src="{{ $feature_section->feat_image }}" class="img-fluid" alt="{{ $feature_section->feat_title }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6 pt-5 pt-lg-0 order-2 order-lg-2 align-items-center">
                                                            <div class="html-content">{!! $feature_section->feat_content !!}</div>
                                                            <div class="text-center">
                                                                <a href="javascript:void(0)" class="ml-3" onclick="duplicateComponent('PageFeatureSection', {{ $feature_section->id }}, 'Duplicate Feature Section?', 'This will duplicate the feature section under the same section.')" data-toggle="popover" data-placement="top" title="Duplicate Feature Section">
                                                                    <i class="si si-docs"></i>
                                                                </a>
                                                                <a href="{{ route('pb-feature-section', ['section_id' => $item->id, 'feature_section_id' => $feature_section->id]) }}" class="ml-1" data-toggle="popover" data-placement="top" title="Edit Feature Section">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>
                                                                <a href="javascript:void(0)" onclick="removeData('PageFeatureSection', {{ $feature_section->id }}, 'Remove Page Feature Section?', 'You cannot undo this process.')" class="ml-1" data-toggle="popover" data-placement="top" title="Remove Feature Section">
                                                                    <i class="fa fa-trash"></i>
                                                                </a> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="row mb-3 d-flex align-items-center">
                                                        <div class="col-sm-12 col-lg-6 order-1 order-lg-2 hero-img">
                                                            <img src="{{ $feature_section->feat_image }}" class="img-fluid" alt="{{ $feature_section->feat_title }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6 pt-5 pt-lg-0 order-1 order-lg-1 align-items-center">
                                                            <div class="html-content">{!! $feature_section->feat_content !!}</div>
                                                            <div class="text-center">
                                                                <a href="javascript:void(0)" class="ml-3" onclick="duplicateComponent('PageFeatureSection', {{ $feature_section->id }}, 'Duplicate Feature Section?', 'This will duplicate the feature section under the same section.')" data-toggle="popover" data-placement="top" title="Duplicate Feature Section">
                                                                    <i class="si si-docs"></i>
                                                                </a>
                                                                <a href="{{ route('pb-feature-section', ['section_id' => $item->id, 'feature_section_id' => $feature_section->id]) }}" class="ml-1" data-toggle="popover" data-placement="top" title="Edit Feature Section">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>
                                                                <a href="javascript:void(0)" onclick="removeData('PageFeatureSection', {{ $feature_section->id }}, 'Remove Page Feature Section?', 'You cannot undo this process.')" class="ml-1" data-toggle="popover" data-placement="top" title="Remove Feature Section">
                                                                    <i class="fa fa-trash"></i>
                                                                </a> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        @endif
        <hr>
        {{-- This is the FAQ CRUD section accepts faq_type --}}
        @php
            $faq_type = $page->slug;
        @endphp
        @include('dashboard.admin.common.faqs_section')
        {{-- End FAQ CRUD section --}}
    </div>

    {{-- Page Section Modal --}}
    <div class="modal fade" id="add-page-section-modal" tabindex="-1" role="dialog" aria-labelledby="add-page-section-modal" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-popout" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent">
                    <div class="block-header bg-primary">
                        <h3 class="block-title">Add Section</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <form action="{{ route('pb-create-data') }}" method="POST" enctype="multipart/form-data" id="page-section-form">
                            @csrf
                            <input type="hidden" name="id" value="">
                            <input type="hidden" name="page_id" value="{{ $page->id }}">
                            <input type="hidden" name="arrangement" value="{{ count($page_sections) + 1 }}">
                            <input type="hidden" name="model" value="PageSection">

                            <div class="block">
                                <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#btabs-basic">Basic</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#btabs-advance">Advanced</a>
                                    </li>
                                </ul>
                                <div class="block-content tab-content">
                                    <div class="tab-pane active" id="btabs-basic" role="tabpanel">
                                        
                                    </div>
                                    <div class="tab-pane" id="btabs-advance" role="tabpanel">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="ps_section_style">Section Style</label>
                                                <select name="section_style" id="ps_section_style" class="form-control" required>
                                                    @foreach ($page_types as $pt)
                                                        <option value="{{ $pt->class }}">{{ $pt->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="ps_div_image_background">Background Image</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="ps_div_image_background" name="ps_div_image_background_file">
                                                    <label class="custom-file-label" for="ps_div_image_background">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-6">
                                                <div class="form-material">
                                                    <input type="text" class="form-control" id="ps_div_class" name="div_class">
                                                    <label for="ps_div_class">Class Name</label>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-material">
                                                    <input type="text" class="form-control" id="ps_div_id" name="div_id">
                                                    <label for="ps_div_id">ID Name</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="ps_div_styling">Styling</label>
                                                <textarea class="form-control" id="ps_div_styling" name="div_styling" rows="8"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="section_type" id="secttionTypeLabel">Component</label>
                                            <select name="section_type" id="section_type" class="form-control" required>
                                                <option value="" selected disabled>Select Component Type</option>
                                                @foreach ($page_section_types as $pst)
                                                    <option value="{{ $pst['model'] }}" data-description="{{ $pst['description'] }}">{{ $pst['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-12">
                                            <div id="desc" class="mt-2"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <hr>
                            <div class="form-group mt-3 text-center">
                                <button type="submit" class="btn btn-alt-info" id="submitPageSection">
                                    <i class="fa fa-floppy-o"></i> Add & Continue
                                </button>
                                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">
                                    <i class="fa fa-times"></i> Cancel
                                </button>
                            </div>
    
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <form action="{{ route('pb-duplicate-section') }}" method="POST" id="duplicate-section-form">
            @csrf
            <input type="hidden" name="page_section_id" value="">
        </form>
        <form action="{{ route('pb-duplicate-component') }}" method="POST" id="duplicate-component-form">
            @csrf
            <input type="hidden" name="component_id" value="">
            <input type="hidden" name="model" value="">
        </form>
    </div>
@endsection

@section('js_after')
    <script>
        $('#color-picker').spectrum({
            type: "component"
        });

        $('select[name="section_type"]').on('change', function() {
            $('#desc').html($(this).find(':selected').data('description'))
        });

        function duplicateSection(id, title, message) {
            swal.fire({
                title: title,
                text: message,
                icon: "question",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, duplicate it!",
                html: !1,
            })
            .then((result) => {
                $('#duplicate-section-form input[name="page_section_id"]').val(id)
                $('#duplicate-section-form').submit()
            });
        }

        function duplicateComponent(model, id, title, message) {
            console.log(model)
            swal.fire({
                title: title,
                text: message,
                icon: "question",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, duplicate it!",
                html: !1,
            })
            .then((result) => {
                if(result.value) {
                    $('#duplicate-component-form input[name="component_id"]').val(id)
                    $('#duplicate-component-form input[name="model"]').val(model)
                    $('#duplicate-component-form').submit()
                }
            });
        }

        function editData(item) {
            $('#page-section-form').attr('action', '{{ route('pb-update-data') }}');
            $('input[name="id"]').val(item.id);
            $('input[name="div_class"]').val(item.div_class);
            $('input[name="div_id"]').val(item.div_id);
            $('input[name="arrangement"]').val(item.arrangement);
            $('select[name="section_style"]').val(item.section_style);
            $('select[name="section_type"]').removeAttr('required');
            $('#ps_div_styling').val(item.div_styling);
            $('#submitPageSection').html('<i class="fa fa-floppy-o"></i> Save & Continue');
            $('#secttionTypeLabel').html('Add New Component');

            $('#add-page-section-modal').modal('show');
        }

        function removeData(model, id, title, message) {
            swal.fire({
                title: title,
                text: message,
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, delete it!",
                html: !1,
            })
            .then((result) => {
                if(result.value) {
                    $.ajax({
                        url: "{{ route('pb-remove-data') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: id,
                            model: model,
                        }
                    }).done(function() {
                        swal.fire({
                            title: "Success!",
                            text: "Deleted successfully.",
                            icon: "success",
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            html: !1,
                            timer: 1000
                        })
                        .then(() => {
                            window.location.reload(true)
                        })
                    });
                }
            })
        }
    </script>
@endsection