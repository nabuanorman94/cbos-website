@extends('layouts.backend')

@section('title')
    Pricing - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="fa fa-cogs"></i> Pricing</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Pricing List</h3>
                <div class="block-options">
                    <a href="{{ route('admin-manage-site-pricing-add') }}" type="button" class="btn btn-sm btn-success">
                        <i class="fa fa-plus"></i> Add Pricing
                    </a>
                </div>
            </div>
            <div class="block-content">
                
                @if ($items)
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 3%;">#</th>
                                <th>Package Name</th>
                                <th style="width: 10%;">Type</th>
                                <th class="text-center"  style="width: 10%;">Show to Live</th>
                                <th class="text-center" style="width: 15%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td>{{ $item->package_name }}</td>
                                    <td>{{ $item->pricing_type }}</td>
                                    <td class="text-center">
                                        <label class="css-control css-control-sm css-control-info css-switch" for="value_{{ $item->id }}">
                                            <input type="checkbox" class="css-control-input" id="value_{{ $item->id }}" name="show_{{ $item->id }}" data-id="{{ $item->id }}" true-value="1" false-value="0" {{ ($item && $item->show == 1)? 'checked' : '' }}>
                                            <span class="css-control-indicator"></span>
                                        </label>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('admin-manage-site-pricing-edit', ['id' => $item->id]) }}" class="btn btn-sm btn-alt-info"><i class="fa fa-pencil"></i> Edit</a>
                                        <button class="btn btn-sm btn-alt-secondary" onclick="removePage({{ $item->id }})"><i class="fa fa-trash"></i> Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif

            </div>
        </div>
    </div>
@endsection

@section('js_after')
    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>
    <script>
        function removePage(pageId) {
            swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover action!",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, delete it!",
                html: !1,
            })
            .then((result) => {
                if(result.value) {
                    $.ajax({
                        url: "{{ route('remove-data') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: pageId,
                            model: 'SitePricing',
                            type: 'pricing',
                        }
                    }).done(function() {
                        swal.fire({
                            title: "Success!",
                            text: "Deleted successfully.",
                            icon: "success",
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            html: !1,
                            timer: 1000
                        })
                        .then(() => {
                            window.location.reload(true)
                        })
                    });
                }
            })
        }

        $('input:checkbox').change(function(){
            var itemId = $(this).data('id')
            var value = (this.checked)? 1 : 0

            $.ajax({
                url: "{{ route('update-switch') }}",
                method: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: itemId,
                    model: 'SitePricing',
                    type: 'pricing',
                    show: value
                }
            }).done(function() {
                swal.fire({
                    title: "Success!",
                    text: "Updated successfully.",
                    icon: "success",
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    html: !1,
                    timer: 1000
                })
                // .then(() => {
                //     window.location.reload(true)
                // })
            });
        });
    </script>
@endsection