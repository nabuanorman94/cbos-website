@extends('layouts.backend')

@section('title')
    My Team - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="si si-star"></i> My Team</h2>
    </div>
@endsection