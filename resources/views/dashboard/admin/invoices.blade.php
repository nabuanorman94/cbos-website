@extends('layouts.backend')

@section('title')
    Invoices - {{ (auth()->user())? auth()->user()->getRoleNameAttribute() : '' }}
@endsection

@section('content')
    <div id="app" class="content pt-0">
        <h2 class="content-heading"><i class="si si-note"></i> Invoices</h2>
    </div>
@endsection