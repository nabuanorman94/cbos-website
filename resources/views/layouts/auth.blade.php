@php
    $details = App\SiteDetails::first();
@endphp

<!doctype html>
<html lang="{{ config('app.locale') }}" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <meta name="robots" content="noindex, nofollow">

        <title>@yield('title') {{ ($details && $details->name)? $details->name : '' }}</title>

        <link rel="shortcut icon" href="{{ asset('media/favicons/favicon.ico') }}">
        <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

        <link rel="dns-prefetch" href="//fonts.gstatic.com">

        @yield('css_before')
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" id="css-main" href="{{ mix('/css/codebase.css') }}">

        {{-- <link rel="stylesheet" id="css-theme" href="{{ mix('/css/themes/pulse.css') }}"> --}}
        @yield('css_after')
        
    </head>
    <body>
        <div id="page-container" class="main-content-boxed">
            <main>
                @yield('content')
            </main>
        </div>
        <script src="{{ mix('js/codebase.app.js') }}"></script>
        <script src="{{ mix('js/laravel.app.js') }}"></script>
        
        @yield('js_after')

        {{-- @include('includes/freshchat') --}}
    </body>
</html>
