@php
    $details = App\SiteDetails::first();
    $services = App\SitePage::where([['show', '=', 1], ['page_type', '!=', 'stand-alone']])->orderBy('id', 'asc')->get();
    $site_content = App\SiteContent::first();
    $pages = App\SitePage::where([['show', '=', 1], ['page_type', '=', 'stand-alone']])->orderBy('id', 'asc')->get();
@endphp

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title') {{ ($details)? $details->name : '' }}</title>
    {{-- <meta name="robots" content="index, follow" /> --}}
    <meta name="robots" content="noindex, nofollow">

    {{-- basic seo --}}
    @yield('seo')

    {{-- facebook og --}}
    @yield('facebook_og')

    <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/icofont/icofont.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('vendor/venobox/venobox.css') }}" rel="stylesheet"> --}}
    {{-- <link href="{{ asset('vendor/aos/aos.css') }}" rel="stylesheet"> --}}

    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('css/style.css') }}" rel="stylesheet"> --}}

    @yield('css_after')
</head>
<body class="site">
    <header id="header" class="fixed-top d-flex align-items-center navigation-default">
        <div class="container d-flex align-items-center">
    
            <div class="logo mr-auto">
                {{-- <h1 class="text-light"><a href="/"><span>CBOS</span></a></h1> --}}
                <a href={{ route('homepage') }}><img src="{{ ($details)? $details->logo : '' }}" alt="" class="img-fluid"></a>
            </div>
            
            <nav class="nav-menu d-none d-lg-block">
                <ul>
                {{-- <li><a href="{{ route('homepage') }}#about">About Us</a></li>
                <li><a href="{{ route('homepage') }}#pricing">Pricing</a></li> --}}

                @if ($services && count($services) > 0)
                        <li class="drop-down"><a href="" onclick="return false">Ph Registrations</a>
                            <ul>
                                @foreach ($services as $item)
                                    @if ($item->page_type == 'ph-registrations')
                                        <li><a href="{{ route('view-service-page', $item->slug) }}">{{ $item->title }}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                        <li class="drop-down"><a href="" onclick="return false">Bookkeeping & Accounting</a>
                            <ul>
                                @foreach ($services as $item)
                                    @if ($item->page_type == 'bookkeeping-accounting')
                                        <li><a href="{{ route('view-service-page', $item->slug) }}">{{ $item->title }}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                @endif
                
                <li class="drop-down"><a href="" onclick="return false">Resources</a>
                    <ul>
                    <li><a href="#">News & Blogs</a></li>
                    <li><a href="#">Articles & Videos</a></li>
                    <li><a href="#">Question of the Day</a></li>
                    </ul>
                </li>
                <li><a href="{{ route('homepage') }}#contact">Contact Us</a></li>
        
        
                <!-- <li><a href="#portfolio">Portfolio</a></li>
                <li><a href="#team">Team</a></li> -->
                
                @guest
                    {{-- <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif --}}
                    {{-- <li class="get-started"><a href="{{ route('login') }}">Sign In</a></li> --}}
                    <li class="get-started"><a href="{{ route('get-started') }}">Get Started</a></li>
                @else
                    <li class="drop-down">
                        <a id="navbarDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->username }}
                        </a>

                        <ul>
                            <li>
                                @switch(auth()->user()->getRoleNameAttribute())
                                    @case('Admin')
                                        <a href="{{ route('admin-home') }}">Admin Dashboard</a>
                                        @break
                                    @case('User')
                                        <a href="{{ route('home') }}">Dashboard</a>
                                        @break
                                    @default
                                @endswitch
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>
                @endguest

                </ul>
            </nav><!-- .nav-menu -->
    
        </div>
    </header>

    <div id="app">
        <section class="breadcrumbs">
            <div class="container">
        
              <div class="d-flex justify-content-between align-items-center">
                <h5 class="mb-0">{{ $breadcrumbs_title }}</h5>
                <ol>
                    @foreach ($breadcrumbs_links as $item)
                          <li><a href="{{ route($item['route']) }}">{{ $item['name'] }}</a></li>
                    @endforeach
                </ol>
              </div>
        
            </div>
        </section>
        
        <section class="inner-page" style="height: 80vh;">
            <div class="container">
              <main class="pb-4">
                @yield('content')
              </main>
            </div>
        </section>
    </div>

    <footer id="footer">
      <div class="container">
        <div class="row d-flex align-items-center">
          <div class="col-lg-6 text-lg-left text-center">
            <div class="copyright">
              &copy; Copyright 2021 <strong>{{ ($details)? $details->name : '' }}</strong>. All Rights Reserved
            </div>
            
          </div>
          <div class="col-lg-6">
            <nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">
              {{-- <a href="/" class="scrollto">Home</a> --}}
              @if ($pages)
                @foreach ($pages as $page)
                <a href="{{ URL::to($page->slug) }}" class="scrollto">{{ $page->title }}</a>
                @endforeach
              @endif
            </nav>
          </div>
        </div>
      </div>
    </footer>

    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    {{-- <script src="{{ asset('vendor/php-email-form/validate.js') }}"></script> --}}
    <script src="{{ asset('vendor/waypoints/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('vendor/counterup/counterup.min.js') }}"></script>
    <script src="{{ asset('vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    {{-- <script src="{{ asset('vendor/venobox/venobox.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('vendor/aos/aos.js') }}"></script> --}}

    <!-- Scripts -->
    <script src="{{ asset('js/site-default.js') }}" defer></script>
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    @yield('js_after')

    @include('includes/freshchat')
</body>
</html>