@php
    $details = App\SiteDetails::first();
@endphp

<!doctype html>
<html lang="{{ config('app.locale') }}" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>@yield('title') Dashboard</title>

        <meta name="description" content="CBOS Dashboard">
        <meta name="author" content="Norman">
        <meta name="robots" content="noindex, nofollow">

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">

        <!-- Icons -->
        <link rel="shortcut icon" href="{{ asset('media/favicons/favicon.ico') }}">
        <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

        {{-- Plugins --}}
        <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
        <link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
        <link rel="stylesheet" href="{{ asset('js/plugins/spectrum/src/spectrum.css') }}">
        <link rel="stylesheet" href="{{ asset('js/plugins/summernote/summernote-bs4.css') }}">
        <link rel="stylesheet" href="{{ asset('js/plugins/fullcalendar/fullcalendar.min.css') }}">
        
        <link rel="stylesheet" href="{{ asset('js/plugins/flatpickr/flatpickr.min.css') }}">

        <!-- Fonts and Styles -->
        @yield('css_before')
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="{{ mix('/css/codebase.css') }}">

        <!-- You can include a specific file from public/css/themes/ folder to alter the default color theme of the template. eg: -->
        {{-- <link rel="stylesheet" id="css-theme" href="{{ mix('/css/themes/pulse.css') }}"> --}}
        @yield('css_after')

        <!-- Scripts -->
        {{-- <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script> --}}
    </head>
    <body>
        <!-- Page Container -->
        <div id="page-container" class="sidebar-o sidebar-inverse enable-page-overlay side-scroll main-content-narrow side-trans-enabled page-header-fixed">
        {{-- <div id="page-container" class="sidebar-o sidebar enable-page-overlay side-scroll main-content-narrow side-trans-enabled page-header-fixed"> --}}
            <!-- Sidebar -->
            <nav id="sidebar">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Side Header -->
                    <div class="content-header content-header-fullrow px-15">
                        <!-- Mini Mode -->
                        <div class="content-header-section sidebar-mini-visible-b">
                            <!-- Logo -->
                            <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                                <span class="text-dual-primary-dark">CB</span><span class="text-primary">OS</span>
                            </span>
                            <!-- END Logo -->
                        </div>
                        <!-- END Mini Mode -->

                        <!-- Normal Mode -->
                        <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                            <!-- Close Sidebar, Visible only on mobile screens -->
                            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                            <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times"></i>
                            </button>
                            <!-- END Close Sidebar -->

                            <!-- Logo -->
                            <div class="content-header-item">
                                @if (auth()->user() && $details != null)
                                    @switch(auth()->user()->getRoleNameAttribute())
                                        @case('Super Admin')
                                            <a class="font-w700" href="{{ route('admin-home') }}">
                                                <img src="{{ $details->logo_2 }}" alt="CBOS" width="120">
                                            </a>
                                            @break
                                        @case('Admin')
                                            <a class="font-w700" href="{{ route('admin-home') }}">
                                                <img src="{{ $details->logo_2 }}" alt="CBOS" width="120">
                                            </a>
                                            @break
                                        @case('User')
                                            <a class="font-w700" href="{{ route('home') }}">
                                                <img src="{{ $details->logo_2 }}" alt="CBOS" width="120">
                                            </a>
                                            @break
                                        @default
                                            
                                    @endswitch
                                @endif
                            </div>
                            <!-- END Logo -->
                        </div>
                        <!-- END Normal Mode -->
                    </div>
                    <!-- END Side Header -->

                    <!-- Side User -->
                    {{-- <div class="content-side content-side-full content-side-user px-10 align-parent">
                        <!-- Visible only in mini mode -->
                        <div class="sidebar-mini-visible-b align-v animated fadeIn">
                            <img class="img-avatar img-avatar32" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
                        </div>
                        <!-- END Visible only in mini mode -->

                        <!-- Visible only in normal mode -->
                        <div class="sidebar-mini-hidden-b text-center">
                            <a class="img-link" href="javascript:void(0)">
                                <img class="img-avatar" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
                            </a>
                            <ul class="list-inline mt-10">
                                <li class="list-inline-item">
                                    <a class="link-effect text-dual-primary-dark font-size-sm font-w600 text-uppercase" href="javascript:void(0)">{{ Auth::user()->username }}</a>
                                </li>
                                <li class="list-inline-item">
                                    <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                    <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
                                        <i class="si si-drop"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="link-effect text-dual-primary-dark">
                                        <i class="si si-logout mr-5"></i>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                        <!-- END Visible only in normal mode -->
                    </div> --}}
                    <!-- END Side User -->

                    <!-- Side Navigation -->
                    <div class="content-side content-side-full">
                        @include('dashboard.sidemenu')
                    </div>
                    <!-- END Side Navigation -->
                </div>
                <!-- Sidebar Content -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="page-header">
                <!-- Header Content -->
                <div class="content-header">
                    <!-- Left Section -->
                    <div class="content-header-section">
                        <!-- Toggle Sidebar -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="sidebar_toggle">
                            <i class="fa fa-navicon "></i>
                        </button>
                        <!-- END Toggle Sidebar -->
                    </div>
                    <!-- END Left Section -->

                    <!-- Right Section -->
                    <div class="content-header-section">
                        <!-- User Dropdown -->
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-user d-sm-none"></i>
                                <span class="d-none d-sm-inline-block ">{{ auth()->user()->username }}</span>
                                <i class="fa fa-angle-down ml-5 "></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right min-width-200" aria-labelledby="page-header-user-dropdown">
                                <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">{{ request()->is('admin') || request()->is('admin/*') ? ' ADMIN' : 'USER' }}</h5>
                                <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="si si-user mr-5"></i> Profile
                                </a>
                                {{-- <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="si si-note mr-5"></i> Invoices
                                </a> --}}
                                <div class="dropdown-divider"></div>

                                <!-- Toggle Side Overlay -->
                                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="si si-wrench mr-5"></i> Settings
                                </a>
                                <a class="dropdown-item" href="{{ route('homepage') }}" target="_blank">
                                    <i class="si si-globe mr-5"></i> CBOS Website
                                </a>
                                <!-- END Side Overlay -->

                                <div class="dropdown-divider"></div>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item">
                                    <i class="si si-logout mr-5"></i> Sign Out
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                        <!-- END User Dropdown -->
                        <!-- Notifications -->
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-notifications" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bell"></i>
                                <span class="badge badge-primary badge-pill">5</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right min-width-300" aria-labelledby="page-header-notifications">
                                <h5 class="h6 text-center py-10 mb-0 border-b text-uppercase">Notifications</h5>
                                {{-- <ul class="list-unstyled my-20">
                                    <li>
                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                            <div class="ml-5 mr-15">
                                                <i class="fa fa-fw fa-check text-success"></i>
                                            </div>
                                            <div class="media-body pr-10">
                                                <p class="mb-0">You’ve upgraded to a VIP account successfully!</p>
                                                <div class="text-muted font-size-sm font-italic">15 min ago</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                            <div class="ml-5 mr-15">
                                                <i class="fa fa-fw fa-exclamation-triangle text-warning"></i>
                                            </div>
                                            <div class="media-body pr-10">
                                                <p class="mb-0">Please check your payment info since we can’t validate them!</p>
                                                <div class="text-muted font-size-sm font-italic">50 min ago</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                            <div class="ml-5 mr-15">
                                                <i class="fa fa-fw fa-times"></i>
                                            </div>
                                            <div class="media-body pr-10">
                                                <p class="mb-0">Web server stopped responding and it was automatically restarted!</p>
                                                <div class="text-muted font-size-sm font-italic">4 hours ago</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                            <div class="ml-5 mr-15">
                                                <i class="fa fa-fw fa-exclamation-triangle text-warning"></i>
                                            </div>
                                            <div class="media-body pr-10">
                                                <p class="mb-0">Please consider upgrading your plan. You are running out of space.</p>
                                                <div class="text-muted font-size-sm font-italic">16 hours ago</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                            <div class="ml-5 mr-15">
                                                <i class="fa fa-fw fa-plus text-primary"></i>
                                            </div>
                                            <div class="media-body pr-10">
                                                <p class="mb-0">New purchases! +$250</p>
                                                <div class="text-muted font-size-sm font-italic">1 day ago</div>
                                            </div>
                                        </a>
                                    </li>
                                </ul> --}}
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-center mb-0" href="javascript:void(0)">
                                    <i class="fa fa-flag mr-5"></i> View All
                                </a>
                            </div>
                        </div>
                        <!-- END Notifications -->

                        <!-- Toggle Side Overlay -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        {{-- <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="side_overlay_toggle">
                            <i class="fa fa-tasks"></i>
                        </button> --}}
                        <!-- END Toggle Side Overlay -->
                    </div>
                    <!-- END Right Section -->
                </div>
                <!-- END Header Content -->
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                @include('common.simple-alerts')
                @yield('content')
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="opacity-0">
                <div class="content py-20 font-size-sm clearfix">
                    <div class="float-left">
                        <a class="font-w600" href="https://1.envato.market/95j" target="_blank">{{ ($details != null)? $details->name : '' }} Portal</a> &copy; <span class="js-year-copy"></span>
                    </div>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="{{ mix('js/codebase.app.js') }}"></script>

        <!-- Laravel Scaffolding JS -->
        {{-- <script src="{{ mix('js/laravel.app.js') }}"></script> --}}

        {{-- for pop overs --}}
        {{-- data-toggle="popover" title="" data-placement="top" data-content="This is example content. You can put a description or more info here." data-original-title="Top Popover" --}}

        {{-- Plugins --}}
        <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('js/plugins/spectrum/src/spectrum.js') }}"></script>
        <script src="{{ asset('js/plugins/moment/moment.min.js') }}"></script>
        <script src="{{ asset('js/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
        <script src="{{ asset('js/plugins/flatpickr/flatpickr.min.js') }}"></script>

        {{-- <script src="{{ asset('js/plugins/ckeditor/ckeditor.js') }}"></script> --}}
        <script src="{{ asset('js/plugins/summernote/summernote-bs4.min.js') }}"></script>
        <script>var _tooltip = jQuery.fn.tooltip;</script>
        <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('js/plugins/jquery-ui/jquery.ui.touch-punch.min.js') }}"></script>
        <script>jQuery.fn.tooltip = _tooltip;</script>
        <!-- Page JS Helpers (CKEditor plugin) -->
        <script>jQuery(function(){ Codebase.helpers(['summernote', 'core-tab', 'draggable-items']); });</script>
        <script>
            $(".custom-file-input").on("change", function() {
                var fileName = $(this).val().split("\\").pop();
                $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
            });
        </script>

        <script>
            function removeActiveTab() {
                localStorage.removeItem('activeTab');
            }
            function cancelEdit() {
                window.location.reload()
            }
            setTimeout(() => {
                $(".alert-dismissable").fadeOut(1000);
            }, 5000)
        </script>
        
        <script>
            $('.init-summernote').summernote({
                lang: 'en-EN',
                codeviewFilter: false,
                dialogsInBody: true,
                height: 350,
                disableDragAndDrop: false,
                lineHeights: ['0.2', '0.3', '0.4', '0.5', '0.6', '0.8', '1.0', '1.2', '1.4', '1.5', '2.0', '3.0'],
                fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '22', '24', '30', '32', '34', '36', '40', '48' , '64', '72', '82', '150'],
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['height', ['height']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']],
                ],
                callbacks: {
                    onImageUpload: function(image) {
                        editor = $(this);
                        uploadImageContent(image[0], editor);
                    }
                }
            });
    
            function uploadImageContent(image, editor) {
                swal.fire({
                    // title: 'Uploading image to cloud.',
                    text: 'Uploading image to cloud. Please wait...',
                    showCancelButton: false,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    html: !1,
                })
                // console.log(image)
                var data = new FormData();
                data.append("image", image);
                data.append("folder", '/resources');
                data.append("_token", '{{ csrf_token() }}');
    
                $.ajax({
                    type: "post",
                    url: "{{ route('pb-upload-image') }}",
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: "POST",
                    data: data,
                    success: function(url) {
                        var image = $('<img>').attr('src', url);
                        $(editor).summernote("insertNode", image[0]);

                        swal.close();
                    },
                    error: function(data) {
                        console.log(data);
                        swal.close();
                    }
                });
            }

            function getCtaType(select, input_video, input_link) {
                switch($('select[name="'+select+'"]').val()) {
                    case 'link':
                        $('input[name="'+input_link+'"]').show()
                        $('input[name="'+input_video+'"]').hide()
                        break;
                    case 'video':
                        $('input[name="'+input_link+'"]').hide()
                        $('input[name="'+input_video+'"]').show()
                        break;
                    case 'youtube':
                        $('input[name="'+input_video+'"]').hide()
                        $('input[name="'+input_link+'"]').show().val('//www.youtube.com/embed/')
                        break;
                }
            }

            function uploadVideo(video, value) {
                var files = $('input[name="'+video+'"]')[0].files;

                swal.fire({
                    text: 'Uploading video to cloud. Please wait...',
                    showCancelButton: false,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    html: !1,
                })
                // console.log(image)
                var data = new FormData();
                data.append("video", files[0]);
                data.append("folder", '/resources/videos');
                data.append("_token", '{{ csrf_token() }}');
    
                $.ajax({
                    type: "post",
                    url: "{{ route('pb-upload-video') }}",
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: "POST",
                    data: data,
                    success: function(url) {
                        $('input[name="'+value+'"]').val(url)
                        swal.close();
                    },
                    error: function(data) {
                        console.log(data);
                        swal.close();
                    }
                });
            }

            function convertToSlug(title) {
                return title
                    .toLowerCase()
                    .replace(/[^\w ]+/g,'')
                    .replace(/ +/g,'-')
                    ;
            }
        </script>

        {{-- <script src="js/pages/calendar.js"></script> --}}
        <script src="{{ asset('js/pages/calendar.js') }}"></script>

        @yield('js_after')
        @yield('js_after_faq')
    </body>
</html>
