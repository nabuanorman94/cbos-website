@php
    $details = App\SiteDetails::first();
    $services = App\SitePage::where([['show', '=', 1], ['page_type', '!=', 'stand-alone']])->orderBy('id', 'asc')->get();
    $site_content = App\SiteContent::first();
    $pages = App\SitePage::where([['show', '=', 1], ['page_type', '=', 'stand-alone']])->orderBy('id', 'asc')->get();
@endphp

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-55872026-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-55872026-1');
    </script>

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title') {{ ($details)? $details->name : '' }}</title>
    <meta name="robots" content="index, follow" />
    {{-- <meta name="robots" content="noindex, nofollow"> --}}

    {{-- basic seo --}}
    @yield('seo')

    {{-- facebook og --}}
    @yield('facebook_og')

    <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/icofont/icofont.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    {{-- <link href="{{ asset('vendor/venobox/venobox.css') }}" rel="stylesheet"> --}}
    {{-- <link href="{{ asset('vendor/aos/aos.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('css/style.css') }}" rel="stylesheet"> --}}
    
    @yield('css_after')
</head>
<body class="site">
    {{-- Facebook SQK --}}
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v10.0&appId=199858108233629&autoLogAppEvents=1" nonce="hO4YEgS8"></script>
    {{-- End Facebook SDK --}}
    
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center">
    
            <div class="logo mr-auto">
                {{-- <h1 class="text-light"><a href="/"><span>CBOS</span></a></h1> --}}
                <a href={{ route('homepage') }}><img src="{{ ($details)? $details->logo : '' }}" alt="" class="img-fluid"></a>
            </div>
            
            <nav class="nav-menu d-none d-lg-block">
                <ul>
                {{-- <li><a href="{{ route('homepage') }}#about">About Us</a></li>
                <li><a href="{{ route('homepage') }}#pricing">Pricing</a></li> --}}

                @if ($services && count($services) > 0)
                        <li class="drop-down"><a href="" onclick="return false">Ph Registrations</a>
                            <ul>
                                @foreach ($services as $item)
                                    @if ($item->page_type == 'ph-registrations')
                                        <li><a href="{{ route('view-service-page', $item->slug) }}">{{ $item->title }}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                        <li class="drop-down"><a href="" onclick="return false">Bookkeeping & Accounting</a>
                            <ul>
                                @foreach ($services as $item)
                                    @if ($item->page_type == 'bookkeeping-accounting')
                                        <li><a href="{{ route('view-service-page', $item->slug) }}">{{ $item->title }}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                @endif
                
                <li class="drop-down"><a href="" onclick="return false">Resources</a>
                    <ul>
                    <li><a href="{{ route('view-all-blogs', ['category' => 'all']) }}">News & Blogs</a></li>
                    {{-- <li><a href="#">Articles & Videos</a></li> --}}
                    {{-- <li><a href="{{ route('view-qotd', ['date' => Carbon\Carbon::now()->setTimezone('Asia/Manila')->format('Y-m-d')]) }}">Question of the Day</a></li>  --}}
                    <li><a href="{{ route('view-qotd') }}">Question of the Day</a></li>
                    </ul>
                </li>
                <li><a href="{{ route('homepage') }}#contact">Contact Us</a></li>
        
        
                <!-- <li><a href="#portfolio">Portfolio</a></li>
                <li><a href="#team">Team</a></li> -->
                
                @guest
                    {{-- <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif --}}
                    {{-- <li class="get-started"><a href="{{ route('login') }}">Sign In</a></li> --}}
                    <li class="get-started"><a href="{{ route('get-started') }}">Get Started</a></li>
                @else
                    <li class="drop-down">
                        <a id="navbarDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->username }}
                        </a>

                        <ul>
                            <li>
                                @switch(auth()->user()->getRoleNameAttribute())
                                    @case('Admin')
                                        <a href="{{ route('admin-home') }}">Admin Dashboard</a>
                                        @break
                                    @case('User')
                                        <a href="{{ route('home') }}">Dashboard</a>
                                        @break
                                    @default
                                @endswitch
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>
                @endguest

                </ul>
            </nav><!-- .nav-menu -->
    
        </div>
    </header>
    {{-- <div id="app">
      @if (Route::currentRouteName() != 'homepage')
        <section class="breadcrumbs">
          <div class="container">
      
            <div class="d-flex justify-content-between align-items-center">
              <h5 class="mb-0">{{ $breadcrumbs_title }}</h5>
              <ol>
                  @foreach ($breadcrumbs_links as $item)
                        <li><a href="{{ route($item['route']) }}">{{ $item['name'] }}</a></li>
                  @endforeach
              </ol>
            </div>
      
          </div>
      </section>
      
      <section class="inner-page">
          <div class="container">
            <main class="pb-4">
              @yield('content')
            </main>
          </div>
      </section>
      @else
        <main class="pb-4">
            @yield('content')
        </main>
      @endif
    </div> --}}

    <div id="app">
      <main class="pb-4">
          @yield('content')
      </main>
    </div>

    <footer id="footer">
      <div class="container">
        <div class="row d-flex align-items-center">
          <div class="col-lg-6 text-lg-left text-center">
            <div class="copyright">
              &copy; Copyright 2021 <strong>{{ ($details)? $details->name : '' }}</strong>. All Rights Reserved
            </div>
            
          </div>
          <div class="col-lg-6">
            <nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">
              {{-- <a href="/" class="scrollto">Home</a> --}}
              @if ($pages)
                @foreach ($pages as $page)
                <a href="{{ URL::to($page->slug) }}" class="scrollto">{{ $page->title }}</a>
                @endforeach
              @endif
            </nav>
          </div>
        </div>
      </div>
    </footer>

    {{-- Video Modal Here --}}
    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>        
                    <!-- 16:9 aspect ratio -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div> 

    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    {{-- <script src="{{ asset('vendor/php-email-form/validate.js') }}"></script> --}}
    <script src="{{ asset('vendor/waypoints/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('vendor/counterup/counterup.min.js') }}"></script>
    <script src="{{ asset('vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    {{-- <script src="{{ asset('vendor/venobox/venobox.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('vendor/aos/aos.js') }}"></script> --}}

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <!-- Scripts -->
    <script src="{{ asset('js/site.js') }}" defer></script>
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    <script>
        $(document).ready(function() {
            var $videoSource;  

            $('.video-btn').click(function() {
                $videoSource = $(this).data( "src" );
            });

            setTimeout(() => {
                $('#videoModal').on('shown.bs.modal', function (e) {
                    $("#video").attr('src',$videoSource + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
                });

                $('#videoModal').on('hide.bs.modal', function (e) {
                    $("#video").attr('src',''); 
                });
            }, 500)
        });
    </script>
    @yield('js_after')
    @yield('js_after_contact')
    @yield('js_after_newsletter')

    @include('includes/freshchat')
</body>
</html>