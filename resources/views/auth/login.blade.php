@extends('layouts.auth')

@section('title')
    Sign In - 
@endsection

@php
    $details = App\SiteDetails::first();
@endphp

@section('content')
    <div id="app">
        <!-- Main Container -->
    <main id="main-container">
        <!-- Page Content -->
        <div class="bg-image" style="background-image: url({{ asset('media/photos/photo34@2x.jpg') }});">
            <div class="row mx-0 bg-black-op">
                <div class="hero-static col-md-6 col-xl-9 d-none d-md-flex align-items-md-end">
                    <div class="p-30">
                        <p class="font-size-h3 font-w600 text-white">
                            {{ $details->name }}
                        </p>
                        <p class="font-italic text-white-op">
                            Copyright &copy; <span class="js-year-copy"></span>
                        </p>
                    </div>
                </div>
                <div class="hero-static col-md-6 col-xl-3 d-flex align-items-center bg-white">
                    <div class="content content-full">
                        <!-- Header -->
                        <div class="p-10 text-center">
                            <a href={{ route('homepage') }}><img src="{{ ($details)? $details->logo : '' }}" alt="" class="w-50"></a>
                            {{-- <h1 class="h3 font-w700 mt-30 mb-10">Login</h1> --}}
                            <h2 class="h5 font-w400 text-muted mt-3 mb-0">Please sign in to your account</h2>
                        </div>
                        <!-- END Header -->
                        @include('common.simple-alerts')
                        <!-- Sign In Form -->
                        <!-- jQuery Validation functionality is initialized with .js-validation-signin class in js/pages/op_auth_signin.min.js which was auto compiled from _es6/pages/op_auth_signin.js -->
                        <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                        <form class="js-validation-signin p-10" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material floating">
                                        <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                                        @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <label for="username">Username</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material floating">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <label for="password">Password</label>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="form-group row">
                                <div class="col-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="remember-me" name="remember-me">
                                        <label class="custom-control-label" for="remember-me">Remember Me</label>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="form-group">
                                <button type="submit" class="btn btn-block btn-sm btn-hero btn-primary mt-30">
                                    <i class="si si-login mr-10"></i> Sign In
                                </button>
                                {{-- <div class="mt-30">
                                    <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="op_auth_signup2.html">
                                        <i class="fa fa-plus mr-5"></i> Create Account
                                    </a>
                                    <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="op_auth_reminder2.html">
                                        <i class="fa fa-warning mr-5"></i> Forgot Password
                                    </a>
                                </div> --}}
                            </div>
                        </form>
                        <!-- END Sign In Form -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->
    </div>
@endsection

@section('js_after')
    <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    {{-- <script src="{{ asset('js/pages/op_auth_signin.min.js') }}"></script> --}}
@endsection