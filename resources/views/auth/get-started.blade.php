@extends('layouts.auth')

@section('title')
    Enjoy 2 filing months for free -
@endsection

@php
    $details = App\SiteDetails::first();
@endphp

@section('content')
    <div id="app" class="main-content-boxed" data-success="{{ Session::get('success') }}">
        <main id="main-container">
            <div class="">
                <div class="row mx-0 justify-content-center">
                    <div class="hero-static col-lg-6 col-xl-4">
                        <div class="content content-full overflow-hidden">
                            <div class="py-30 text-center">
                              <div class="p-10 text-center">
                                  <a href={{ route('homepage') }}><img src="{{ ($details)? $details->logo : '' }}" alt="" class="w-50"></a>
                              </div>
                            </div>

                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissable" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form method="POST" action="{{ route('get-started-send') }}" class="get-started-form">
                                @csrf
                                <small class="text-left" id="contact-us-success"></small>
                                <div class="block block-themed block-rounded block-shadow">
                                    <div class="block-header bg-default-lighter">
                                        <h3 class="block-title">Client's Information</h3>
                                    </div>
                                    <div class="block-content">
                                        @error('info')
                                            <div class="alert alert-info mb-0">
                                                <i class="fa fa-info-circle"></i> {{ $message }}
                                            </div>
                                        @enderror
                                        
                                        <div class="form-group row">
                                            <div class="col-6">
                                                <div class="form-material {{ !old('first_name') ? 'floating' : ''}}">
                                                    <input id="first_name" type="input" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required>
                                                    <label for="first_name">First Name <small class="required">*</small></label>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-material {{ !old('last_name') ? 'floating' : ''}}">
                                                    <input id="last_name" type="input" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required>
                                                    <label for="last_name">Last Name <small class="required">*</small></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <div class="form-material {{ !old('contact_no') ? 'floating' : ''}}">
                                                    <input id="contact_no" type="input" class="form-control @error('contact_no') is-invalid @enderror" name="contact_no" value="{{ old('contact_no') }}" required>
                                                    <label for="contact_no">Contact Number <small class="required">*</small></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <div class="form-material {{ !old('email') ? 'floating' : ''}}">
                                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required>
                                                    <label for="email">Email Address <small class="required">*</small></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <div class="form-material {{ !old('company_name') ? 'floating' : ''}}">
                                                    <input id="company_name" type="input" class="form-control @error('company_name') is-invalid @enderror" name="company_name" value="{{ old('company_name') }}">
                                                    <label for="company_name">Company/Business Name <small>(Optional)</small></label>
                                                </div>
                                            </div>
                                        </div>

                                        {{-- <div class="form-group row mt-2 mb-0">
                                            <div class="col-sm-6 push">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" id="get-started-terms" name="get_started_terms" class="custom-control-input"> 
                                                    <label for="get-started-terms" class="custom-control-label">I agree to Terms &amp; Conditions</label>
                                                </div>
                                            </div> 
                                        </div> --}}

                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-block btn-sm btn-hero btn-primary mt-30" id="submit">
                                                <i class="si si-envelope mr-10"></i> Submit
                                            </button>
                                        </div>
                                    </div>

                                    {{-- <div class="block-content bg-body-light">
                                        <div class="form-group text-center">
                                            <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="#" data-toggle="modal" data-target="#modal-terms">
                                                <i class="fa fa-book text-muted mr-5"></i> Read Terms
                                            </a>
                                        </div>
                                    </div> --}}

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>

    {{-- <div class="modal fade" id="modal-terms" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-slidedown" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header">
                        <h3 class="block-title">Terms &amp; Conditions</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="text">
                            <div>
                                <h3 class="text-primary">Terms and Conditions</h3>
                                
                                Content Here

                            </div>
                        </div>    
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-alt-info" onclick="agreeToTerms()" data-dismiss="modal">
                        <i class="fa fa-check"></i> I Agree
                    </button>
                </div>
            </div>
        </div>
    </div> --}}
@endsection

@section('css_after')
<link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script>
        $(".get-started-form").submit(function(e) {

            e.preventDefault();

            // if($('.get-started-form input[name="get_started_terms"]').is(":checked")) {
                $('#submit').attr({
                    disabled: true,
                }).html('<i class="fa fa-cog fa-spin"></i> Loading...');

                var form = $(this);
                var url = form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(),
                    success: function(response)
                    {
                        $('.get-started-form input[name="first_name"]').val('');
                        $('.get-started-form input[name="last_name"]').val('');
                        $('.get-started-form input[name="contact_no"]').val('');
                        $('.get-started-form input[name="email"]').val('');
                        $('.get-started-form input[name="company_name"]').val('');
                        $('.get-started-form textarea').val('');
                        $('.get-started-form input[name="get_started_terms"]').prop('checked', false);
                        $('#contact-us-success').html('<div class="alert alert-success alert-dismissable mb-3" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><p class="mb-0"><i class="fa fa-check"></i> '+response.success+'</p></div>');
                        $('#submit').removeClass('btn-primary').addClass('btn-alt-info').html('<i class="fa fa-check"></i> Success');
                    }
                });
            // } else {
            //     swal.fire({
            //         title: "Accept Terms and Condition",
            //         text: "Please accept the terms and condition below to continue.",
            //         icon: "info",
            //         showCancelButton: !1,
            //         buttonsStyling: !1,
            //         customClass: {
            //             confirmButton: "btn btn-alt-warning m-5",
            //             cancelButton: "btn btn-alt-danger m-5",
            //             input: "form-control"
            //         },
            //         html: !1,
            //         allowOutsideClick: false,
            //     });
            // }

            
        });

        // function agreeToTerms() {
        //     $('#get-started-terms').prop('checked', true)
        // }
    </script>
@endsection