@extends('layouts.auth2')

@section('title')
    Schedule A Call
@endsection

@php
    $details = App\SiteDetails::first();
@endphp

@section('content')
    <div id="app" class="main-content-boxed" data-success="{{ Session::get('success') }}">
        <main id="main-container" style="height: 100vh;">
            <div class="row vh-100">
                <div class="col-12 @if (count($schedules) > 0) @if($time) col-lg-6 @else col-lg-4 @endif @else col-lg-2 @endif mx-auto d-flex align-items-center">
                    <div class="block block-rounded mx-auto">
                        <!-- Alerts -->
                        @include('common.simple-alerts')
                        <div class="block-header">
                            <h3 class="block-title">Select Date and Time</h3>
                        </div>
                        <div class="block-content">
                            <div class="row d-flex">
                                <div class="col-12 @if (count($schedules) > 0) @if($time) col-md-4 @else col-md-6 @endif @else col-lg-12 @endif mx-auto">
                                    <p class="mb-2">Please schedule a quick call with us, so that we could help you out. The call will be over Google Hangouts.</p>
                                    <h4 class="mb-2"><span class="fa fa-clock-o"></span> 30 Min</h4>
                                    <p>Speak to you soon! :)</p>

                                    <div class="form-group mt-4 d-flex">
                                        <input type="text" class="form-control w-100" id="schedule_date" name="schedule_date" data-inline="true">
                                    </div>
                                </div>
                                @if (count($schedules) > 0)
                                    <div class="col-12 @if($time) col-md-4 @else col-md-6 @endif d-flex">
                                        <div class="mx-auto">
                                            <ul class="pl-0" style="columns: 2; -webkit-columns: 2; -moz-columns: 2; list-style: none;">
                                                @foreach ($schedules as $item)
                                                    <li>
                                                        <button type="button" class="btn btn-rounded @if($time == $item->time_start) btn-secondary @else btn-info @endif min-width-125 mb-3" onclick="setTime({{ $item }})">{{ $item->time_start }}</button>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                                @if ($time)
                                    <div class="col-12 col-md-4">
                                        <form action="{{ route('schedule-a-call-submit') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="sched_id" value="{{ $sched_id }}">
                                            <div class="form-material floating">
                                                <input type="text" class="form-control" name="name" placeholder="" value="{{ old('name') }}" required>
                                                <label>Name *</label>
                                            </div>
                                            <div class="form-material floating">
                                                <input type="text" class="form-control" name="email" placeholder="" value="{{ old('email') }}" required>
                                                <label>Email *</label>
                                            </div>
                                            <div class="form-material floating">
                                                <input type="text" class="form-control" name="phone" placeholder="" value="{{ old('phone') }}">
                                                <label>Phone</label>
                                            </div>
                                            <div class="form-material mt-3 mb-2">
                                                <textarea class="form-control" name="details" placeholder="" value="{{ old('details') }}" rows="5"></textarea>
                                                <label class="mb-3"><i>Share anything that will help prepare for our meeting.</i> *</label>
                                            </div>
                                            <div class="form-material mb-4">
                                                <input type="text" class="form-control" name="date_time" placeholder="" value="{{ \Carbon\Carbon::createFromTimeStamp(strtotime($date))->isoFormat('LL') . ' ' . $time }}" readonly>
                                                <label>Date Time *</label>
                                            </div>
                                            <div class="form-group text-center">
                                                <button type="submit" class="btn btn-primary min-width-125 mb-3"><i class="fa fa-check"></i> Schedule Call</button>
                                            </div>
                                        </form>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

@section('css_after')
    <style>
        .event {
            position: absolute;
            width: 3px;
            height: 3px;
            border-radius: 150px;
            bottom: 3px;
            left: calc(50% - 1.5px);
            content: " ";
            display: block;
            background: #3d8eb9;
        }

        .event.busy {
            background: #f64747;
        }
    </style>
@endsection

@section('js_after')
    <script>
        var active_dates = @json($active_dates);

        flatpickr("#schedule_date", {
            altInput: true,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d",
            enable: active_dates,
            onReady (_, __, fp) {
                fp.calendarContainer.classList.add("mx-auto");
            },
            onChange: function(selectedDates, dateStr, instance) {
                var url = '{{ route("schedule-a-call") }}?date=' + dateStr;
                // url = url.replace(':date', dateStr);
                window.location.href = url;
            },
            onDayCreate: function(dObj, dStr, fp, dayElem) {
                var date = dayElem.dateObj;
                var my_date = moment(date).format('Y-MM-DD')
                // console.log(String(my_date))
                if(active_dates.indexOf(my_date) >= 0) {
                    // console.log(my_date)
                    dayElem.innerHTML += "<span class='event'></span>";
                }
            }
        });

        function setTime(item) {
            var url = '{{ route("schedule-a-call") }}?date=' + item.date + '&time=' + item.time_start + '&id=' + item.id;
            window.location.href = url;
        }
    </script>
@endsection