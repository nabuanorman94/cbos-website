@extends('layouts.app')

@section('title')
    {{ $blog->title }} -
@endsection

@section('facebook_og')
    <meta name="description" content="{{ $blog->seo_serp }}" />

    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:type" content="{{ $page_type }}" />
    <meta property="og:title" content="{{ $blog->title }}" />
    <meta property="og:description" content="{{ $blog->seo_serp }}" />
    <meta property="og:image" content="{{ $blog->seo_image }}" />
@endsection

@section('content')
    <section id="hero-3" class="d-flex align-items-center" style="background-image: url({{ $blog->banner_image }}); background-color: {{ $blog->bg_color }}">
        <div class="container heading-content">
            <div class="row">
                
                <div class="col-12 justify-content-center">
                    <div>
                        {!! $blog->sub_title !!}
                    </div>
                    <div class="text-center">
                        <small>
                            <p class="mb-0">
                                {{ $blog->author }}
                            </p>
                            <i>
                                <p>
                                    {{ \Carbon\Carbon::createFromTimeStamp(strtotime($blog->created_at))->diffForHumans() }}
                                </p>
                            </i>
                        </small>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    @if ($blog->banner_image_mobile)
        <div class="hero-mobile">
            <img src="{{ $blog->banner_image_mobile }}" alt="{{ $blog->title }}" class="w-100">
        </div>
    @endif

    <main id="main">
        <section class="inner-page">
            <div class="container">
                <a href="javascript:void(0)" onclick="goBack()" class="d-flex mb-3"><i class="ri-arrow-left-line my-auto mr-3" style="font-size: 25px;"></i></a>
                <div class="row">
                    <div class="col-12 col-md-9">
                        {{-- @if ($blog->seo_image)
                            <img src="{{ $blog->seo_image }}" alt="{{ $blog->title }}" class="w-100 mb-4">
                        @endif --}}
                        @foreach ($blog_sections as $item)
                        <div class="{{ $item->div_class }}" id="{{ $item->div_id }}">
                            @if ($item->contents)
                                @foreach ($item->contents as $content)
                                    <div class="{{ $content->div_class }}" id="{{ $content->div_id }}" style="{{ $content->div_styling }}">
                                        <div class="html-content">
                                            {!! $content->value !!} 
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            @if ($item->content_sections)
                                @foreach ($item->content_sections as $content_section)
                                    <div class="{{ $content_section->div_class }}" id="{{ $content_section->div_id }}" style="{{ $content_section->div_styling }}">
                                        <h3 class="html-heading">{{ $content_section->content_heading }}</h3>
                                        <h5 class="html-sub-heading">{{ $content_section->content_sub_heading }}</h5>
                                        <div class="html-content">
                                            {!! $content_section->content_value !!} 
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            @if ($item->feature_blocks)
                                @foreach ($item->feature_blocks as $feature_block)
                                    <div class="{{ $feature_block->div_class }}" id="{{ $feature_block->div_id }}" style="{{ $feature_block->div_styling }}">
                                        <div class="mb-2">
                                            <img src="{{ $feature_block->feat_image }}" alt="{{ $feature_block->feat_title }} Feature Image" class="w-100">
                                        </div>
                                        {{-- <h4 class="html-title mb-2">{{ $feature_block->feat_title }}</h4> --}}
                                        <div class="html-content">
                                            {!! $feature_block->feat_content !!} 
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            @if ($item->feature_image_blocks)
                                @foreach ($item->feature_image_blocks as $feature_image_block)
                                    <div class="{{ $feature_image_block->div_class }}" id="{{ $feature_image_block->div_id }}" style="{{ $feature_image_block->div_styling }}">
                                        @if ($feature_image_block->feat_image)
                                            <div class="mb-2">
                                                <img src="{{ $feature_image_block->feat_image }}" alt="{{ $feature_image_block->feat_title }} Feature Image" class="w-100">
                                            </div>
                                        @endif
                                        <div class="html-content">
                                            {!! $feature_image_block->feat_title !!} 
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            @if ($item->feature_sections)
                                @foreach ($item->feature_sections as $feature_section)
                                    <div class="{{ $feature_section->div_class }}" id="{{ $feature_section->div_id }}" style="{{ $feature_section->div_styling }}">
                                        @if ($feature_section->feat_image)
                                            @if ($feature_section->image_placement == 'left')
                                                <div class="row mb-3 d-flex align-items-center" style="padding: 40px 0 20px">
                                                    <div class="col-sm-12 col-lg-6 order-1 order-lg-1 hero-img">
                                                        <img src="{{ $feature_section->feat_image }}" class="img-fluid" alt="{{ $feature_section->feat_title }}">
                                                    </div>
                                                    <div class="col-sm-12 col-lg-6 pt-5 pt-lg-0 order-2 order-lg-2 justify-content-center">
                                                        <h3 class="html-heading">{{ $feature_section->feat_title }}</h3>
                                                        <div class="html-content">{!! $feature_section->feat_content !!}</div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="row mb-3 d-flex align-items-center" style="padding: 40px 0 20px">
                                                    <div class="col-sm-12 col-lg-6 order-1 order-lg-2 hero-img">
                                                        <img src="{{ $feature_section->feat_image }}" class="img-fluid" alt="{{ $feature_section->feat_title }}">
                                                    </div>
                                                    <div class="col-sm-12 col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 justify-content-center">
                                                        <h3 class="html-heading">{{ $feature_section->feat_title }}</h3>
                                                        <div class="html-content">{!! $feature_section->feat_content !!}</div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        @endforeach
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="fb-share-button mb-3" data-href="{{ url()->current() }}" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}" class="fb-xfbml-parse-ignore">Share</a></div>

                        <div class="bg-tone p-3 mb-4">
                            <h5>Recent Blogs</h5>
                            @if (isset($recent_blogs))
                                @foreach ($recent_blogs as $recent_blog)
                                    <p>
                                        <h6 class="mb-0"><a href="{{ route('view-blog', $recent_blog->slug) }}">{{ $recent_blog->title }}</a></h6>
                                        <small><i>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($recent_blog->created_at))->diffForHumans() }}</i></small>
                                    </p>
                                @endforeach
                            @endif
                        </div>
                        <div class="bg-tone p-3 mb-4">
                            <h5>Blog Categories</h5>
                            <p>
                                <h6 class="mb-0"><a href="{{ route('view-all-blogs', ['category' => 'all']) }}">All</a></h6>
                            </p>
                            @if (isset($categories))
                                @foreach ($categories as $category)
                                    <p>
                                        <h6 class="mb-0"><a href="{{ route('view-all-blogs', ['category' => $category->slug]) }}">{{ $category->name }}</a></h6>
                                    </p>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        {{-- FAQs List --}}
        @if ($faqs && count($faqs) > 0)
            @include('includes/faq')
        @endif

        <section class="inner-page">
            <div class="container">
                <div class="section-title">
                    <h2>Related Articles</h2>
                </div>
                <div class="row d-flex mt-4">
                    @if (isset($related_blogs))
                        @foreach ($related_blogs as $related_blog)
                        <div class="col-md-6 col-lg-4 d-flex align-items-stretch mb-3 mx-auto">
                            {{-- <div class="card" style="background-color: {{ $related_blog->bg_color }};background-image: url('{{ $related_blog->banner_image }}');background-position: center; background-repeat: no-repeat;">
                                <div class="card-body">
                                    <h5 class="card-title"><a href="{{ route('view-blog', $related_blog->slug) }}">{{ $related_blog->title }}</a></h5>
                                    <p class="card-text">{{ $related_blog->seo_serp }}</p>
                                    <small>
                                        <p class="text-muted mb-0">
                                            {{ $related_blog->author }}
                                        </p>
                                        <i>
                                            <p>
                                                {{ \Carbon\Carbon::createFromTimeStamp(strtotime($related_blog->created_at))->diffForHumans() }}
                                            </p>
                                        </i>
                                    </small>
                                    <div class="d-flex">
                                        <div class="read-more"><a href="{{ route('view-blog', $related_blog->slug) }}"><i class="icofont-arrow-right"></i> Read More</a></div>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="card">
                                <p>
                                    <i style="min-width: 200px; border-bottom: 2px solid #7bbfce;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i>
                                </p>
                                <p class="mb-2">
                                    {{ \Carbon\Carbon::createFromTimeStamp(strtotime($related_blog->created_at))->isoFormat('ll') }}
                                </p>
                                <h5><a href="{{ route('view-blog', $related_blog->slug) }}" class="text-muted">{{ $related_blog->title }}</a></h5>
                                <p class="card-text">{{ $related_blog->seo_serp_trim }}</p>
                            </div>
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </section>

        @if ($site_content && $site_content->show_content_2_section)
            @include('includes/join_us')
        @endif

        @include('includes/contact')
        
    </main>
@endsection

@section('js_after')
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
@endsection