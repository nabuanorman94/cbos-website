@extends('layouts.app')

@section('title')
    {{ $page->title }} -
@endsection

@section('facebook_og')
    <meta name="description" content="{{ $page->seo_serp }}" />

    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:type" content="{{ $page->page_type }}" />
    <meta property="og:title" content="{{ $page->title }}" />
    <meta property="og:description" content="{{ $page->seo_serp }}" />
    <meta property="og:image" content="{{ $page->seo_image }}" />
@endsection

@section('content')
    
    <section id="hero-3" class="d-flex align-items-center" style="background-image: url({{ $page->banner_image }}); background-color: {{ $page->bg_color }}">
        <div class="container heading-content">
            <div class="row">
                
                <div class="col-12 order-1 order-lg-1 justify-content-center">
                    <div>
                        {!! $page->sub_title !!}
                    </div>
                    {{-- <p class="text-center">
                        {{ \Carbon\Carbon::createFromTimeStamp(strtotime($qotd->start))->isoFormat('LL') }}
                    </p> --}}
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3">
                            <form id="search-qotd" action="{{ route('view-qotd') }}" method="GET">
                                <div class="form-group">
                                    <label for="search" class="sr-only">Search</label>
                                    <input type="text" class="form-control" id="search" name="search" placeholder="Search Questions">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    @if ($page->banner_image_mobile)
        <div class="hero-mobile">
            <img src="{{ $page->banner_image_mobile }}" alt="{{ $page->title }}" class="w-100">
        </div>
    @endif

    <main id="main">
        @foreach ($page_sections as $item)
        <section class="inner-page" style="{{ $item->div_styling }} background-image: url({{ $page->div_image_background }});">
            <div class="{{ $item->section_style }}">
                <div class="{{ $item->div_class }}" id="{{ $item->div_id }}">
                    @if ($item->contents)
                        @foreach ($item->contents as $content)
                            <div class="{{ $content->div_class }}" id="{{ $content->div_id }}" style="{{ $content->div_styling }}">
                                <div class="html-content">
                                    {!! $content->value !!} 
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if ($item->content_sections)
                        @foreach ($item->content_sections as $content_section)
                            <div class="{{ $content_section->div_class }}" id="{{ $content_section->div_id }}" style="{{ $content_section->div_styling }}">
                                <h3 class="html-heading">{{ $content_section->content_heading }}</h3>
                                <h5 class="html-sub-heading">{{ $content_section->content_sub_heading }}</h5>
                                <div class="html-content">
                                    {!! $content_section->content_value !!} 
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if ($item->feature_blocks)
                        @foreach ($item->feature_blocks as $feature_block)
                            <div class="{{ $feature_block->div_class }}" id="{{ $feature_block->div_id }}" style="{{ $feature_block->div_styling }}">
                                <div class="mb-2">
                                    <img src="{{ $feature_block->feat_image }}" alt="{{ $feature_block->feat_title }} Feature Image" class="w-100">
                                </div>
                                {{-- <h4 class="html-title mb-2">{{ $feature_block->feat_title }}</h4> --}}
                                <div class="html-content">
                                    {!! $feature_block->feat_content !!} 
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if ($item->feature_image_blocks)
                        @foreach ($item->feature_image_blocks as $feature_image_block)
                            <div class="{{ $feature_image_block->div_class }}" id="{{ $feature_image_block->div_id }}" style="{{ $feature_image_block->div_styling }}">
                                @if ($feature_image_block->feat_image)
                                    <div class="mb-2">
                                        <img src="{{ $feature_image_block->feat_image }}" alt="{{ $feature_image_block->feat_title }} Feature Image" class="w-100">
                                    </div>
                                @endif
                                <div class="html-content">
                                    {!! $feature_image_block->feat_title !!} 
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if ($item->feature_sections)
                        @foreach ($item->feature_sections as $feature_section)
                            <div class="{{ $feature_section->div_class }}" id="{{ $feature_section->div_id }}" style="{{ $feature_section->div_styling }}">
                                @if ($feature_section->feat_image)
                                    @if ($feature_section->image_placement == 'left')
                                        <div class="row mb-3 d-flex align-items-center" style="padding: 40px 0 20px">
                                            <div class="col-sm-12 col-lg-6 order-1 order-lg-1 hero-img">
                                                <img src="{{ $feature_section->feat_image }}" class="img-fluid" alt="{{ $feature_section->feat_title }}">
                                            </div>
                                            <div class="col-sm-12 col-lg-6 pt-5 pt-lg-0 order-2 order-lg-2 justify-content-center">
                                                <h3 class="html-heading">{{ $feature_section->feat_title }}</h3>
                                                <div class="html-content">{!! $feature_section->feat_content !!}</div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="row mb-3 d-flex align-items-center" style="padding: 40px 0 20px">
                                            <div class="col-sm-12 col-lg-6 order-1 order-lg-2 hero-img">
                                                <img src="{{ $feature_section->feat_image }}" class="img-fluid" alt="{{ $feature_section->feat_title }}">
                                            </div>
                                            <div class="col-sm-12 col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 justify-content-center">
                                                <h3 class="html-heading">{{ $feature_section->feat_title }}</h3>
                                                <div class="html-content">{!! $feature_section->feat_content !!}</div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </section>
        @endforeach
        
        {{-- @if ($qotd)
            <section>
                <div class="container">
                    @if (isset($info))
                        <div class="alert alert-info alert-dismissable mb-0" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <p class="mb-0">{{ $info }}</p>
                        </div>
                    @endif
                    <div class="row mt-3">
                        <div class="col-12 col-md-5 mb-3">
                            <div id="calendar" class="form-inline">
                                <label for="">Select Date: </label>
                                <input type="text" class="form-control ml-3">
                            </div>
                        </div>
                        <div class="col-12">
                            <h4 class="text-center mb-3">{{ $qotd->title }}</h4>
                            <div class="html-content">
                                {!! $qotd->description !!}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif --}}

        <section>
            <div class="container">
                <div class="d-flex align-items-center mb-3">
                    {{-- <p>  Search Results</p> --}}
                    @if (app('request')->input('search') != null)
                        <a href="{{ route('view-qotd') }}" class="d-flex"><i class="ri-arrow-left-line my-auto" style="font-size: 25px;"></i></a>
                    @endif
                    <p class="ml-auto my-auto"><i>{{ $results }} {{ ($results > 1)? 'Results' : 'Result' }}</i></p>
                </div>
                @if (count($qotds) > 0)
                    <div class="row">
                        @foreach ($qotds as $item)
                            <div class="col-12 col-md-4">
                                <a href="javascript:void(0)" class="w-100" onclick="showQotdModal({{ $item }})">
                                    <div class="text-center">
                                        <iframe frameborder="0" src="{{ $item->video }}" width="100%" height="200" class="note-video-clip"></iframe>
                                    </div>
                                    <p class="mb-0 text-muted"><b>{{ $item->title }}</b></p>
                                    <p><small>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item->start))->isoFormat('LL') }}</small></p>
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <div class="mt-3">
                        {{ $qotds->render("pagination::bootstrap-4") }}
                    </div>
                @else 
                   <div class="row">
                        <div class="col-12">
                            <div class="alert alert-info mb-0" role="alert">
                                <p class="mb-0">No Results Found</p>
                            </div>
                        </div>
                   </div>
                @endif
            </div>
        </section>
        

        @if ($faqs && count($faqs) > 0)
            @include('includes/faq')
        @endif

        @if ($site_content && $site_content->show_content_2_section)
            @include('includes/join_us')
        @endif

        @include('includes/contact')

        @include('includes/newsletter')

    </main>

    <div id="modal-view-qotd" class="modal modal-top fade calendar-modal">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center">
                        <iframe frameborder="0" src="" width="100%" height="360" class="note-video-clip"></iframe>
                    </div>
                    <h4 class="modal-title mb-2"><span class="event-title"></span></h4>
                    <div class="event-body"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css_after')
    <style>
        /* .datepicker {
            width: 100%;
        }
        .datepicker {
            width: 100%;
        } */
    </style>
@endsection

{{-- @section('js_after')
    <script>
        var dp = $('#calendar input').datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
        }).on('changeDate', function(e) {
            // console.log(e.format());
            var date = e.format()
            window.location.href = '/question-of-the-day/' + date
        });

        dp.datepicker('update', new Date('{{ $qotd->start }}'));
    </script>
@endsection --}}

@section('js_after')
    <script>
        $( function() {
            $( "#search" ).autocomplete({
                minLength: 3,
                source: function( request, response ) {
                    $.ajax({
                        url:"{{ route('autocomplete-qotd')}} ",
                        type: 'POST',
                        dataType: "json",
                        data: {
                            _token: "{{ csrf_token() }}",
                            search: request.term
                        },
                        success: function( data ) {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) {
                    $('#search').val(ui.item.label);
                    $('#search-qotd').submit();
                    return false;
                }
            });
        });
        
        function showQotdModal(item) {
            console.log(item)
            $('.event-title').html(item.title);
            $('#modal-view-qotd .note-video-clip').attr('src', item.video)
            $('.event-body').html(item.description);
            $('#modal-view-qotd').modal();
        }
    </script>
@endsection