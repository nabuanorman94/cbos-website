@extends('layouts.app2')

@section('title')
    Website Under Maintenance
@endsection

@section('content')
    <div id="app" class="main-content-boxed">
        <main id="main-container">
            <section id="hero" class="d-flex align-items-center">
              
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 order-1 order-lg-2 hero-img">
                    <img src="{{ asset('/media/images/construction2.gif') }}" class="img-fluid" alt="">
                    <!-- Credits to <a href="https://storyset.com/people">Illustration by Freepik Storyset</a> -->
                  </div>
                  <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                    <h1>Website Under Maintenance</h1>
                    <h2 data-aos-delay="300">Our website is currently undergoing scheduled maintenance. We should be back shortly.</h2>
                    <div data-aos-delay="400">
                      <a href="https://web.facebook.com/cbos.official" class="btn-get-started scrollto">Reach us on Facebook</a>
                    </div>
                  </div>
                </div>
              </div>
            
            </section>
        </main>
    </div>
@endsection