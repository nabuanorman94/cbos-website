@extends('layouts.app')

@section('seo')
    <meta name="description" content="{{ ($details != null)? strip_tags($details->description) : '' }}">
@endsection

@section('facebook_og')
<meta property="og:url" content="{{ config('app.url') }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ ($details != null)? $details->name : '' }}" />
    <meta property="og:description" content="{{ ($details != null)? strip_tags($details->description) : '' }}" />
    <meta property="og:image" content="{{ ($details != null)? $details->logo : '' }}" />
@endsection

@section('content')
    @include('includes/hero')

    <main id="main">

        @if ($site_content && $site_content->show_about_us_section)
            @include('includes/about')
        @endif

        @if ($site_content && $site_content->show_testimonials_section)
            @include('includes/testimonials')
        @endif

        @if ($site_content && $site_content->show_partners_section)
            @include('includes/partners')
        @endif

        @if ($site_content && $site_content->show_content_1_section)
            @include('includes/dedicated_bookepper')
        @endif

        @include('includes/features')

        @if ($site_content && $site_content->show_pricing_section)
            @include('includes/pricing')
        @endif

        @if ($site_content && $site_content->show_team_section)
            @include('includes/team')
        @endif

        @if ($site_content && $site_content->show_qotd_section)
            @include('includes/qotd')
        @endif

        @if ($site_content && $site_content->show_recent_blogs_section)
            @include('includes/recent_blogs')
        @endif

        @if ($site_content && $site_content->show_faq_section)
            @include('includes/faq')
        @endif

        @if ($site_content && $site_content->show_content_2_section)
            @include('includes/join_us')
        @endif

        @include('includes/contact')

        @if ($site_content)
            @include('includes/newsletter')
        @endif

    </main>
@endsection

{{-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
  
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
  
            </ul>
  
            <ul class="navbar-nav ml-auto">
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>
  
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
  
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
  </nav> --}}

@section('js_after')
<script>
    $(document).ready(function() {
        $('.content li').each((index, element) => {
            let el = jQuery(element);
            el.append('<i class="ri-check-line"></i>')
        });
    });
</script>
@endsection