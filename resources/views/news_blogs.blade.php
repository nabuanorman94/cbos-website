@extends('layouts.app')

@section('title')
    {{ $page->title }} -
@endsection

@section('facebook_og')
    <meta name="description" content="{{ $page->seo_serp }}" />

    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:type" content="{{ $page->page_type }}" />
    <meta property="og:title" content="{{ $page->title }}" />
    <meta property="og:description" content="{{ $page->seo_serp }}" />
    <meta property="og:image" content="{{ $page->seo_image }}" />
@endsection

@section('content')
    
    <section id="hero-3" class="d-flex align-items-center" style="background-image: url({{ $page->banner_image }}); background-color: {{ $page->bg_color }}">
        <div class="container heading-content">
            <div class="row">
                
                <div class="col-12 order-1 order-lg-1 justify-content-center">
                    <div>
                        {!! $page->sub_title !!}
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3">
                            <form id="search-blog" action="{{ route('view-all-blogs', ['category' => $my_category]) }}" method="GET">
                                <div class="form-group">
                                    <label for="search" class="sr-only">Search</label>
                                    <input type="text" class="form-control" id="search" name="search" placeholder="Search Blogs">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    @if ($page->banner_image_mobile)
        <div class="hero-mobile">
            <img src="{{ $page->banner_image_mobile }}" alt="{{ $page->title }}" class="w-100">
        </div>
    @endif

    <main id="main">
        @foreach ($page_sections as $item)
        <section class="inner-page" style="{{ $item->div_styling }} background-image: url({{ $page->div_image_background }});">
            <div class="{{ $item->section_style }}">
                <div class="{{ $item->div_class }}" id="{{ $item->div_id }}">
                    @if ($item->contents)
                        @foreach ($item->contents as $content)
                            <div class="{{ $content->div_class }}" id="{{ $content->div_id }}" style="{{ $content->div_styling }}">
                                <div class="html-content">
                                    {!! $content->value !!} 
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if ($item->content_sections)
                        @foreach ($item->content_sections as $content_section)
                            <div class="{{ $content_section->div_class }}" id="{{ $content_section->div_id }}" style="{{ $content_section->div_styling }}">
                                <h3 class="html-heading">{{ $content_section->content_heading }}</h3>
                                <h5 class="html-sub-heading">{{ $content_section->content_sub_heading }}</h5>
                                <div class="html-content">
                                    {!! $content_section->content_value !!} 
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if ($item->feature_blocks)
                        @foreach ($item->feature_blocks as $feature_block)
                            <div class="{{ $feature_block->div_class }}" id="{{ $feature_block->div_id }}" style="{{ $feature_block->div_styling }}">
                                <div class="mb-2">
                                    <img src="{{ $feature_block->feat_image }}" alt="{{ $feature_block->feat_title }} Feature Image" class="w-100">
                                </div>
                                {{-- <h4 class="html-title mb-2">{{ $feature_block->feat_title }}</h4> --}}
                                <div class="html-content">
                                    {!! $feature_block->feat_content !!} 
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if ($item->feature_image_blocks)
                        @foreach ($item->feature_image_blocks as $feature_image_block)
                            <div class="{{ $feature_image_block->div_class }}" id="{{ $feature_image_block->div_id }}" style="{{ $feature_image_block->div_styling }}">
                                @if ($feature_image_block->feat_image)
                                    <div class="mb-2">
                                        <img src="{{ $feature_image_block->feat_image }}" alt="{{ $feature_image_block->feat_title }} Feature Image" class="w-100">
                                    </div>
                                @endif
                                <div class="html-content">
                                    {!! $feature_image_block->feat_title !!} 
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if ($item->feature_sections)
                        @foreach ($item->feature_sections as $feature_section)
                            <div class="{{ $feature_section->div_class }}" id="{{ $feature_section->div_id }}" style="{{ $feature_section->div_styling }}">
                                @if ($feature_section->feat_image)
                                    @if ($feature_section->image_placement == 'left')
                                        <div class="row mb-3 d-flex align-items-center" style="padding: 40px 0 20px">
                                            <div class="col-sm-12 col-lg-6 order-1 order-lg-1 hero-img">
                                                <img src="{{ $feature_section->feat_image }}" class="img-fluid" alt="{{ $feature_section->feat_title }}">
                                            </div>
                                            <div class="col-sm-12 col-lg-6 pt-5 pt-lg-0 order-2 order-lg-2 justify-content-center">
                                                <h3 class="html-heading">{{ $feature_section->feat_title }}</h3>
                                                <div class="html-content">{!! $feature_section->feat_content !!}</div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="row mb-3 d-flex align-items-center" style="padding: 40px 0 20px">
                                            <div class="col-sm-12 col-lg-6 order-1 order-lg-2 hero-img">
                                                <img src="{{ $feature_section->feat_image }}" class="img-fluid" alt="{{ $feature_section->feat_title }}">
                                            </div>
                                            <div class="col-sm-12 col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 justify-content-center">
                                                <h3 class="html-heading">{{ $feature_section->feat_title }}</h3>
                                                <div class="html-content">{!! $feature_section->feat_content !!}</div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </section>
        @endforeach
        
        <section id="services" class="services">
            <div class="container">
                <div class="d-flex align-items-center mb-3">
                    {{-- <p>  Search Results</p> --}}
                    @if (app('request')->input('search') != null)
                        <a href="{{ route('view-all-blogs', ['category' => $my_category]) }}" class="d-flex"><i class="ri-arrow-left-line my-auto mr-3" style="font-size: 25px;"></i></a>
                    @endif
                    <select name="blog_category" id="blog_category" class="form-control w-25">
                        @if (count($categories) > 0)
                            <option value="all" selected>Recent Blogs</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->slug }}" {{ ($my_category == $category->slug)? 'selected' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        @else 
                            <option value="" selected disabled>No Categories Found</option>
                        @endif
                    </select>
                    <p class="ml-auto my-auto"><i>{{ $results }} {{ ($results > 1)? 'Results' : 'Result' }}</i></p>
                </div>
                {{-- <div class="form-group row">
                    <div class="col-12 col-md-4">
                        <select name="blog_category" id="blog_category" class="form-control">
                            @if (count($categories) > 0)
                                <option value="all" selected>Recent Blogs</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->slug }}" {{ ($my_category == $category->slug)? 'selected' : '' }}>{{ $category->name }}</option>
                                @endforeach
                            @else 
                                <option value="" selected disabled>No Categories Found</option>
                            @endif
                        </select>
                    </div>
                </div> --}}
                <div class="row">
                    @if (count($items) > 0)
                        @foreach ($items as $my_item)
                            <div class="col-md-6 col-lg-4 d-flex align-items-stretch mb-3">
                                {{-- <div class="card" style="background-color: {{ $my_item->bg_color }};background-image: url('{{ $my_item->banner_image }}');background-position: center; background-repeat: no-repeat;">
                                    <div class="card-body">
                                        <h5 class="card-title"><a href="{{ route('view-blog', $my_item->slug) }}">{{ $my_item->title }}</a></h5>
                                        <p class="card-text">{{ $my_item->seo_serp }}</p>
                                        <small>
                                            <p class="text-muted mb-0">
                                                {{ $my_item->author }}
                                            </p>
                                            <i>
                                                <p>
                                                    {{ \Carbon\Carbon::createFromTimeStamp(strtotime($my_item->created_at))->diffForHumans() }}
                                                </p>
                                            </i>
                                        </small>
                                        <div class="d-flex">
                                            <div class="read-more"><a href="{{ route('view-blog', $my_item->slug) }}"><i class="icofont-arrow-right"></i> Read More</a></div>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="card">
                                    <p>
                                        <i style="min-width: 200px; border-bottom: 2px solid #7bbfce;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i>
                                    </p>
                                    <p class="mb-2">
                                        {{ \Carbon\Carbon::createFromTimeStamp(strtotime($my_item->created_at))->isoFormat('ll') }}
                                    </p>
                                    <h5><a href="{{ route('view-blog', $my_item->slug) }}" class="text-muted">{{ $my_item->title }}</a></h5>
                                    <p class="card-text">{{ $my_item->seo_serp_trim }}</p>
                                </div>
                            </div>
                        @endforeach
                    @else 
                        <div class="col-12">
                            <div class="alert alert-info mb-0" role="alert">
                                <p class="mb-0">No Results Found</p>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="mt-3">
                    {{ $items->render("pagination::bootstrap-4") }}
                </div>
            </div>
        </section>
        {{-- <section class="inner-page">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 order-1 order-lg-2 text-center">
                        <img src="{{ $page->image_2 }}" class="img-fluid w-100" alt="">
                    </div>
                    <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                        <div class="page-content text-justify">
                            {!! $page->content_1 !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-content text-justify">
                            {!! $page->content_2 !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @if ($pricings && count($pricings) > 0)
            @include('includes/pricing')
        @endif

        @if ($faqs && count($faqs) > 0)
            @include('includes/faq')
        @endif --}}

        @if ($faqs && count($faqs) > 0)
            @include('includes/faq')
        @endif

        @if ($site_content && $site_content->show_content_2_section)
            @include('includes/join_us')
        @endif

        @include('includes/contact')
    </main>
@endsection

@section('js_after')
    <script>
        $( function() {
            $( "#search" ).autocomplete({
                minLength: 3,
                source: function( request, response ) {
                    $.ajax({
                        url:"{{ route('autocomplete-blog')}} ",
                        type: 'POST',
                        dataType: "json",
                        data: {
                            _token: "{{ csrf_token() }}",
                            search: request.term
                        },
                        success: function( data ) {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) {
                    $('#search').val(ui.item.label);
                    $('#search-blog').submit();
                    return false;
                }
            });
        });

        $('select[name="blog_category"]').change(function(){
            let category = $(this).val();
            let url = "{{ route('view-all-blogs', ['category' => 'category_value']) }}";
            // window.location.href = url.replace('category_value', category);
            $('#search-blog').attr('action', url.replace('category_value', category));
            $('#search-blog').submit();
        })
    </script>
@endsection