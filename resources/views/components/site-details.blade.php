<div>
    <!-- No surplus words or unnecessary actions. - Marcus Aurelius -->
    <div class="card">
        <div class="card-header">
          Mange {{ $title }}
        </div>
        <div class="card-body">
          <h5 class="card-title">{{ $title }} Form</h5>
          
          <form>
            <div class="form-group">
              <label for="exampleFormControlInput1">Name</label>
              <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" value="{{ $site_details->name }}">
            </div>
            <div class="form-group">
              <label for="exampleFormControlTextarea1">Description</label>
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="8">{!! $site_details->description !!}</textarea>
            </div>
          </form>

        </div>
    </div>
</div>