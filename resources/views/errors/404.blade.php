@extends('layouts.app2')

@section('title')
    Error 404
@endsection

@section('content')
    <div id="app" class="main-content-boxed">
        <main id="main-container">
            <section id="hero" class="d-flex align-items-center">
              
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 order-1 order-lg-2 hero-img">
                    <img src="{{ asset('/media/images/404.gif') }}" class="img-fluid" alt="">
                    <!-- Credits to <a href="https://storyset.com/people">Illustration by Freepik Storyset</a> -->
                  </div>
                  <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center text-center">
                    <h1>ERROR 404</h1>
                    <h2 data-aos-delay="300">The page you are looking is not part of our website. Please use the right URL.</h2>
                    <div data-aos-delay="400">
                      <a href="/" class="btn-get-started scrollto">Go Back to Home</a>
                    </div>
                  </div>
                </div>
              </div>
            
            </section>
        </main>
    </div>
@endsection