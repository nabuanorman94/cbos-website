@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        <img src="{{ $details->logo }}" alt="{{ $details->name }}" style="width: 120px;">
    @endcomponent
@endslot

{!! $content !!}

<br>
Regards,
<br>
CBOS Team
{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

@slot('footer')
@component('mail::footer')
© {{ date('Y') }} <a href="{{ config('app.url') }}">{{ $details->name }}</a> . @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent
