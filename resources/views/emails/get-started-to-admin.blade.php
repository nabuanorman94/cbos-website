@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        <img src="{{ $details->logo }}" alt="{{ $details->name }}" style="width: 120px;">
    @endcomponent
@endslot

Hi Admin, {{ $user->first_name }} wants to avail the 2 filing months free promotion.
<br>
<p style="margin-bottom: 0;">Name: <b>{{ $user->first_name . ' ' . $user->last_name }}</b></p>
<p style="margin-bottom: 0;">Contact Number: <b>{{ $user->contact_no }}</b></p>
<p style="margin-bottom: 0;">Email: <b>{{ $user->email }}</b></p>
<p>Company: <b>{{ $user->company_name }}</b></p>

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent
