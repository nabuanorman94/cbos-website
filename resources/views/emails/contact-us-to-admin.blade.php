@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        <img src="{{ $details->logo }}" alt="{{ $details->name }}" style="width: 120px;">
    @endcomponent
@endslot

Hi Admin, {{ $user->name }} just contacted us.
<br>
<p style="margin-bottom: 0;">Name: <b>{{ $user->name }}</b></p>
<p style="margin-bottom: 0;">Email: <b>{{ $user->email }}</b></p>
<p>Message: <b>{{ $user->message }}</b></p>

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent
