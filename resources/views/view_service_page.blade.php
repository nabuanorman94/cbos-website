@extends('layouts.app')

@section('title')
    {{ $service->name }} -
@endsection

@section('facebook_og')
<meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{ $service->name }}" />
    <meta property="og:description" content="{{ $service->seo_serp }}" />
    <meta property="og:image" content="{{ $service->seo_image }}" />
@endsection

@section('content')
    
    <section id="hero-2" class="d-flex" style="background-image: url({{ $service->image_1 }}); background-color: {{ $service->icon }}">
        <div class="container heading-content">
            <div class="row mt-4 h-100">
                {{-- <div class="col-lg-6 order-1 order-lg-2 hero-img">
                    <img src="{{ $service->image_1 }}" class="img-fluid" alt="">
                </div> --}}
                <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                    <h2>{{ $service->name }}</h2>
                    <p>{{ $service->details }}</p>
                </div>
            </div>
        </div>

    </section>

    <main id="main">
        <section class="inner-page">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 order-1 order-lg-2 text-center">
                        <img src="{{ $service->image_2 }}" class="img-fluid w-100" alt="">
                    </div>
                    <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                        <div class="page-content text-justify">
                            {!! $service->content_1 !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-content text-justify">
                            {!! $service->content_2 !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @if ($pricings && count($pricings) > 0)
            @include('includes/pricing')
        @endif

        @if ($faqs && count($faqs) > 0)
            @include('includes/faq')
        @endif
    </main>
@endsection
