<div style="min-height: 43px;">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissable mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{-- <h4 class="alert-heading font-size-h4 font-w400">Success</h4> --}}
            <p class="mb-0">{{ $message }}</p>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-dismissable mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{-- <h4 class="alert-heading font-size-h4 font-w400">Error</h4> --}}
            <p class="mb-0">{{ $message }}</p>
        </div>
    @endif

    @if ($message = Session::get('info'))
        <div class="alert alert-info alert-dismissable mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{-- <h4 class="alert-heading font-size-h4 font-w400">Error</h4> --}}
            <p class="mb-0">{{ $message }}</p>
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>