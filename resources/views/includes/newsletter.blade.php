<section class="tone-1 lg-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-lg-8 offset-lg-2 justify-content-center text-center">
				@if ($site_content)
					<h3>{{ $site_content->newsletter_heading }}</h3>
					<p>{{ $site_content->newsletter_details }}</p>
                    <div class="input-group mb-1">
                        <input type="email" name="subs_email" class="form-control input-get-started" placeholder="Your Email Address" aria-label="Your Email Address" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-get-started-2" type="button" onclick="subscribe()" id="subscribe">Subscribe</button>
                        </div>
                    </div>
                    <small class="text-left" id="status"></small>
				@endif
			</div>
		</div>
	</div>
</section>

@section('js_after_newsletter')
    <script>
        function subscribe() {
            const email = $('input[name="subs_email"]').val();
            const token = $('input[name="_token"]').val();
            var url = '{{ route("subscribe-send") }}';

            if (validateEmail(email)) {
                $('#subscribe').attr({
                    disabled: true,
                    style: 'background-color: #0f456a;color: #ffffff;'
                }).html('Loading...');
                
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        _token: "{{ csrf_token() }}",
                        email: email
                    },
                    success: function(response)
                    {
                        $('input[name="subs_email').val('');
                        $('.php-email-form input[name="email"]').val('');
                        $('.php-email-form textarea').val('');
                        // $('#status').html('<i>'+response.success+'</i>')
                        $('#subscribe').html('Subscribed');
                    }
                });
            } else {
                $('#status').html('<i>Please provide a valid email address.</i>')
            }
            return false;
        }

        function validateEmail(email) {
            const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
    </script>
@endsection