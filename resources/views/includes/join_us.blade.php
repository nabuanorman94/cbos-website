<section class="main-tone lg-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-lg-8 offset-lg-2 justify-content-center text-center">
				@if ($site_content)
					<h3>{{ $site_content->content_2_heading }}</h3>
					<p>{{ $site_content->content_2_details }}</p>
					{{-- CTA 1 --}}
                    @if ($site_content->content_2_cta_name != null)
                        @switch($site_content->content_2_cta_type)
                            @case('link')
                                <a href="{{ $site_content->content_2_cta_link }}" class="btn-get-started">{{ $site_content->content_2_cta_name }}</a>
                                @break
                            @default
                                <button type="button" class="btn-get-started video-btn" data-toggle="modal" data-src="{{ $site_content->content_2_cta_link }}" data-target="#videoModal">
                                    {{ $site_content->content_2_cta_name }}
                                </button>
                        @endswitch
                    @endif
                    {{-- CTA 2 --}}
                    @if ($site_content->content_2_cta_name_2 != null)
                        @switch($site_content->content_2_cta_type_2)
                            @case('link')
                                <a href="{{ $site_content->content_2_cta_link_2 }}" class="btn-get-started-2">{{ $site_content->content_2_cta_name_2 }}</a>
                                @break
                            @default
                                <button type="button" class="btn-get-started-2 video-btn" data-toggle="modal" data-src="{{ $site_content->content_2_cta_link_2 }}" data-target="#videoModal">
                                    {{ $site_content->content_2_cta_name_2 }}
                                </button>
                        @endswitch
                    @endif
				@endif
			</div>
		</div>
	</div>
</section>