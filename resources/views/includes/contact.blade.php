<section id="contact" class="contact">
    <div class="container">
        @include('common.simple-alerts')
        <div class="section-title">
        <h2>Contact Us</h2>
        </div>

        @if ($details)
        <div class="row">

            <div class="col-lg-4 col-md-6">
                <div class="contact-about">
                    <a href={{ route('homepage') }}><img src="{{ $details->logo }}" alt="" class="w-50 mb-2"></a>
                    <div>
                    {!! $details->description !!}
                    </div>
                    <div class="social-links">
                        @if ($socials && count($socials) > 0)
                            @foreach ($socials as $item)
                            <a href="{{ $item->link }}" class="{{ str_replace('icofont-', '', $item->icon) }}"><i class="{{ $item->icon }}"></i></a>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
            <div class="info">

                <div>
                    <i class="ri-phone-line"></i>
                    <p>{{ $details->phone }}</p>
                </div>

                <div>
                    <i class="ri-mail-send-line"></i>
                    <p>{{ $details->email }}</p>
                </div>

                <div>
                    <i class="ri-calendar-todo-fill"></i>
                    <p>{{ $details->open_on }}</p>
                </div>

                <div>
                    <i class="ri-map-pin-line"></i>
                    <p>{{ $details->address }}</p>
                </div>

            </div>
        </div>
        @endif

        <div class="col-lg-5 col-md-12">
            
            <form action="{{ route('send-contact-us') }}" method="POST" role="form" class="php-email-form">
                @csrf
                <small class="text-left" id="contact-us-success"></small>
                <div class="form-group">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name *" required/>
                    <div class="validate"></div>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email *" required/>
                    <div class="validate"></div>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="message" rows="5" placeholder="Message *" required></textarea>
                    <div class="validate"></div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <div class="g-recaptcha" data-sitekey="{{ config('services.recaptcha.sitekey') }}"></div>
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback" style="display: block;">
                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" id="send">Send Message</button>
                </div>
            </form>
            
        </div>

        </div>

    </div>
</section>

@section('css_after')
    <link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after_contact')
    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script>
        // $(".php-email-form").submit(function(e) {
        //     var form = $(this);
        //     var url = form.attr('action');

        //     e.preventDefault();

        //     grecaptcha.ready(function() {
        //         grecaptcha.execute("{{ config('services.recaptcha.sitekey') }}", {action: 'submit'}).then(function(token) {
        //             $('#send').attr({
        //                 disabled: true,
        //                 style: 'background-color: #0f456a;'
        //             }).html('Sending...');

        //             $.ajax({
        //                 type: "POST",
        //                 url: url,
        //                 data: form.serialize(),
        //                 success: function(response)
        //                 {
        //                     $('.php-email-form input[name="name"]').val('');
        //                     $('.php-email-form input[name="email"]').val('');
        //                     $('.php-email-form textarea').val('');
        //                     $('#contact-us-success').html('<div class="alert alert-success alert-dismissable mb-3" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><p class="mb-0"><i class="ri-mail-check-line"></i> '+response.success+'</p></div>');
        //                     $('#send').html('Message Sent');
        //                 }
        //             });
        //         });
        //     });
        // });
    </script>
@endsection