@if ($site_content)
<div class="feature tone-1">
    <section>
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-sm-12 col-lg-7 order-1 order-lg-1 hero-img">
                    <img src="{{ $site_content->feature_1_image }}" class="img-fluid" alt="{{ $site_content->feature_1_heading }}">
                </div>
                <div class="col-sm-12 col-lg-5 pt-5 pt-lg-0 order-2 order-lg-2 justify-content-center">
                    <h3>{{ $site_content->feature_1_heading }}</h3>
                    <p>{{ $site_content->feature_1_details }}</p>
                    {{-- CTA 1 --}}
                    @if ($site_content->feature_1_cta_name != null)
                        @switch($site_content->feature_1_cta_type)
                            @case('link')
                                <a href="{{ $site_content->feature_1_cta_link }}" class="btn-get-started">{{ $site_content->feature_1_cta_name }}</a>
                                @break
                            @default
                                <button type="button" class="btn-get-started video-btn" data-toggle="modal" data-src="{{ $site_content->feature_1_cta_link }}" data-target="#videoModal">
                                    {{ $site_content->feature_1_cta_name }}
                                </button>
                        @endswitch
                    @endif
                    {{-- CTA 2 --}}
                    @if ($site_content->feature_1_cta_name_2 != null)
                        @switch($site_content->feature_1_cta_type_2)
                            @case('link')
                                <a href="{{ $site_content->feature_1_cta_link_2 }}" class="btn-get-started-2">{{ $site_content->feature_1_cta_name_2 }}</a>
                                @break
                            @default
                                <button type="button" class="btn-get-started-2 video-btn" data-toggle="modal" data-src="{{ $site_content->feature_1_cta_link_2 }}" data-target="#videoModal">
                                    {{ $site_content->feature_1_cta_name_2 }}
                                </button>
                        @endswitch
                    @endif
                </div>
            </div>
        </div>
    </section>
    
    <section>
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-sm-12 col-lg-7 order-1 order-lg-2 hero-img">
                    <img src="{{ $site_content->feature_2_image }}" class="img-fluid" alt="{{ $site_content->feature_2_heading }}">
                </div>
                <div class="col-sm-12 col-lg-5 pt-5 pt-lg-0 order-2 order-lg-1 justify-content-center">
                    <h3>{{ $site_content->feature_2_heading }}</h3>
                    <p>{{ $site_content->feature_2_details }}</p>
                    {{-- CTA 1 --}}
                    @if ($site_content->feature_2_cta_name != null)
                        @switch($site_content->feature_2_cta_type)
                            @case('link')
                                <a href="{{ $site_content->feature_2_cta_link }}" class="btn-get-started">{{ $site_content->feature_2_cta_name }}</a>
                                @break
                            @default
                                <button type="button" class="btn-get-started video-btn" data-toggle="modal" data-src="{{ $site_content->feature_2_cta_link }}" data-target="#videoModal">
                                    {{ $site_content->feature_2_cta_name }}
                                </button>
                        @endswitch
                    @endif
                    {{-- CTA 2 --}}
                    @if ($site_content->feature_2_cta_name_2 != null)
                        @switch($site_content->feature_2_cta_type_2)
                            @case('link')
                                <a href="{{ $site_content->feature_2_cta_link_2 }}" class="btn-get-started-2">{{ $site_content->feature_2_cta_name_2 }}</a>
                                @break
                            @default
                                <button type="button" class="btn-get-started-2 video-btn" data-toggle="modal" data-src="{{ $site_content->feature_2_cta_link_2 }}" data-target="#videoModal">
                                    {{ $site_content->feature_2_cta_name_2 }}
                                </button>
                        @endswitch
                    @endif
                </div>
            </div>
        </div>
    </section>
    
    <section>
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-sm-12 col-lg-7 order-1 order-lg-1 hero-img">
                    <img src="{{ $site_content->feature_3_image }}" class="img-fluid" alt="{{ $site_content->feature_3_heading }}">
                </div>
                <div class="col-sm-12 col-lg-5 pt-5 pt-lg-0 order-2 order-lg-2 justify-content-center">
                    <h3>{{ $site_content->feature_3_heading }}</h3>
                    <p>{{ $site_content->feature_3_details }}</p>
                    {{-- CTA 1 --}}
                    @if ($site_content->feature_3_cta_name != null)
                        @switch($site_content->feature_3_cta_type)
                            @case('link')
                                <a href="{{ $site_content->feature_3_cta_link }}" class="btn-get-started">{{ $site_content->feature_3_cta_name }}</a>
                                @break
                            @default
                                <button type="button" class="btn-get-started video-btn" data-toggle="modal" data-src="{{ $site_content->feature_3_cta_link }}" data-target="#videoModal">
                                    {{ $site_content->feature_3_cta_name }}
                                </button>
                        @endswitch
                    @endif
                    {{-- CTA 2 --}}
                    @if ($site_content->feature_3_cta_name_2 != null)
                        @switch($site_content->feature_3_cta_type_2)
                            @case('link')
                                <a href="{{ $site_content->feature_3_cta_link_2 }}" class="btn-get-started-2">{{ $site_content->feature_3_cta_name_2 }}</a>
                                @break
                            @default
                                <button type="button" class="btn-get-started-2 video-btn" data-toggle="modal" data-src="{{ $site_content->feature_3_cta_link_2 }}" data-target="#videoModal">
                                    {{ $site_content->feature_3_cta_name_2 }}
                                </button>
                        @endswitch
                    @endif
                </div>
            </div>
        </div>
    </section>
</div>
@endif