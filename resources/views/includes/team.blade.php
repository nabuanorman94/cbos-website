@if ($teams && count($teams) > 0)
<section id="team" class="team section-bg">
    <div class="container">
  
      <div class="section-title" data-aos="fade-up">
        <h2>Meet Our Team</h2>
        <p>Our Team of Professionals</p>
      </div>
  
      <div class="row">

        @foreach ($teams as $item)
            <div class="col-6 col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="member" data-aos="fade-up" data-aos-delay="100">
                <div class="member-img">
                    <img src="{{ $item->image }}" class="img-fluid" alt="">
                    {{-- <div class="social">
                    <a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>
                    <a href=""><i class="icofont-linkedin"></i></a>
                    </div> --}}
                </div>
                <div class="member-info">
                    <h4>{{ $item->name }}</h4>
                    <span>{{ $item->position }}</span>
                </div>
                </div>
            </div>
        @endforeach

      </div>
    </div>
  </section>
@endif