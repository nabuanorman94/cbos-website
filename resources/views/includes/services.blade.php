<section id="services" class="services">
  <div class="container">

    <div class="section-title" data-aos="fade-up">
      <h2>Services</h2>
      <p>Business Essentials</p>
    </div>

    @if ($services)
        <div class="row">
            @foreach ($services as $item)
                @if ($item->service_type == 'ph-registrations')
                    <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="zoom-in" data-aos-delay="100">
                        <div class="icon-box">
                            <div class="icon"><i class="{{ $item->icon }}"></i></div>
                            <h4 class="title"><a href="{{ route('view-service-page', $item->slug) }}">{{ $item->name }}</a></h4>
                            <p class="description">{{ $item->details }}</p>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

        <div class="section-title mt-3" data-aos="fade-up">
        <p>Bookkeeping & Accounting</p>
        </div>

        <div class="row">
            @foreach ($services as $item)
                @if ($item->service_type == 'bookkeeping-accounting')
                    <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="zoom-in" data-aos-delay="100">
                        <div class="icon-box">
                            <div class="icon"><i class="{{ $item->icon }}"></i></div>
                            <h4 class="title"><a href="{{ route('view-service-page', $item->slug) }}">{{ $item->name }}</a></h4>
                            <p class="description">{{ $item->details }}</p>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    @endif

  </div>
</section>