<section class="clients bg-tone lg-section">
  <div class="container">
    @if ($site_content)
        <h4>{{ $site_content->partners_heading }}</h4>
    @endif
    <div class="row mt-4">
      @if ($partners)
          @foreach ($partners as $item)
            <div class="col-md-3 col-6">
              <img src="{{ $item->logo }}" class="img-fluid" alt="{{ $item->name }}" data-aos="zoom-in">
            </div>
          @endforeach
      @endif
    </div>

  </div>
</section>