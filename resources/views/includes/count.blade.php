<section id="counts" class="counts">
  <div class="container">

    @if ($details)
      <div class="row">
        <div class="col-xl-7 d-flex align-items-stretch pt-4 pt-xl-0" data-aos="fade-right" data-aos-delay="100">
          <div class="content d-flex flex-column justify-content-center">
            <div class="row">
              <div class="col-md-6 d-md-flex align-items-md-stretch">
                <div class="count-box">
                  <i class="{{ $about_us->count_1_icon }}"></i>
                  <span data-toggle="counter-up">{{ $about_us->count_1_number }}</span>
                  <p>{{ $about_us->count_1_details }}</p>
                </div>
              </div>

              <div class="col-md-6 d-md-flex align-items-md-stretch">
                <div class="count-box">
                  <i class="{{ $about_us->count_2_icon }}"></i>
                  <span data-toggle="counter-up">{{ $about_us->count_2_number }}</span>
                  <p>{{ $about_us->count_2_details }}</p>
                </div>
              </div>

              <div class="col-md-6 d-md-flex align-items-md-stretch">
                <div class="count-box">
                  <i class="{{ $about_us->count_3_icon }}"></i>
                  <span data-toggle="counter-up">{{ $about_us->count_3_number }}</span>
                  <p>{{ $about_us->count_3_details }}</p>
                </div>
              </div>

              <div class="col-md-6 d-md-flex align-items-md-stretch">
                <div class="count-box">
                  <i class="{{ $about_us->count_4_icon }}"></i>
                  <span data-toggle="counter-up">{{ $about_us->count_4_number }}</span>
                  <p>{{ $about_us->count_4_details }}</p>
                </div>
              </div>
            </div>
          </div><!-- End .content-->
        </div>
        <div class="image col-xl-5 d-flex align-items-stretch justify-content-center justify-content-xl-start" data-aos="fade-left" data-aos-delay="150">
          <img src="{{ $about_us->image }}" alt="" class="img-fluid">
        </div>
      </div>
    @endif

  </div>
</section>