{{-- <section id="hero" class="d-flex align-items-center"> --}}
@if ($banners && count($banners) > 0)
<div class="{{ (count($banners) > 1)? 'owl-carousel banner-carousel' : ''}}">
    @foreach ($banners as $item)
    <section id="hero" class="d-flex align-items-center" style="background-image: url({{ $item->image }}); background-color: {{ $item->bg_color }}">
        <div class="container heading-content">
            <div class="row">
                
                <div class="col-lg-7 {{ ($item->heading_position == 'right')? 'offset-lg-5' : '' }} order-1 order-lg-1 justify-content-center">
                    {{-- <div>
                        {!! $item->title !!}
                    </div> --}}
                    <div>
                        {!! $item->sub_title !!}
                    </div>

                    {{-- CTA 1 --}}
                    @if ($item->cta_name != null)
                        @switch($item->cta_type)
                            @case('link')
                                <a href="{{ $item->cta_link }}" class="btn-get-started">{{ $item->cta_name }}</a>
                                @break
                            @default
                                <button type="button" class="btn-get-started video-btn" data-toggle="modal" data-src="{{ $item->cta_link }}" data-target="#videoModal">
                                    {{ $item->cta_name }}
                                </button>
                        @endswitch
                    @endif
                    {{-- CTA 2 --}}
                    @if ($item->cta_name_2 != null)
                        @switch($item->cta_type_2)
                            @case('link')
                                <a href="{{ $item->cta_link_2 }}" class="btn-get-started-2">{{ $item->cta_name_2 }}</a>
                                @break
                            @default
                                <button type="button" class="btn-get-started-2 video-btn" data-toggle="modal" data-src="{{ $item->cta_link_2 }}" data-target="#videoModal">
                                    {{ $item->cta_name_2 }}
                                </button>
                        @endswitch
                    @endif
                    
                </div>
                
            </div>
        </div>
    </section>
    <div class="hero-mobile">
        <img src="{{ $item->image_mobile }}" alt="{{ $item->name }}" class="w-100">
    </div>
    @endforeach
</div>
@endif

{{-- </section> --}}