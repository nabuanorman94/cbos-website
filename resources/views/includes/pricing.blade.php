@if ($pricings && count($pricings) > 0)
    <section id="pricing" class="pricing section-bg">
        <div class="container" data-aos="fade-up">
    
        <div class="section-title">
            <h2>Pricing</h2>
            <p>Accounting & Taxation</p>
        </div>
    
        <div class="row">
            @foreach ($pricings as $item)
            @if ($item->pricing_type == 'accounting_taxation')
                <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
                    <div class="box">
                        <h3>{{ $item->package_name }}</h3>
                        <h4><sup>Php</sup>{{ $item->price_per_month }}<span> / month</span></h4>
                        <div>
                            {!! $item->inclusions !!}
                        </div>
                        <div class="spacer"></div>
                        <div class="btn-wrap">
                        <a href="#" class="btn-buy">Get Started</a>
                        </div>
                    </div>
                </div>
            @endif
            @endforeach
            
    
            {{-- <div class="col-lg-3 col-md-6 mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="200">
            <div class="box featured">
                <h3>Business</h3>
                <h4><sup>$</sup>19<span> / month</span></h4>
                <ul>
                <li>Aida dere</li>
                <li>Nec feugiat nisl</li>
                <li>Nulla at volutpat dola</li>
                <li>Pharetra massa</li>
                <li class="na">Massa ultricies mi</li>
                </ul>
                <div class="btn-wrap">
                <a href="#" class="btn-buy">Get Started</a>
                </div>
            </div>
            </div>
    
            <div class="col-lg-3 col-md-6 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300">
            <div class="box">
                <h3>Developer</h3>
                <h4><sup>$</sup>29<span> / month</span></h4>
                <ul>
                <li>Aida dere</li>
                <li>Nec feugiat nisl</li>
                <li>Nulla at volutpat dola</li>
                <li>Pharetra massa</li>
                <li>Massa ultricies mi</li>
                </ul>
                <div class="btn-wrap">
                <a href="#" class="btn-buy">Get Started</a>
                </div>
            </div>
            </div>
    
            <div class="col-lg-3 col-md-6 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="400">
            <div class="box">
                <span class="advanced">Advanced</span>
                <h3>Ultimate</h3>
                <h4><sup>$</sup>49<span> / month</span></h4>
                <ul>
                <li>Aida dere</li>
                <li>Nec feugiat nisl</li>
                <li>Nulla at volutpat dola</li>
                <li>Pharetra massa</li>
                <li>Massa ultricies mi</li>
                </ul>
                <div class="btn-wrap">
                <a href="#" class="btn-buy">Get Started</a>
                </div>
            </div>
            </div> --}}
        
        </div>

        <div class="section-title mt-5" data-aos="fade-up">
            <p>Bookkeeping & Taxation</p>
        </div>

        <div class="row">
            @foreach ($pricings as $item)
            @if ($item->pricing_type == 'bookkeeping_taxation')
                <div class="col-lg-4 col-md-6 row-same-height mb-4" data-aos="fade-up" data-aos-delay="100">
                    <div class="box h-100">
                        <h3>{{ $item->package_name }}</h3>
                        <div>
                            {!! $item->inclusions !!}
                        </div>
                        <div class="spacer"></div>
                        <div class="btn-wrap">
                        <a href="#" class="btn-buy">Get Started</a>
                        </div>
                    </div>
                </div>
            @endif
            @endforeach
        </div>
    
        <div class="text-center mt-1">
            <button class="btn btn-primary"><i class="ri-calculator-line"></i> Calculate Pricing</button>
            <button class="btn btn-warning"><i class="ri-customer-service-2-line"></i> Talk To Our Sales Team</button>
        </div>
    
        </div>
    </section>
@endif