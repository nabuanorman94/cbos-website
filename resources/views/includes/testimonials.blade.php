@if ($testimonials && count($testimonials) > 0 && $site_content)
<section id="testimonials" class="testimonials lg-section">
  <div class="container">
    <div class="section-title">
      <h2>What our clients say about us</h2>
      {{-- <h2>{{ $site_content->partners_heading }}</h2> --}}
    </div>

    <div class="owl-carousel testimonials-carousel">
        @foreach ($testimonials as $item)
            <div class="testimonial-wrap">
                <div class="testimonial-item">
                    <img src="{{ $item->image }}" class="testimonial-img" alt="">
                    <h3>{{ $item->name }}</h3>
                    <h4>{{ $item->position }}</h4>
                    <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        {{ $item->content }}
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                </div>
            </div>
        @endforeach
    </div>
  </div>
</section>
@endif