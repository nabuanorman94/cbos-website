@if ($blogs && count($blogs) > 0)
<section id="recent_blogs" class="lg-section">
    <div class="container">
  
        <div class="section-title">
            <h2>Recent Blogs</h2>
        </div>
    
        <div class="row d-flex mt-4">
            @if (isset($blogs))
                @foreach ($blogs as $blog)
                <div class="col-md-6 col-lg-4 d-flex align-items-stretch mb-3 mx-auto">
                    {{-- <div class="card" style="background-color: {{ $blog->bg_color }};background-image: url('{{ $blog->banner_image }}');background-position: center; background-repeat: no-repeat;">
                        <div class="card-body">
                            <h5 class="card-title"><a href="{{ route('view-blog', $blog->slug) }}">{{ $blog->title }}</a></h5>
                            <p class="card-text">{{ $blog->seo_serp }}</p>
                            <small>
                                <p class="text-muted mb-0">
                                    {{ $blog->author }}
                                </p>
                                <i>
                                    <p>
                                        {{ \Carbon\Carbon::createFromTimeStamp(strtotime($blog->created_at))->diffForHumans() }}
                                    </p>
                                </i>
                            </small>
                            <div class="d-flex">
                                <div class="read-more"><a href="{{ route('view-blog', $blog->slug) }}"><i class="icofont-arrow-right"></i> Read More</a></div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="card">
                        <p>
                            <i style="min-width: 200px; border-bottom: 2px solid #7bbfce;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i>
                        </p>
                        <p class="mb-2">
                            {{ \Carbon\Carbon::createFromTimeStamp(strtotime($blog->created_at))->isoFormat('ll') }}
                        </p>
                        <h5><a href="{{ route('view-blog', $blog->slug) }}" class="text-muted">{{ $blog->title }}</a></h5>
                        <p class="card-text">{{ $blog->seo_serp_trim }}</p>
                    </div>
                </div>
                @endforeach
            @endif
        </div>

        <div class="text-center mt-3">
            <a href="{{ route('view-all-blogs', ['category' => 'all']) }}">View All Blogs</a>
        </div>
    </div>
  
    </div>
  </section>
@endif