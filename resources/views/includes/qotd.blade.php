@if ($qotd)
<section id="qotd" class="bg-tone">
    <div class="container">
  
        <div class="section-title">
            <h2>Question of the Day</h2>
            <p>{{ $qotd->title }}</p>
        </div>
      
        <div class="html-content">
            {!! $qotd->description !!}
        </div>

        {{-- <div class="text-center mt-3">
            <a href="{{ route('view-all-blogs', ['category' => 'all']) }}">View More</a>
        </div> --}}
  
    </div>
  </section>
@endif