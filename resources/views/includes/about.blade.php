<section class="bg-tone">
  <div class="container">

    <div class="section-title">
      <h2>About Us</h2>
    </div>

    @if ($about_us)
      <div class="row content">
        <div class="col-lg-6">
          {!! $about_us->section_1_details !!}
        </div>
        <div class="col-lg-6 pt-4 pt-lg-0">
          {!! $about_us->section_2_details !!}
          <a href="{{ $about_us->cta_link }}" class="btn-get-started">{{ $about_us->cta_name }}</a>
        </div>
      </div>
    @endif
      <hr>
  </div>
</section>