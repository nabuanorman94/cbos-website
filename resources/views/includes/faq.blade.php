@if ($faqs && count($faqs) > 0)
<section id="faq" class="faq bg-tone lg-section">
    <div class="container">
  
      <div class="section-title">
        <h2>Frequently Asked Questions</h2>
      </div>
  
      <div class="faq-list">
        <ul>
            @foreach ($faqs as $item)
                <li>
                    <a data-toggle="collapse" class="{{ ($loop->index == 0)? 'collapsed' : 'collapse' }}" href="#faq-list-{{ $item->id }}" aria-expanded="{{ ($loop->index == 0)? 'true' : 'false' }}">
                      <h5><b>{{ $item->question }}</b></h5> 
                      <i class="bx bx-chevron-down icon-show"></i>
                      <i class="bx bx-chevron-up icon-close"></i>
                    </a>
                    <div id="faq-list-{{ $item->id }}" class="collapse {{ ($loop->index == 0)? 'show' : '' }}" data-parent=".faq-list">
                      <div class="html-content">
                          {!! $item->answer !!}
                      </div>
                    </div>
                </li>
            @endforeach
        </ul>
      </div>
  
    </div>
  </section>
@endif