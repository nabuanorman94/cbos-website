<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageFeatureSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_feature_sections', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('page_section_id');
            $table->tinyInteger('arrangement');
            $table->string('div_styling')->nullable();
            $table->string('div_class')->nullable();
            $table->string('div_id')->nullable();
            $table->string('value')->nullable();
            $table->string('feat_image')->nullable();
            $table->string('feat_title')->nullable();
            $table->string('feat_content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_feature_sections');
    }
}
