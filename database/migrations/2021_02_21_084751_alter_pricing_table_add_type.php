<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPricingTableAddType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_pricings', function (Blueprint $table) {
            $table->string('price_one_time')->nullable()->after('package_name');
            $table->string('pricing_type')->after('is_best_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_pricings', function (Blueprint $table) {
            //
        });
    }
}
