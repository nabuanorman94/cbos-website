<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSitePageAddBanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_pages', function (Blueprint $table) {
            $table->text('sub_title')->after('show')->nullable();
            $table->string('banner_image')->after('sub_title')->nullable();
            $table->string('banner_image_mobile')->after('banner_image')->nullable();
            $table->string('heading_position')->after('banner_image_mobile')->nullable();
            $table->string('bg_color')->after('heading_position')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_pages', function (Blueprint $table) {
            //
        });
    }
}
