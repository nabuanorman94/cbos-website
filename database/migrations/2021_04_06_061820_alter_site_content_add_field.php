<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSiteContentAddField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_contents', function (Blueprint $table) {
            $table->boolean('show_qotd_section')->after('show_team_section');
            $table->boolean('show_recent_blogs_section')->after('show_qotd_section');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_contents', function (Blueprint $table) {
            //
        });
    }
}
