<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageReviewSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_review_sections', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('page_section_id');
            $table->tinyInteger('arrangement');
            $table->string('div_styling')->nullable();
            $table->string('div_class')->nullable();
            $table->string('div_id')->nullable();
            $table->string('image_src')->nullable();
            $table->string('image_alt')->nullable();
            $table->string('rev_name')->nullable();
            $table->string('rev_position')->nullable();
            $table->string('rev_content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_review_sections');
    }
}
