<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSiteDetailsAddLogo2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_details', function (Blueprint $table) {
            $table->string('logo_2')->after('logo');
            $table->string('logo_3')->after('logo_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_details', function (Blueprint $table) {
            //
        });
    }
}
