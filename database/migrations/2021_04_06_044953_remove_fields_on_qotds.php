<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveFieldsOnQotds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qotds', function (Blueprint $table) {
            $table->dropColumn('sub_title');
            $table->dropColumn('slug');
            $table->dropColumn('banner_image');
            $table->dropColumn('banner_image_mobile');
            $table->dropColumn('heading_position');
            $table->dropColumn('bg_color');
            $table->dropColumn('seo_serp');
            $table->dropColumn('seo_keywords');
            $table->dropColumn('seo_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qotds', function (Blueprint $table) {
            //
        });
    }
}
