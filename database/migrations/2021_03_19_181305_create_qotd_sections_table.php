<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQotdSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qotd_sections', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('qotd_id');
            $table->tinyInteger('arrangement');
            $table->text('div_styling')->nullable();
            $table->string('div_class')->nullable();
            $table->string('div_id')->nullable();
            $table->text('div_image_background')->nullable();
            $table->text('section_style')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qotd_sections');
    }
}
