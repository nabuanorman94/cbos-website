<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSiteBannerAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_banners', function (Blueprint $table) {
            $table->string('cta_type')->nullable()->after('show');
            $table->string('cta_name_2')->nullable()->after('cta_type');
            $table->string('cta_link_2')->nullable()->after('cta_name_2');
            $table->string('cta_type_2')->nullable()->after('cta_link_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_banners', function (Blueprint $table) {
            //
        });
    }
}
