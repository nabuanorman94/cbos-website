<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('video_category_id');
            $table->string('title');
            $table->text('sub_title')->nullable();
            $table->string('slug')->nullable();
            $table->text('banner_image')->nullable();
            $table->text('banner_image_mobile')->nullable();
            $table->string('heading_position')->nullable();
            $table->string('bg_color')->nullable();
            $table->text('seo_serp')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->text('seo_image')->nullable();
            $table->boolean('show');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
