<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogFeatureSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_feature_sections', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('blog_section_id');
            $table->tinyInteger('arrangement');
            $table->text('div_styling')->nullable();
            $table->string('div_class')->nullable();
            $table->string('div_id')->nullable();
            $table->string('value')->nullable();
            $table->text('feat_image')->nullable();
            $table->string('feat_title')->nullable();
            $table->text('feat_content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_feature_sections');
    }
}
