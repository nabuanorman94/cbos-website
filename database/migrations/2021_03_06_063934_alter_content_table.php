<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_contents', function (Blueprint $table) {
            $table->boolean('show_about_us_section')->after('partners_heading');
            $table->boolean('show_testimonials_section')->after('show_about_us_section');
            $table->boolean('show_partners_section')->after('show_testimonials_section');
            $table->boolean('show_content_1_section')->after('show_partners_section');
            $table->boolean('show_content_2_section')->after('show_content_1_section');
            $table->boolean('show_faq_section')->after('show_content_2_section');
            $table->boolean('show_pricing_section')->after('show_faq_section');
            $table->boolean('show_team_section')->after('show_pricing_section');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_contents', function (Blueprint $table) {
            //
        });
    }
}
