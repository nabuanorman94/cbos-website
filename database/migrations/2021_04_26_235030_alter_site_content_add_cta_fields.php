<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSiteContentAddCtaFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_contents', function (Blueprint $table) {
            $table->string('content_1_cta_type')->nullable()->after('feature_3_image');
            $table->string('content_1_cta_name_2')->nullable()->after('content_1_cta_type');
            $table->string('content_1_cta_link_2')->nullable()->after('content_1_cta_name_2');
            $table->string('content_1_cta_type_2')->nullable()->after('content_1_cta_link_2');
            $table->string('content_2_cta_type')->nullable()->after('content_1_cta_type_2');
            $table->string('content_2_cta_name_2')->nullable()->after('content_2_cta_type');
            $table->string('content_2_cta_link_2')->nullable()->after('content_2_cta_name_2');
            $table->string('content_2_cta_type_2')->nullable()->after('content_2_cta_link_2');

            $table->string('feature_1_cta_type')->nullable()->after('content_2_cta_type_2');
            $table->string('feature_1_cta_name_2')->nullable()->after('feature_1_cta_type');
            $table->string('feature_1_cta_link_2')->nullable()->after('feature_1_cta_name_2');
            $table->string('feature_1_cta_type_2')->nullable()->after('feature_1_cta_link_2');

            $table->string('feature_2_cta_type')->nullable()->after('feature_1_cta_type_2');
            $table->string('feature_2_cta_name_2')->nullable()->after('feature_2_cta_type');
            $table->string('feature_2_cta_link_2')->nullable()->after('feature_2_cta_name_2');
            $table->string('feature_2_cta_type_2')->nullable()->after('feature_2_cta_link_2');

            $table->string('feature_3_cta_type')->nullable()->after('feature_2_cta_type_2');
            $table->string('feature_3_cta_name_2')->nullable()->after('feature_3_cta_type');
            $table->string('feature_3_cta_link_2')->nullable()->after('feature_3_cta_name_2');
            $table->string('feature_3_cta_type_2')->nullable()->after('feature_3_cta_link_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_contents', function (Blueprint $table) {
            //
        });
    }
}
