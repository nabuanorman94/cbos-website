<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterServicesTableAddContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_services', function (Blueprint $table) {
            $table->text('content_1')->after('service_type');
            $table->text('content_2')->after('content_1')->nullable();
            $table->string('image_1')->after('content_2')->nullable();
            $table->string('image_2')->after('image_1')->nullable();
            $table->string('seo_serp')->after('image_2');
            $table->string('seo_keywords')->after('seo_serp');
            $table->string('seo_image')->after('seo_keywords');
            $table->string('slug')->after('seo_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_services', function (Blueprint $table) {
            //
        });
    }
}
