<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteAboutUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_about_us', function (Blueprint $table) {
            $table->id();
            $table->text('section_1_details');
            $table->text('section_1_list');
            $table->text('section_2_details');
            $table->string('cta_name');
            $table->string('cta_link');
            $table->string('count_1_number');
            $table->string('count_1_icon');
            $table->text('count_1_details');
            $table->string('count_2_number');
            $table->string('count_2_icon');
            $table->text('count_2_details');
            $table->string('count_3_number');
            $table->string('count_3_icon');
            $table->text('count_3_details');
            $table->string('count_4_number');
            $table->string('count_4_icon');
            $table->text('count_4_details');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_about_us');
    }
}
