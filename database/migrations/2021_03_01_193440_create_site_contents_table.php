<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_contents', function (Blueprint $table) {
            $table->id();
            $table->string('partners_heading')->nullable();
            $table->string('content_1_heading')->nullable();
            $table->text('content_1_details')->nullable();
            $table->string('content_1_cta_name')->nullable();
            $table->string('content_1_cta_link')->nullable();
            $table->string('content_2_heading')->nullable();
            $table->text('content_2_details')->nullable();
            $table->string('content_2_cta_name')->nullable();
            $table->string('content_2_cta_link')->nullable();
            $table->string('feature_1_heading')->nullable();
            $table->text('feature_1_details')->nullable();
            $table->string('feature_1_cta_name')->nullable();
            $table->string('feature_1_cta_link')->nullable();
            $table->string('feature_1_image')->nullable();
            $table->string('feature_2_heading')->nullable();
            $table->text('feature_2_details')->nullable();
            $table->string('feature_2_cta_name')->nullable();
            $table->string('feature_2_cta_link')->nullable();
            $table->string('feature_2_image')->nullable();
            $table->string('feature_3_heading')->nullable();
            $table->text('feature_3_details')->nullable();
            $table->string('feature_3_cta_name')->nullable();
            $table->string('feature_3_cta_link')->nullable();
            $table->string('feature_3_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_contents');
    }
}
