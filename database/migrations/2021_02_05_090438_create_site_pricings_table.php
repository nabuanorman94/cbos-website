<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitePricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_pricings', function (Blueprint $table) {
            $table->id();
            $table->string('package_name');
            $table->string('price_per_month')->nullable();
            $table->string('price_per_year')->nullable();
            $table->text('inclusions');
            $table->string('is_best_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_pricings');
    }
}
