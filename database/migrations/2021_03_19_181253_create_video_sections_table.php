<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_sections', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('video_id');
            $table->tinyInteger('arrangement');
            $table->text('div_styling')->nullable();
            $table->string('div_class')->nullable();
            $table->string('div_id')->nullable();
            $table->text('div_image_background')->nullable();
            $table->text('section_style')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_sections');
    }
}
