<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Manage your roles here (1 or more)
        DB::table('roles')->insert([
            ['name' => 'Super Admin', 'slug' => 'super-admin', 'description' => 'Super admin account'],
            ['name' => 'Admin', 'slug' => 'admin', 'description' => 'Admin/Manager account'],
            ['name' => 'User', 'slug' => 'user', 'description' => 'User that can input data to the system']
        ]);
    }
}
