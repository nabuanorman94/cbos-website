<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function() {
//     return view('updating');
// })->name('updating');

Route::get('/', 'WebsiteController@index')->name('homepage');
Route::get('/websiteundermaintenance', 'WebsiteController@updating');

Auth::routes([
    'register' => false
]);

Route::get('/home', 'HomeController@index')->name('home');

// Contact Us
Route::post('/contact-us', 'WebsiteController@contactUsSend')->name('send-contact-us');

// Get Started
Route::get('/get-started', 'WebsiteController@getStarted')->name('get-started');
Route::post('/get-started', 'WebsiteController@getStartedSend')->name('get-started-send');

// Schedule a call
Route::get('/schedule-a-call', 'WebsiteController@scheduleACall')->name('schedule-a-call');
Route::post('/schedule-a-call', 'WebsiteController@scheduleACallSubmit')->name('schedule-a-call-submit');

// Subscribe
Route::post('/subscribe', 'WebsiteController@subscribeSend')->name('subscribe-send');

// All CRUD Model Routes
Route::group(['prefix' => 'admin'], function() {
    Route::get('/', 'AdminController@index')->name('admin-home');

    Route::group(['prefix' => 'resources'], function() {
        Route::get('/blogs/{category}', 'BlogController@showBlogsPage')->name('admin-resources-blogs');
        Route::get('/blog/add', 'BlogController@addBlogPage')->name('admin-resources-blog-add');
        Route::get('/blog/edit/{slug}', 'BlogController@editBlogPage')->name('admin-resources-blog-edit');
        Route::post('/blog/builder', 'BlogController@addOrEditBlog')->name('admin-resources-blog-add-submit');
        Route::get('/blog/builder/{slug}', 'BlogController@blogBuilder')->name('admin-resources-blog-builder');
        Route::get('/manage-blog-categories', 'BlogController@showBlogsCategoriesPage')->name('admin-resources-manage-blog-categories');

        // Blog Builder
        Route::group(['prefix' => 'blog-builder'], function() {
            Route::post('create', 'BlogController@createData')->name('bb-create-data');
            Route::post('update', 'BlogController@updateData')->name('bb-update-data');
            Route::post('remove', 'BlogController@removeData')->name('bb-remove-data');
            Route::get('preview/{slug}', 'BlogController@previewBlog')->name('bb-preview-page');

            // Section Components
            Route::get('content/{section_id}/{content_id}', 'BlogController@sectionAddContent')->name('bb-content');
            Route::get('feature-block/{section_id}/{feature_block_id}', 'BlogController@sectionAddFeatureBlock')->name('bb-feature-block');
            Route::get('feature-section/{section_id}/{feature_section_id}', 'BlogController@sectionAddFeatureSection')->name('bb-feature-section');

            // Duplicate Page, Section and Component
            Route::post('duplicate-blog', 'BlogController@duplicatePage')->name('bb-duplicate-blog');
            Route::post('duplicate-section', 'BlogController@duplicateSection')->name('bb-duplicate-section');
            Route::post('duplicate-component', 'BlogController@duplicateComponent')->name('bb-duplicate-component');
        });
        
        Route::get('/question-for-the-day', 'AdminController@showQuestionsDailyPage')->name('admin-resources-question-for-the-day');
        Route::post('/question-for-the-day', 'AdminController@showQuestionsDailyPageCreateUpdate')->name('admin-resources-question-for-the-day-create-update');


        Route::get('/videos', 'AdminController@showVideosPage')->name('admin-resources-videos');

        // All CRUD Model Routes
        Route::group(['prefix' => 'other-data'], function() {
            Route::post('create', 'AdminController@createOtherData')->name('create-other-data');
            Route::post('update', 'AdminController@updateOtherData')->name('update-other-data');
            Route::post('remove', 'AdminController@removeOtherData')->name('remove-other-data');
        });

    });

    Route::get('/notifications', 'AdminController@showNotificationsPage')->name('admin-notifications');
    Route::get('/manage-clients', 'AdminController@showManageClientsPage')->name('admin-manage-clients');
    Route::get('/manage-users', 'AdminController@showManageUsersPage')->name('admin-manage-users');
    Route::get('/my-team', 'AdminController@showMyTeamPage')->name('admin-my-team');
    Route::get('/newsletters', 'AdminController@showNewslettersPage')->name('admin-newsletters');
    Route::get('/webminars', 'AdminController@showWebminarsPage')->name('admin-webminars');
    Route::get('/invoices', 'AdminController@showInvoicesPage')->name('admin-invoices');
    Route::get('/contact-us', 'AdminController@showContactUsPage')->name('admin-contact-us');
    Route::get('/inquiries', 'AdminController@showInquiriesPage')->name('admin-inquiries');
    Route::get('/reports', 'AdminController@showReportsPage')->name('admin-reports');

    // Call Schedule
    Route::group(['prefix' => 'call-schedule'], function() {
        Route::get('/current', 'AdminController@showCallScheduleAllPage')->name('admin-call-schedule-current');
        Route::get('/manage/{date}', 'AdminController@showCallScheduleManagePage')->name('admin-call-schedule-manage');
        Route::post('/create', 'AdminController@createCallSchedulesForTheDay')->name('admin-call-schedule-create');
    });

    // Updates switches
    Route::post('update', 'AdminController@updateSwitch')->name('update-switch');
    
    // Manage Site
    Route::group(['prefix' => 'manage-site'], function() {
        Route::get('/pages/all', 'PageBuilderController@allPages')->name('admin-manage-site-pages-all');
        Route::get('/pages/add', 'PageBuilderController@addPage')->name('admin-manage-site-page-add');
        Route::get('/pages/edit/{slug}', 'PageBuilderController@editPage')->name('admin-manage-site-page-edit');
        Route::post('/pages/page-builder', 'PageBuilderController@addOrEditPage')->name('admin-manage-site-page-submit');
        Route::get('/pages/page-builder/{type}/{slug}', 'PageBuilderController@pageBuilder')->name('admin-manage-site-page-builder');
        
        
        Route::get('/details', 'AdminController@showSiteDetailsPage')->name('admin-manage-site-details');
        Route::get('/content', 'AdminController@showSiteContentPage')->name('admin-manage-site-content');

        Route::get('/banners', 'AdminController@showSiteBannersPage')->name('admin-manage-site-banners');
        Route::get('/banners/add', 'AdminController@addSiteBannersPage')->name('admin-manage-site-banners-add');
        Route::get('/banners/edit/{id}', 'AdminController@editSitebannersPage')->name('admin-manage-site-banners-edit');

        Route::get('/about-us', 'AdminController@showSiteAboutUsPage')->name('admin-manage-site-about-us');
        Route::get('/partners', 'AdminController@showSitePartnersPage')->name('admin-manage-site-partners');

        // Route::get('/services', 'AdminController@showSiteServicesPage')->name('admin-manage-site-services');
        // Route::get('/services/add', 'AdminController@addSiteServicesPage')->name('admin-manage-site-services-add');
        // Route::get('/services/edit/{id}', 'AdminController@editSiteServicesPage')->name('admin-manage-site-services-edit');
        // Route::get('/services/preview/{slug}', 'AdminController@previewService')->name('admin-manage-site-preview-service-page');

        Route::get('/testimonials', 'AdminController@showSiteTestimonialsPage')->name('admin-manage-site-testimonials');
        Route::get('/testimonials/add', 'AdminController@addSiteTestimonialsPage')->name('admin-manage-site-testimonials-add');
        Route::get('/testimonials/edit/{id}', 'AdminController@editSiteTestimonialsPage')->name('admin-manage-site-testimonials-edit');

        Route::get('/team', 'AdminController@showSiteTeamPage')->name('admin-manage-site-team');
        Route::get('/team/add', 'AdminController@addSiteTeamPage')->name('admin-manage-site-team-add');
        Route::get('/team/edit/{id}', 'AdminController@editSiteTeamPage')->name('admin-manage-site-team-edit');

        Route::get('/pricing', 'AdminController@showSitePricingPage')->name('admin-manage-site-pricing');
        Route::get('/pricing/add', 'AdminController@addSitePricingPage')->name('admin-manage-site-pricing-add');
        Route::get('/pricing/edit/{id}', 'AdminController@editSitePricingPage')->name('admin-manage-site-pricing-edit');

        Route::get('/faqs', 'AdminController@showSiteFAQsPage')->name('admin-manage-site-faqs');
        Route::get('/faqs/add', 'AdminController@addSiteFAQsPage')->name('admin-manage-site-faqs-add');
        Route::get('/faqs/edit/{id}', 'AdminController@editSiteFAQsPage')->name('admin-manage-site-faqs-edit');

        Route::get('/social-links', 'AdminController@showSiteSocialLinksPage')->name('admin-manage-site-socials');
        Route::get('/social-links/add', 'AdminController@addSiteSocialPage')->name('admin-manage-site-social-add');
        Route::get('/social-links/edit/{id}', 'AdminController@editSiteSocialPage')->name('admin-manage-site-social-edit');

        Route::get('/email-contents', 'AdminController@showEmailContentsPage')->name('admin-manage-site-email-contents');
        Route::post('/email-contents-test-email', 'AdminController@sendTestEmail')->name('admin-manage-site-email-contents-test-email');
    });

    // Partners Page
    // Route::prefix('partners-page')->group(function() {
    //     Route::get('/other-pages', 'AdminController@manageSiteOther')->name('manage-site-other-pages');
    //     Route::get('/add-page', 'AdminController@addPageView')->name('add-page-view');
    //     Route::get('/edit-page/{id}', 'AdminController@editPageView')->name('edit-page-view');
    //     Route::post('/add-page', 'AdminController@addPage')->name('add-page');
    //     Route::post('/remove-page', 'AdminController@removePage')->name('page-remove');
    // });
});


// All CRUD Model Routes
Route::group(['prefix' => 'site'], function() {
    Route::post('create', 'SiteController@createData')->name('create-data');
    Route::post('update', 'SiteController@updateData')->name('update-data');
    // Route::post('get', 'SiteController@otherDataGet');
    // Route::post('edit', 'SiteController@otherDataEdit');
    // Route::post('get-to-edit', 'SiteController@otherDataGetToEdit');
    Route::post('remove', 'SiteController@removeData')->name('remove-data');
    // Route::post('get-all-by-id', 'SiteController@otherDataGetAllByModel');
    // Route::post('get-all', 'SiteController@otherDataGetAll');
});


// All CRUD Page Builder Model Routes
Route::group(['prefix' => 'page-builder'], function() {
    Route::post('create', 'PageBuilderController@createData')->name('pb-create-data');
    Route::post('update', 'PageBuilderController@updateData')->name('pb-update-data');
    Route::post('remove', 'PageBuilderController@removeData')->name('pb-remove-data');
    Route::get('preview/{slug}', 'PageBuilderController@previewPage')->name('pb-preview-page');

    // Section Components
    Route::get('content/{section_id}/{content_id}', 'PageBuilderController@sectionAddContent')->name('pb-content');
    Route::get('feature-block/{section_id}/{feature_block_id}', 'PageBuilderController@sectionAddFeatureBlock')->name('pb-feature-block');
    Route::get('feature-section/{section_id}/{feature_section_id}', 'PageBuilderController@sectionAddFeatureSection')->name('pb-feature-section');

    // Duplicate Page, Section and Component
    Route::post('duplicate-page', 'PageBuilderController@duplicatePage')->name('pb-duplicate-page');
    Route::post('duplicate-section', 'PageBuilderController@duplicateSection')->name('pb-duplicate-section');
    Route::post('duplicate-component', 'PageBuilderController@duplicateComponent')->name('pb-duplicate-component');

    Route::post('upload-image', 'PageBuilderController@uploadImage')->name('pb-upload-image');
    Route::post('upload-video', 'PageBuilderController@uploadVideo')->name('pb-upload-video');
});

// Services Pages
Route::get('/services/{slug}', 'WebsiteController@viewService')->name('view-service-page');

// Blogs Pages
Route::get('/blogs/{category}', 'WebsiteController@viewAllBlogs')->name('view-all-blogs');
Route::get('/blog/{slug}', 'WebsiteController@viewBlog')->name('view-blog');
Route::post('/blog/get-title', 'WebsiteController@getBlogTitleAutocomplete')->name('autocomplete-blog');

// Qotds Page
Route::get('/question-of-the-day', 'WebsiteController@viewAllQotds')->name('view-qotd');
Route::post('/question-of-the-day/get-question', 'WebsiteController@getQotdQuestionAutocomplete')->name('autocomplete-qotd');

// Pages
Route::get('/{page}', 'WebsiteController@page');
